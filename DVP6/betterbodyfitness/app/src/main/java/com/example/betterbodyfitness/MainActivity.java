package com.example.betterbodyfitness;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.betterbodyfitness.database.DataBaseHelper;
import com.example.betterbodyfitness.fragments.ChooseWorkoutFragment;
import com.example.betterbodyfitness.fragments.DisplayWorkoutFragment;
import com.example.betterbodyfitness.fragments.LogReps;
import com.example.betterbodyfitness.fragments.MainFragment;

import java.util.ArrayList;
import java.util.Objects;

import com.example.betterbodyfitness.fragments.PreferenceFragment;
import com.example.betterbodyfitness.objects.Workouts;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class MainActivity extends AppCompatActivity implements ChooseWorkoutFragment.onWorkoutClickListener {

    public static final String TAG = "MainActivity.TAG";
    DataBaseHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = getSharedPreferences(PreferenceFragment.MY_SAVED_PREFS, Context.MODE_PRIVATE);
        String theme = prefs.getString(PreferenceFragment.MY_SAVED_PREFS, PreferenceFragment.DARK_THEME);
        if (theme.equals(PreferenceFragment.DARK_THEME)) {
            setContentView(R.layout.activity_main);
        } else {
            setContentView(R.layout.activity_main_light);
        }

        MainFragment fragment = MainFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.animator.slide_in_from_left, 0, 0, R.animator.slide_out_from_right)
                .replace(R.id.fragment_container, fragment, MainFragment.TAG)
                .commit();

        getAllWorkouts();

        mHelper = DataBaseHelper.getINSTANCE(this);
        mHelper.DeleteAllWorkouts();
    }

    private void getAllWorkouts() {
        // Access a Cloud Firestore instance from your Activity
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("Upper Body")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Workouts workouts = document.toObject(Workouts.class);
                                mHelper.insertWorkout(workouts.getTitle(), workouts.getBodyPart(), workouts.getDescription());
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

        db.collection("Lower Body")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Workouts workouts = document.toObject(Workouts.class);
                                mHelper.insertWorkout(workouts.getTitle(), workouts.getBodyPart(), workouts.getDescription());
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

        db.collection("Total Body")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Workouts workouts = document.toObject(Workouts.class);
                                mHelper.insertWorkout(workouts.getTitle(), workouts.getBodyPart(), workouts.getDescription());
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

        db.collection("Cardio")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                Workouts workouts = document.toObject(Workouts.class);
                                mHelper.insertWorkout(workouts.getTitle(), workouts.getBodyPart(), workouts.getDescription());
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }

                    }
                });
    }

    @Override
    public void workoutPlanClicked(ArrayList<Workouts> workoutsArrayList) {
        DisplayWorkoutFragment fragment = DisplayWorkoutFragment.newInstance(workoutsArrayList);
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("Display")
                .setCustomAnimations(R.animator.slide_in_from_left, 0, 0, R.animator.slide_out_from_right)
                .replace(R.id.fragment_container, fragment, DisplayWorkoutFragment.TAG)
                .commit();

        Toast.makeText(this, "Get ready to workout", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMenuClick() {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void openPreferences() {
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("Pref")
                .setCustomAnimations(R.animator.slide_in_from_left, 0, 0, R.animator.slide_out_from_right)
                .replace(R.id.fragment_container, PreferenceFragment.newInstance())
                .commit();
    }

    @Override
    public void signOut() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void deleteAccount() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void changeTheme() {
        SharedPreferences prefs = getSharedPreferences(PreferenceFragment.MY_SAVED_PREFS, Context.MODE_PRIVATE);
        String theme = prefs.getString(PreferenceFragment.MY_SAVED_PREFS, PreferenceFragment.DARK_THEME);
        if (theme.equals(PreferenceFragment.DARK_THEME)) {
            setContentView(R.layout.activity_main);
        } else {
            setContentView(R.layout.activity_main_light);
        }
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void logReps(Workouts workout) {
        LogReps fragment = LogReps.newInstance(workout);
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("Log")
                .setCustomAnimations(R.animator.slide_in_from_left, 0, 0, R.animator.slide_out_from_right)
                .replace(R.id.fragment_container, fragment, LogReps.TAG)
                .commit();
    }

    @Override
    public void closeLogRep() {
        getSupportFragmentManager().popBackStack();
    }
}
