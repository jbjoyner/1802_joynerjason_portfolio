package com.example.betterbodyfitness.objects;

import java.io.Serializable;

public class Workouts implements Serializable {

    private String title;
    private String bodyPart;
    private String description;
    private String reps;
    private String time;
    private int _id;

    public Workouts() {
    }

    public Workouts(String title, String bodyPart, String description) {
        this.title = title;
        this.bodyPart = bodyPart;
        this.description = description;
    }

    public Workouts(int _id, String title, String bodyPart, String description) {
        this._id = _id;
        this.title = title;
        this.bodyPart = bodyPart;
        this.description = description;
    }

    public Workouts(int _id,String title, String bodyPart, String description, String reps, String  time) {
        this._id = _id;
        this.title = title;
        this.bodyPart = bodyPart;
        this.description = description;
        this.reps = reps;
        this.time = time;
    }

    public int get_id() { return _id; }

    public String getTitle() {
        return title;
    }

    public String getBodyPart() {
        return bodyPart;
    }

    public String getDescription() { return description; }

    public String getReps() { return reps; }

    public String  getTime() { return time; }
}
