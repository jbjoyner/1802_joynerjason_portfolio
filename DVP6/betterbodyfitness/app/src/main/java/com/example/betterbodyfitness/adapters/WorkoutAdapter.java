package com.example.betterbodyfitness.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.betterbodyfitness.R;
import com.example.betterbodyfitness.objects.Workouts;

import java.util.ArrayList;

public class WorkoutAdapter extends BaseAdapter {

    private static class ViewHolder {
        TextView title;
        TextView bodyPart;
        TextView description;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    private static final String TAG = "";

    private final ArrayList<Workouts> workouts;
    private final Context context;

    public WorkoutAdapter(ArrayList<Workouts> workouts, Context mContext) {
        this.workouts = workouts;
        this.context = mContext;
    }

    @Override
    public int getCount() {

        if (workouts != null) {
            return  workouts.size();
        } else {
            Log.i(TAG, "getCount: There is no collection" );
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {

        if (workouts != null && position >= 0 && position < workouts.size()) {
            return workouts.get(position);
        } else {
            Log.i(TAG, "getItem: There was a problem getting an item.");
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
            return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.workout_listview_layout, parent, false);
        }

        Workouts workout = (Workouts) getItem(position);

        mViewHolder.title = convertView.findViewById(R.id.titleTextView);
        mViewHolder.bodyPart = convertView.findViewById(R.id.bodyPartTextView);
        mViewHolder.description = convertView.findViewById(R.id.descriptionTextView);

        mViewHolder.title.setText(workout.getTitle());
        mViewHolder.bodyPart.setText(workout.getBodyPart());
        mViewHolder.description.setText(workout.getDescription());
        return convertView;
    }
}
