package com.example.betterbodyfitness.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.example.betterbodyfitness.R;
import com.example.betterbodyfitness.adapters.WorkoutAdapter;
import com.example.betterbodyfitness.database.DataBaseHelper;
import com.example.betterbodyfitness.objects.Workouts;

import java.util.ArrayList;

public class ViewWorkoutsFragment extends ListFragment {

    public static final String TAG = "ViewWorkoutsFrag.TAG";
    private ArrayList<Workouts> workoutsArrayList;
    private ChooseWorkoutFragment.onWorkoutClickListener mListener;
    private DataBaseHelper mHelper;
    private Cursor cursor;

    public static ViewWorkoutsFragment newInstance() {
        return new ViewWorkoutsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        workoutsArrayList = new ArrayList<>();
        mHelper = DataBaseHelper.getINSTANCE(getContext());
        cursor = mHelper.getAllData();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences prefs = requireActivity().getSharedPreferences(PreferenceFragment.MY_SAVED_PREFS, Context.MODE_PRIVATE);
        String theme = prefs.getString(PreferenceFragment.MY_SAVED_PREFS, PreferenceFragment.DARK_THEME);
        if (theme.equals(PreferenceFragment.DARK_THEME)) {
            return inflater.inflate(R.layout.view_workouts_layout, container, false);
        } else {
            return inflater.inflate(R.layout.view_workouts_layout_light, container, false);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home, menu);
        inflater.inflate(R.menu.preferences, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                mListener.onMenuClick();
                break;
            case R.id.preferences:
                mListener.openPreferences();
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ChooseWorkoutFragment.onWorkoutClickListener) {
            mListener = (ChooseWorkoutFragment.onWorkoutClickListener) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        while (cursor.moveToNext()) {
            int _id = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.COLUMN_ID));
            String title = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_TITLE));
            String bodypart = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_BODY_PART));
            String description = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_DESCRIPTION));
            Workouts workouts = new Workouts(_id, title, bodypart, description);
            workoutsArrayList.add(workouts);
        }
        WorkoutAdapter adapter = new WorkoutAdapter(workoutsArrayList, getContext());
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (mListener != null) {
            mListener.workoutPlanClicked(workoutsArrayList);
        }
    }
}
