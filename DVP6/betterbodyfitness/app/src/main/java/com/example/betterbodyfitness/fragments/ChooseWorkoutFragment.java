package com.example.betterbodyfitness.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.betterbodyfitness.R;
import com.example.betterbodyfitness.database.DataBaseHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Objects;

import com.example.betterbodyfitness.objects.Workouts;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class ChooseWorkoutFragment extends Fragment {

    public static final String TAG = "ChooseWorkout.TAG";

    private View.OnClickListener mClickListener;

    private DataBaseHelper mHelper;
    private Cursor cursor;

    private ArrayList<Workouts> mUpperBody;
    private ArrayList<Workouts> mLowerBody;
    private ArrayList<Workouts> mTotalBody;
    private ArrayList<Workouts> mCardio;
    private ArrayList<Workouts> mUserChoice;
    private ArrayList<Workouts> mAllWorkouts;

    private static final String UPPER_BODY = "Upper Body";
    private static final String LOWER_BODY = "Lower Body";
    private static final String TOTAL_BODY = "Total Body";
    private static final String CARDIO = "Cardio";

    public static ChooseWorkoutFragment newInstance() {
        return new ChooseWorkoutFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mHelper = DataBaseHelper.getINSTANCE(getContext());
        loadData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences prefs = requireActivity().getSharedPreferences(PreferenceFragment.MY_SAVED_PREFS, Context.MODE_PRIVATE);
        String theme = prefs.getString(PreferenceFragment.MY_SAVED_PREFS, PreferenceFragment.DARK_THEME);
        if (theme.equals(PreferenceFragment.DARK_THEME)) {
            return inflater.inflate(R.layout.choose_layout, container, false);
        } else {
            return inflater.inflate(R.layout.choose_layout_light, container, false);
        }
    }

    public interface onWorkoutClickListener {
        void workoutPlanClicked(ArrayList<Workouts> workoutsArrayList);
        void onMenuClick();
        void openPreferences();
        void signOut();
        void deleteAccount();
        void changeTheme();
        void logReps(Workouts workout);
        void closeLogRep();
    }

    private onWorkoutClickListener mListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof onWorkoutClickListener) {
            mListener = (onWorkoutClickListener) context;
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home, menu);
        inflater.inflate(R.menu.preferences, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                mListener.onMenuClick();
                break;
            case R.id.preferences:
                mListener.openPreferences();
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            view.findViewById(R.id.upper).setOnClickListener(mClickListener);
            view.findViewById(R.id.lower).setOnClickListener(mClickListener);
            view.findViewById(R.id.total).setOnClickListener(mClickListener);
            view.findViewById(R.id.cardio).setOnClickListener(mClickListener);
            view.findViewById(R.id.custom).setOnClickListener(mClickListener);
        }
        cursor = mHelper.getAllData();
        setUpTheArrays();
        getAllData();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.upper:
                        mUserChoice = mUpperBody;
                        break;
                    case R.id.lower:
                        mUserChoice = mLowerBody;
                        break;
                    case R.id.total:
                        mUserChoice = mTotalBody;
                        break;
                    case R.id.cardio:
                        mUserChoice = mCardio;
                        break;
                    case R.id.custom:
                        if (CreateAWorkoutFragment.customArrayList != null) {
                            mUserChoice = CreateAWorkoutFragment.customArrayList;
                        }
                        break;
                }
                mListener.workoutPlanClicked(mUserChoice);
            }
        };
    }

    private void getAllData() {
        while (cursor.moveToNext()) {
            int _id = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.COLUMN_ID));
            String title = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_TITLE));
            String bodyPart = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_BODY_PART));
            String description = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_DESCRIPTION));
            mAllWorkouts.add(new Workouts(_id, title, bodyPart, description));
        }
            for (Workouts workouts:mAllWorkouts
            ) {
                switch (workouts.getDescription()) {
                    case UPPER_BODY:
                        mUpperBody.add(workouts);
                        break;
                    case LOWER_BODY:
                        mLowerBody.add(workouts);
                        break;
                    case TOTAL_BODY:
                        mTotalBody.add(workouts);
                        break;
                    case CARDIO:
                        mCardio.add(workouts);
                        break;

            }
        }
    }

    private void setUpTheArrays() {
        mUpperBody = new ArrayList<>();
        mLowerBody = new ArrayList<>();
        mTotalBody = new ArrayList<>();
        mCardio = new ArrayList<>();
        mUserChoice = new ArrayList<>();
        mAllWorkouts = new ArrayList<>();
    }

    private void loadData() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(CreateAWorkoutFragment.CUSTOM, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(CreateAWorkoutFragment.CUSTOM, null);
        Type type = new TypeToken<ArrayList<Workouts>>() {}.getType();
        CreateAWorkoutFragment.customArrayList = gson.fromJson(json, type);

        if (CreateAWorkoutFragment.customArrayList == null) {
            CreateAWorkoutFragment.customArrayList = new ArrayList<>();
        }
    }
}
