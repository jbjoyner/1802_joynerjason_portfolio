package com.example.betterbodyfitness.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.betterbodyfitness.objects.Workouts;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_FILE = "database.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_NAME = "employees";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_BODY_PART = "body_part";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_REPS = "reps";
    public static final String COLUMN_TIME = "time";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_TITLE + " TEXT, " +
            COLUMN_BODY_PART + " TEXT, " +
            COLUMN_DESCRIPTION + " TEXT, " +
            COLUMN_REPS + " TEXT, " +
            COLUMN_TIME + " TEXT)";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    private static DataBaseHelper INSTANCE = null;

    public static  DataBaseHelper getINSTANCE (Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DataBaseHelper(context);
        }
        return INSTANCE;
    }

    private final SQLiteDatabase mDatabase;

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    private DataBaseHelper(Context context) {
        super(context, DATABASE_FILE, null,
                DATABASE_VERSION);

        mDatabase = getWritableDatabase();
    }

    public void insertWorkout(String title, String bodyPart, String description) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_TITLE, title);
        contentValues.put(COLUMN_BODY_PART, bodyPart);
        contentValues.put(COLUMN_DESCRIPTION, description);

        mDatabase.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getAllData() {
        return mDatabase.query(TABLE_NAME, null, null, null, null, null, null);
    }

    public void updateEmployee(Workouts employee, Workouts newEmployee) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_TITLE, newEmployee.getTitle());
        contentValues.put(COLUMN_BODY_PART, newEmployee.getBodyPart());
        contentValues.put(COLUMN_DESCRIPTION, newEmployee.getDescription());
        contentValues.put(COLUMN_REPS, newEmployee.getReps());
        contentValues.put(COLUMN_TIME, newEmployee.getTime());

        String selection = COLUMN_TITLE + " LIKE ?";
        String[] args = {String.valueOf(employee.getTitle())};
        int count = mDatabase.update(TABLE_NAME, contentValues, selection, args);
        Log.i("DataBase", "Number of updated items " + count);
    }

    public void DeleteAllWorkouts() {
        mDatabase.delete(TABLE_NAME, null, null);
    }
}
