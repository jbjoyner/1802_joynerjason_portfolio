package com.example.betterbodyfitness.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.betterbodyfitness.R;

import java.util.ArrayList;
import java.util.Locale;

import com.example.betterbodyfitness.objects.Workouts;

public class DisplayWorkoutFragment extends Fragment {

    public static final String TAG = "DisplayWorkout.TAG";

    public static final String ARRAY = "Array";

    private static final long START_TIME_IN_MILLIS = 60000;

    private static final long PREP_TIME_IN_MILLIS = 30000;

    private View.OnClickListener mOnClickListener;

    private TextView textView;
    private TextView mTextViewCountDown;
    private Button mStartPause;
    private ArrayList<Workouts> workouts;

    private CountDownTimer mCountDownTimer;

    private boolean mTimerRunning;

    private long mTimeLeftInMillis = START_TIME_IN_MILLIS;

    private long mPrepLeftInMillis = PREP_TIME_IN_MILLIS;

    private ChooseWorkoutFragment.onWorkoutClickListener mListener;

    private int i = 0;

    private Workouts workout;
    public static final String START = "Start";
    public static final String TITLE = "Title: ";
    public static final String BODY_PART = "Body Part: ";
    public static final String DESCRIPTION = "Description: ";

    public static DisplayWorkoutFragment newInstance(ArrayList<Workouts> userChoice) {

        Bundle args = new Bundle();
        args.putSerializable(ARRAY, userChoice);

        DisplayWorkoutFragment fragment = new DisplayWorkoutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences prefs = requireActivity().getSharedPreferences(PreferenceFragment.MY_SAVED_PREFS, Context.MODE_PRIVATE);
        String theme = prefs.getString(PreferenceFragment.MY_SAVED_PREFS, PreferenceFragment.DARK_THEME);
        if (theme.equals(PreferenceFragment.DARK_THEME)) {
            return inflater.inflate(R.layout.display_layout, container, false);
        } else {
            return inflater.inflate(R.layout.display_layout_light, container, false);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home, menu);
        inflater.inflate(R.menu.preferences, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                mListener.onMenuClick();
                break;
            case R.id.preferences:
                mListener.openPreferences();
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ChooseWorkoutFragment.onWorkoutClickListener) {
            mListener = (ChooseWorkoutFragment.onWorkoutClickListener) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();
        if (view != null) {
            mStartPause = view.findViewById(R.id.btnStartPause);
            textView = view.findViewById(R.id.workoutAddedTxtView);
            mTextViewCountDown = view.findViewById(R.id.timerTxtView);
            view.findViewById(R.id.btnStartPause).setOnClickListener(mOnClickListener);
            view.findViewById(R.id.btnReps).setOnClickListener(mOnClickListener);
            view.findViewById(R.id.btnPrevious).setOnClickListener(mOnClickListener);
            view.findViewById(R.id.btnNext).setOnClickListener(mOnClickListener);
        }
        updatePrepCountDownText();
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        workouts = (ArrayList<Workouts>) requireArguments().getSerializable(ARRAY);
        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.btnStartPause:
                        if (workouts != null && !workouts.isEmpty()) {
                            if (!mTimerRunning) {
                                runTimer();
                            } else {
                                pauseTimer();
                            }
                        } else {
                            Toast.makeText(getContext(), R.string.no_custom, Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.btnReps:
                        if (workouts != null && !workouts.isEmpty()) {
                            workout = workouts.get(i);
                            if (mCountDownTimer != null) {
                                mCountDownTimer.cancel();
                            }
                            mListener.logReps(workout);
                        } else {
                            Toast.makeText(getContext(), R.string.no_custom, Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.btnPrevious:
                        if (workouts != null && !workouts.isEmpty()) {
                            if (mTimerRunning) {
                                mCountDownTimer.cancel();
                                mStartPause.setText(R.string.start);
                                mPrepLeftInMillis = PREP_TIME_IN_MILLIS;
                                mTimeLeftInMillis = START_TIME_IN_MILLIS;
                            }
                            if (workouts != null) {
                                if (i == workouts.size()) {
                                    i--;
                                }
                                if (i > 0) {
                                    i--;
                                    updatePrepCountDownText();
                                    if (mTimerRunning) {
                                        runTimer();
                                    }
                                }
                            }
                        } else {
                            Toast.makeText(getContext(), R.string.no_custom, Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.btnNext:
                        if (workouts != null && !workouts.isEmpty()) {
                            if (mTimerRunning) {
                                mCountDownTimer.cancel();
                                mStartPause.setText(R.string.start);
                                mPrepLeftInMillis = PREP_TIME_IN_MILLIS;
                                mTimeLeftInMillis = START_TIME_IN_MILLIS;
                            }
                            if (workouts != null) {
                                if (i < workouts.size()) {
                                    i++;
                                    updatePrepCountDownText();
                                    if (mTimerRunning) {
                                        runTimer();
                                    }
                                }
                            }
                        } else {
                            Toast.makeText(getContext(), R.string.no_custom, Toast.LENGTH_SHORT).show();
                        }
                            break;
                        }
                }
            };
        }

        private void prepTimer() {
            mCountDownTimer = new CountDownTimer(mPrepLeftInMillis, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mPrepLeftInMillis = millisUntilFinished;
                    mStartPause.setText(R.string.pause);
                    updatePrepCountDownText();
                }

                @Override
                public void onFinish() {
                    mTimerRunning = false;
                    mPrepLeftInMillis = PREP_TIME_IN_MILLIS;
                    startTimer();
                }
            }.start();
            mTimerRunning = true;
        }

        private void startTimer() {
            mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mTimeLeftInMillis = millisUntilFinished;
                    mStartPause.setText(R.string.pause);
                    updateCountDownText();
                }

                @Override
                public void onFinish() {
                    mTimerRunning = false;
                    mTimeLeftInMillis = START_TIME_IN_MILLIS;
                    mStartPause.setText(R.string.start);
                    if (i < workouts.size()) {
                        ++i;
                    }
                    runTimer();
                }
            }.start();
            mTimerRunning = true;
        }

        private void pauseTimer() {
            mCountDownTimer.cancel();
            mTimerRunning = false;
            mStartPause.setText(R.string.start);
            mTextViewCountDown.setText(R.string.paused);

        }

        private void updatePrepCountDownText() {
            int minutes = (int) (mPrepLeftInMillis / 1000) / 60;
            int seconds = (int) (mPrepLeftInMillis/ 1000) % 60;
            if (i < workouts.size() && i != -1) {
                textView.setText(String.format("%s\n%s\n%s", TITLE + workouts.get(i).getTitle(), BODY_PART + workouts.get(i).getBodyPart(), DESCRIPTION + workouts.get(i).getDescription()));
            }
            String timeLeftFormatted = String.format(Locale.getDefault(),"Get Ready\n %02d:%02d", minutes, seconds);
            mTextViewCountDown.setText(timeLeftFormatted);
        }

        private void updateCountDownText() {
            int minutes = (int) (mTimeLeftInMillis / 1000) / 60;
            int seconds = (int) (mTimeLeftInMillis/ 1000) % 60;
            if (i < workouts.size() && i != -1) {
                textView.setText(String.format("%s\n%s\n%s", TITLE + workouts.get(i).getTitle(), BODY_PART + workouts.get(i).getBodyPart(), DESCRIPTION + workouts.get(i).getDescription()));
            }
            String timeLeftFormatted = String.format(Locale.getDefault(),"Go\n %02d:%02d", minutes, seconds);
            mTextViewCountDown.setText(timeLeftFormatted);
        }

        private void runTimer() {
            if (workouts != null) {
                if (i < workouts.size() && i != -1) {
                    mTimerRunning = true;
                    updatePrepCountDownText();
                    textView.setText(String.format("%s\n%s\n%s", TITLE + workouts.get(i).getTitle(), BODY_PART + workouts.get(i).getBodyPart(), DESCRIPTION + workouts.get(i).getDescription()));
                    prepTimer();
                }
            }
        }
    }
