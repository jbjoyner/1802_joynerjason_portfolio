package com.example.betterbodyfitness.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.example.betterbodyfitness.R;
import com.example.betterbodyfitness.database.DataBaseHelper;
import com.example.betterbodyfitness.objects.Workouts;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Objects;

public class PreferenceFragment extends PreferenceFragmentCompat {

    private DataBaseHelper helper;
    private ListPreference mListPreference;
    public static final String MY_SAVED_PREFS = "My Saved Preferences";
    private ChooseWorkoutFragment.onWorkoutClickListener mListener;
    public static final String DARK_THEME = "Dark Theme";
    public static final String LIGHT_THEME = "Light Theme";

    public static PreferenceFragment newInstance() {

        Bundle args = new Bundle();

        PreferenceFragment fragment = new PreferenceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ChooseWorkoutFragment.onWorkoutClickListener) {
            mListener = (ChooseWorkoutFragment.onWorkoutClickListener) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        helper = DataBaseHelper.getINSTANCE(getContext());

        final Preference pref = findPreference("DEL_ALL");
        Objects.requireNonNull(pref).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference _pref) {
                AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                //Set the values of the AlertDialog
                builder.setTitle(getString(R.string.delete_all_workouts));
                builder.setMessage(getString(R.string.sure_delete_all));
                //Set the button for the dialog
                builder.setPositiveButton(getString(R.string.cancel), null);
                builder.setNeutralButton(getString(R.string.Delete), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        helper.DeleteAllEmployees();
                        deleteData();
                        Toast.makeText(getContext(), R.string.dataDeleted, Toast.LENGTH_SHORT).show();
                    }
                });
                //Show the dialog
                builder.show();
                return true;
            }
        });

        mListPreference = getPreferenceManager().findPreference("list_preference");
        Objects.requireNonNull(mListPreference).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {

                switch (mListPreference.findIndexOfValue(newValue.toString())) {
                    case 0:
                        SharedPreferences.Editor editor = requireActivity().getSharedPreferences(MY_SAVED_PREFS, Context.MODE_PRIVATE).edit();
                        String newDateString = DARK_THEME;
                        editor.putString(MY_SAVED_PREFS, newDateString);
                        editor.apply();
                        break;
                    case 1:
                        editor = requireActivity().getSharedPreferences(MY_SAVED_PREFS, Context.MODE_PRIVATE).edit();
                        newDateString = LIGHT_THEME;
                        editor.putString(MY_SAVED_PREFS, newDateString);
                        editor.apply();
                        break;
                }
                mListener.changeTheme();
                return true;
            }
        });

        final Preference signOutPref = findPreference("Sign Out");
        Objects.requireNonNull(signOutPref).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference _pref) {
                AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                //Set the values of the AlertDialog
                builder.setTitle(R.string.sign_out);
                builder.setMessage(R.string.sure_sign_out);
                //Set the button for the dialog
                builder.setPositiveButton(getString(R.string.cancel), null);
                builder.setNeutralButton(getString(R.string.sign_out), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        signOut();
                        Toast.makeText(getContext(), R.string.signed_out, Toast.LENGTH_SHORT).show();
                    }
                });
                //Show the dialog
                builder.show();
                return true;
            }
        });

        final Preference deleteAccount = findPreference("Delete Account");
        Objects.requireNonNull(deleteAccount).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference _pref) {
                AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                //Set the values of the AlertDialog
                builder.setTitle(R.string.delete_account);
                builder.setMessage(R.string.sure_delete);
                //Set the button for the dialog
                builder.setPositiveButton(getString(R.string.cancel), null);
                builder.setNeutralButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delete();
                        Toast.makeText(getContext(), R.string.account_deleted, Toast.LENGTH_SHORT).show();
                    }
                });
                //Show the dialog
                builder.show();
                return true;
            }
        });
    }

    public void signOut() {
        AuthUI.getInstance()
                .signOut(requireContext())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        mListener.signOut();
                    }
                });
    }

    public void delete() {
        AuthUI.getInstance()
                .delete(requireContext())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mListener.deleteAccount();
                    }
                });
    }

    private void deleteData() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(CreateAWorkoutFragment.CUSTOM, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferences2 = requireContext().getSharedPreferences(CreateAWorkoutFragment.SAVED, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(CreateAWorkoutFragment.CUSTOM, null);
        String json2 = sharedPreferences2.getString(CreateAWorkoutFragment.SAVED, null);
        Type type = new TypeToken<ArrayList<Workouts>>() {}.getType();
        CreateAWorkoutFragment.customArrayList = gson.fromJson(json, type);
        CreateAWorkoutFragment.workoutsArrayList = gson.fromJson(json2, type);

        if (CreateAWorkoutFragment.customArrayList != null) {
            CreateAWorkoutFragment.customArrayList.clear();
            if (CreateAWorkoutFragment.workoutsArrayList != null) {
                CreateAWorkoutFragment.workoutsArrayList.clear();
            }
        }

        json = gson.toJson(CreateAWorkoutFragment.customArrayList);
        json2 = gson.toJson(CreateAWorkoutFragment.workoutsArrayList);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CreateAWorkoutFragment.CUSTOM, json);
        editor.putString(CreateAWorkoutFragment.SAVED, json2);
        editor.apply();
    }
}
