package com.example.betterbodyfitness.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.betterbodyfitness.R;
import com.example.betterbodyfitness.database.DataBaseHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Objects;

import com.example.betterbodyfitness.objects.Workouts;

public class AddWorkoutFragment extends Fragment {

    public static final String TAG = "AddWorkout.TAG";

    private ArrayList<String> mBodyPartArray;
    private ArrayList<String> mDescriptionArray;
    private String mBodyPart;
    private String mDescription;
    private DataBaseHelper mHelper;
    private String titleText;
    private ChooseWorkoutFragment.onWorkoutClickListener mListener;

    private static final String UPPER_BODY = "Upper Body";
    private static final String LOWER_BODY = "Lower Body";
    private static final String TOTAL_BODY = "Total Body";
    private static final String CARDIO = "Cardio";

    private static final String ARMS = "Arms";
    private static final String LEGS = "Legs";
    private static final String CORE = "Core";
    private static final String CHEST = "Chest";
    private static final String BACK = "Back";
    private static final String SHOULDERS = "Shoulders";
    private static final String USERS = "users";


    private static class ViewHolder {
        TextView titleTextView;
        Button addButton;
        TextView workoutAddedTextView;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    public static AddWorkoutFragment newInstance() {
        return new AddWorkoutFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences prefs = requireActivity().getSharedPreferences(PreferenceFragment.MY_SAVED_PREFS, Context.MODE_PRIVATE);
        String theme = prefs.getString(PreferenceFragment.MY_SAVED_PREFS, PreferenceFragment.DARK_THEME);
        if (theme.equals(PreferenceFragment.DARK_THEME)) {
            return inflater.inflate(R.layout.add_layout, container, false);
        } else {
            return inflater.inflate(R.layout.add_layout_light, container, false);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ChooseWorkoutFragment.onWorkoutClickListener) {
            mListener = (ChooseWorkoutFragment.onWorkoutClickListener) context;
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home, menu);
        inflater.inflate(R.menu.preferences, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                mListener.onMenuClick();
                break;
            case R.id.preferences:
                mListener.openPreferences();
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();
        populateSpinners();
        mHelper = DataBaseHelper.getINSTANCE(getContext());
        if (view != null) {
            mViewHolder.titleTextView = view.findViewById(R.id.titleText);
            mViewHolder.workoutAddedTextView = view.findViewById(R.id.workoutAddedTxtView);
            mViewHolder.addButton = view.findViewById(R.id.btnNext);
        }
        mViewHolder.addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTheWorkouts();
            }
        });
    }

    private void populateSpinners() {
        final Spinner partSpinner = requireView().findViewById(R.id.partSpinner);

        final Spinner descSpinner = requireView().findViewById(R.id.descSpinner);

        mBodyPartArray = new ArrayList<>();
        mDescriptionArray = new ArrayList<>();

        mDescriptionArray.add(UPPER_BODY);
        mDescriptionArray.add(LOWER_BODY);
        mDescriptionArray.add(TOTAL_BODY);
        mDescriptionArray.add(CARDIO);

        mBodyPartArray.add(ARMS);
        mBodyPartArray.add(LEGS);
        mBodyPartArray.add(CORE);
        mBodyPartArray.add(CHEST);
        mBodyPartArray.add(BACK);
        mBodyPartArray.add(SHOULDERS);

        ArrayAdapter<String> partAdapter = new ArrayAdapter<>(
                requireContext(),
                android.R.layout.simple_list_item_1,
                mBodyPartArray
        );

        ArrayAdapter<String> descAdapter = new ArrayAdapter<>(
                requireContext(),
                android.R.layout.simple_list_item_1,
                mDescriptionArray
        );

        if (partSpinner != null) {
            partSpinner.setAdapter(partAdapter);
        }
        if (descSpinner != null) {
            descSpinner.setAdapter(descAdapter);
        }

        if (partSpinner != null) {
            partSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i) {
                        case 0:
                            mBodyPart = mBodyPartArray.get(0);
                            break;
                        case 1:
                            mBodyPart = mBodyPartArray.get(1);
                            break;
                        case 2:
                            mBodyPart = mBodyPartArray.get(2);
                            break;
                        case 3:
                            mBodyPart = mBodyPartArray.get(3);
                            break;
                        case 4:
                            mBodyPart = mBodyPartArray.get(4);
                            break;
                        case 5:
                            mBodyPart = mBodyPartArray.get(5);
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    Log.i("Spinners", "Nothing Selected");
                }
            });
        }

        if (descSpinner != null) {
            descSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i) {
                        case 0:
                            mDescription = mDescriptionArray.get(0);
                            break;
                        case 1:
                            mDescription = mDescriptionArray.get(1);
                            break;
                        case 2:
                            mDescription = mDescriptionArray.get(2);
                            break;
                        case 3:
                            mDescription = mDescriptionArray.get(3);
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    Log.i("Spinners", "Nothing Selected");
                }
            });
        }
    }

    private void addTheWorkouts() {
        titleText = mViewHolder.titleTextView.getText().toString();

        if (titleText.isEmpty()) {
            Toast.makeText(getContext(), R.string.blank_title, Toast.LENGTH_SHORT).show();
        } else {
            mHelper.insertWorkout(titleText, mBodyPart, mDescription);

            // Access a Cloud Firestore instance from your Activity
            FirebaseFirestore db = FirebaseFirestore.getInstance();

            // Create a new workouts with a first and last name
            Workouts workouts = new Workouts(titleText, mBodyPart, mDescription);

            String WORKOUT_DATABASE = "";
            switch (mDescription) {
                case UPPER_BODY:
                    WORKOUT_DATABASE = UPPER_BODY;
                    break;
                case LOWER_BODY:
                    WORKOUT_DATABASE = LOWER_BODY;
                    break;
                case TOTAL_BODY:
                    WORKOUT_DATABASE = TOTAL_BODY;
                    break;
                case CARDIO:
                    WORKOUT_DATABASE = CARDIO;
                    break;
            }
            // Add a new document with a generated ID
            db.collection(WORKOUT_DATABASE)
                    .add(workouts)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                            Toast.makeText(getContext(), "Workout Added", Toast.LENGTH_SHORT).show();
                            mViewHolder.titleTextView.setText("");
                            mViewHolder.workoutAddedTextView.setText(String.format("Title: %s\nBody Part: %s\nDescription: %s", titleText, mBodyPart, mDescription));
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error adding document", e);
                        }
                    });
            db.collection(USERS)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                    Log.d(TAG, document.getId() + " => " + document.getData());
                                }
                            } else {
                                Log.w(TAG, "Error getting documents.", task.getException());
                            }
                        }
                    });
        }
    }
}
