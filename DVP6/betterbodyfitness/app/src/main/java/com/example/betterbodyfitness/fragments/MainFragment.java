package com.example.betterbodyfitness.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.betterbodyfitness.R;

public class MainFragment extends Fragment {

    public static final String TAG = "MainFragment.TAG";
    private View.OnClickListener mListener;
    private ChooseWorkoutFragment.onWorkoutClickListener mOptionClickListener;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences prefs = requireActivity().getSharedPreferences(PreferenceFragment.MY_SAVED_PREFS, Context.MODE_PRIVATE);
        String theme = prefs.getString(PreferenceFragment.MY_SAVED_PREFS, PreferenceFragment.DARK_THEME);
        if (theme.equals(PreferenceFragment.DARK_THEME)) {
            return inflater.inflate(R.layout.main_layout, container, false);
        } else {
            return inflater.inflate(R.layout.main_layout_light, container, false);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ChooseWorkoutFragment.onWorkoutClickListener) {
            mOptionClickListener = (ChooseWorkoutFragment.onWorkoutClickListener) context;
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.preferences, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        mOptionClickListener.openPreferences();
        return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();

        if (view != null) {
            view.findViewById(R.id.viewWorkoutsButton).setOnClickListener(mListener);
            view.findViewById(R.id.addAWorkoutButton).setOnClickListener(mListener);
            view.findViewById(R.id.chooseAPlanButton).setOnClickListener(mListener);
            view.findViewById(R.id.createAPlanButton).setOnClickListener(mListener);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {

                    case R.id.viewWorkoutsButton:
                        if (getFragmentManager() != null) {
                            ViewWorkoutsFragment fragment = ViewWorkoutsFragment.newInstance();
                            getFragmentManager()
                                    .beginTransaction()
                                    .addToBackStack("View")
                                    .setCustomAnimations(R.animator.slide_in_from_left, 0, 0, R.animator.slide_out_from_right)
                                    .replace(R.id.fragment_container, fragment, ViewWorkoutsFragment.TAG)
                                    .commit();
                        }
                        break;

                    case R.id.addAWorkoutButton:
                        if (getFragmentManager() != null) {
                            AddWorkoutFragment fragment = AddWorkoutFragment.newInstance();
                            getFragmentManager()
                                    .beginTransaction()
                                    .addToBackStack("Add")
                                    .setCustomAnimations(R.animator.slide_in_from_left, 0, 0, R.animator.slide_out_from_right)
                                    .replace(R.id.fragment_container, fragment, AddWorkoutFragment.TAG)
                                    .commit();
                        }
                        break;

                    case R.id.chooseAPlanButton:
                        if (getFragmentManager() != null) {
                            ChooseWorkoutFragment fragment = ChooseWorkoutFragment.newInstance();
                            getFragmentManager()
                                    .beginTransaction()
                                    .addToBackStack("Choose")
                                    .setCustomAnimations(R.animator.slide_in_from_left, 0, 0, R.animator.slide_out_from_right)
                                    .replace(R.id.fragment_container, fragment, ChooseWorkoutFragment.TAG)
                                    .commit();
                        }
                        break;

                    case R.id.createAPlanButton:
                        if (getFragmentManager() != null) {
                            CreateAWorkoutFragment fragment = CreateAWorkoutFragment.newInstance();
                            getFragmentManager()
                                    .beginTransaction()
                                    .addToBackStack("Create")
                                    .setCustomAnimations(R.animator.slide_in_from_left, 0, 0, R.animator.slide_out_from_right)
                                    .replace(R.id.fragment_container, fragment, CreateAWorkoutFragment.TAG)
                                    .commit();
                        }
                }
            }
        };
    }
}
