package com.example.betterbodyfitness.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.betterbodyfitness.R;
import com.example.betterbodyfitness.database.DataBaseHelper;
import com.example.betterbodyfitness.objects.Workouts;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class LogReps extends Fragment {

    public static final String TAG = "LogReps.TAG";
    private static final String WORKOUT = "Workout";
    private static final String SAVED = "Saved";
    private static final String REPS = "Reps";
    private ChooseWorkoutFragment.onWorkoutClickListener mListener;
    private View.OnClickListener mOnClickListener;
    private TextView mTimeText;
    private TextView mRepText;
    private EditText mTimeText2;
    private EditText mRepText2;
    private Workouts mWorkout;
    private DataBaseHelper mHelper;
    private ArrayList<Workouts> mSaveRepLogArrayList;

    public static LogReps newInstance(Workouts workout) {
        Bundle args = new Bundle();
        args.putSerializable(WORKOUT, workout);

        LogReps fragment = new LogReps();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHelper = DataBaseHelper.getINSTANCE(getContext());
        mSaveRepLogArrayList = new ArrayList<>();
        loadRepLog();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences prefs = requireActivity().getSharedPreferences(PreferenceFragment.MY_SAVED_PREFS, Context.MODE_PRIVATE);
        String theme = prefs.getString(PreferenceFragment.MY_SAVED_PREFS, PreferenceFragment.DARK_THEME);
        if (theme.equals(PreferenceFragment.DARK_THEME)) {
            return inflater.inflate(R.layout.log_reps_layout, container, false);
        } else {
            return inflater.inflate(R.layout.log_reps_layout_light, container, false);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home, menu);
        inflater.inflate(R.menu.preferences, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                mListener.onMenuClick();
                break;
            case R.id.preferences:
                mListener.openPreferences();
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ChooseWorkoutFragment.onWorkoutClickListener) {
            mListener = (ChooseWorkoutFragment.onWorkoutClickListener) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();
        if (view != null) {
            mTimeText = view.findViewById(R.id.timeText);
            mTimeText2 = view.findViewById(R.id.timeText2);
            mRepText = view.findViewById(R.id.repText);
            mRepText2 = view.findViewById(R.id.repText2);
            view.findViewById(R.id.btnClose).setOnClickListener(mOnClickListener);
            view.findViewById(R.id.btnSave).setOnClickListener(mOnClickListener);
        }

        for (Workouts workouts : mSaveRepLogArrayList
        ) {
            if (workouts.getTitle().equals(mWorkout.getTitle())) {
                mTimeText.setText(workouts.getTime());
                mRepText.setText(workouts.getReps());
            }
        }
    }

    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            mWorkout = (Workouts) getArguments().getSerializable(WORKOUT);
        }

        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btnClose:
                        mListener.closeLogRep();
                        break;
                    case R.id.btnSave:
                        int _id = mWorkout.get_id();
                        String title = mWorkout.getTitle();
                        String bodyPart = mWorkout.getBodyPart();
                        String description = mWorkout.getDescription();
                        String reps = mRepText2.getText().toString();
                        String time = mTimeText2.getText().toString();
                        if (reps.isEmpty() || time.isEmpty()) {
                            Toast.makeText(getContext(), R.string.blank_text, Toast.LENGTH_SHORT).show();
                        } else {
                            Workouts newWorkout = new Workouts(_id, title, bodyPart, description, reps, time);
                            mSaveRepLogArrayList.add(newWorkout);
                            saveRepLog();
                            mHelper.updateEmployee(mWorkout, newWorkout);
                            Toast.makeText(getContext(), "Workout Saved", Toast.LENGTH_SHORT).show();
                            mRepText2.setText("");
                            mTimeText2.setText("");
                            mListener.closeLogRep();
                        }
//                        Toast.makeText(getContext(), R.string.not_available, Toast.LENGTH_LONG).show();
                }
            }
        };
    }

    private void saveRepLog() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(REPS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mSaveRepLogArrayList);
        editor.putString(SAVED, json);
        editor.apply();
    }

    private void loadRepLog() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(REPS, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(SAVED, null);
        Type type = new TypeToken<ArrayList<Workouts>>() {}.getType();
        mSaveRepLogArrayList = gson.fromJson(json, type);

        if (mSaveRepLogArrayList == null) {
            mSaveRepLogArrayList = new ArrayList<>();
        }
    }
}
