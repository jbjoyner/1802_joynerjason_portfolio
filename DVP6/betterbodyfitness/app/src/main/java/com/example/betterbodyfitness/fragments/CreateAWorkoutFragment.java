package com.example.betterbodyfitness.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.example.betterbodyfitness.R;
import com.example.betterbodyfitness.adapters.WorkoutAdapter;
import com.example.betterbodyfitness.database.DataBaseHelper;
import com.example.betterbodyfitness.objects.Workouts;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class CreateAWorkoutFragment extends ListFragment {

    public static final String TAG = "CreateWorkout.TAG";
    public static final String CUSTOM = "Custom";
    public static final String SAVED = "SAVED";
    private DataBaseHelper mHelper;
    private Cursor cursor;
    public static ArrayList<Workouts> workoutsArrayList;
    public static ArrayList<Workouts> customArrayList;
    private ChooseWorkoutFragment.onWorkoutClickListener mListener;

    public static CreateAWorkoutFragment newInstance() {

        Bundle args = new Bundle();

        CreateAWorkoutFragment fragment = new CreateAWorkoutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        workoutsArrayList = new ArrayList<>();
        customArrayList = new ArrayList<>();
        mHelper = DataBaseHelper.getINSTANCE(getContext());
        cursor = mHelper.getAllData();
        loadListArray();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences prefs = requireActivity().getSharedPreferences(PreferenceFragment.MY_SAVED_PREFS, Context.MODE_PRIVATE);
        String theme = prefs.getString(PreferenceFragment.MY_SAVED_PREFS, PreferenceFragment.DARK_THEME);
        if (theme.equals(PreferenceFragment.DARK_THEME)) {
            return inflater.inflate(R.layout.create_workout_layout, container, false);
        } else {
            return inflater.inflate(R.layout.create_workout_layout_light, container, false);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ChooseWorkoutFragment.onWorkoutClickListener) {
            mListener = (ChooseWorkoutFragment.onWorkoutClickListener) context;
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home, menu);
        inflater.inflate(R.menu.preferences, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                mListener.onMenuClick();
                break;
            case R.id.preferences:
                mListener.openPreferences();
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (workoutsArrayList.isEmpty()) {
            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                int _id = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.COLUMN_ID));
                String title = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_TITLE));
                String bodypart = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_BODY_PART));
                String description = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_DESCRIPTION));
                Workouts workouts = new Workouts(_id, title, bodypart, description);
                workoutsArrayList.add(workouts);
            }
        }
        WorkoutAdapter adapter = new WorkoutAdapter(workoutsArrayList, getContext());
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Workouts workouts = workoutsArrayList.get(position);
        customArrayList.add(workouts);
        workoutsArrayList.remove(position);
        Toast.makeText(getContext(), R.string.workout_saved, Toast.LENGTH_SHORT).show();
        WorkoutAdapter adapter = new WorkoutAdapter(workoutsArrayList, getContext());
        setListAdapter(adapter);

       saveData();
       saveListArray();
    }

    private void saveData() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(CUSTOM, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(customArrayList);
        editor.putString(CUSTOM, json);
        editor.apply();
    }

    private void saveListArray() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(CUSTOM, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(workoutsArrayList);
        editor.putString(SAVED, json);
        editor.apply();
    }

    private void loadListArray() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(CUSTOM, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(SAVED, null);
        Type type = new TypeToken<ArrayList<Workouts>>() {}.getType();
        workoutsArrayList = gson.fromJson(json, type);

        if (workoutsArrayList == null) {
            workoutsArrayList = new ArrayList<>();
        }
    }
}
