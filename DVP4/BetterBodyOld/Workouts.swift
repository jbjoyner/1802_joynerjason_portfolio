//
//  Workouts.swift
//  FirebaseCore
//
//  Created by Jason Joyner on 5/17/19.
//

import Foundation

class Workouts {
    //Stored Properties
    var title: String
    var bodyPart: String
    var description: String
    
    init(title: String, bodyPart: String, description: String) {
        self.title = title
        self.bodyPart = bodyPart
        self.description = description
    }
}
