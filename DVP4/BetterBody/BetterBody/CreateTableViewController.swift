//
//  CreateTableViewController.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/17/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit
import Firebase
import CoreData

class CreateTableViewController: UITableViewController {
    
    //Declare the arrays needed
    var upperBody: [Workout] = []
    var lowerBody: [Workout] = []
    var totalBody: [Workout] = []
    var cardio: [Workout] = []
    var allWorkouts: [Workout] = []
    var usersChoice: [Workout] = []
    var customWorkouts: [Workout] = []
    
    let db = Firestore.firestore()
    var ref: DatabaseReference!
    
    //Coredata references
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the title of the tableview to the app name
        title = "Better Body"
        
        //Set the tableview to allow multiple selecting
        tableView.allowsMultipleSelectionDuringEditing = true
        
        //Set the header nib identifier and assign it
        let headerNib = UINib.init(nibName: "CustomHeader", bundle: nil)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "header_ID1")
        
        LoadCustom()
        
        UpperBody()
        LowerBody()
        TotalBody()
        Cardio()
        
        ref = Database.database().reference()
    }
    
    //Set header height
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    //Set header view
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header_ID1") as? CustomHeaderView
        
        //Check the section number
        if section == 0 {
            //Assign the header label to Available Workouts
            header?.titleLabel.text = "Available Workouts"
            //Set the count label to the count of items
            header?.countLabel.text = "Workout Count: \(String(allWorkouts.count))"
        } else {
            //Assign the header label to Custom Workouts
            header?.titleLabel.text = "Custom Workouts"
            //Set the count label to the count of items
            header?.countLabel.text = "Workout Count: \(String(customWorkouts.count))"
        }
        return header
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Check for isEditing and for the section number
        if tableView.isEditing == false {
            if indexPath.section == 0 {
                //Get the data from the allworkouts array at the index and store it in temp
                let temp = allWorkouts[indexPath.row]
                //Add the data to the customworkouts array
                customWorkouts.append(temp)
                //Reload the tableview
                tableView.reloadData()
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        //Variable to hold the count
        var count = 0
        //Check for the section number
        if section == 0 {
            //Add the value from the count from allworkouts to count
            count = allWorkouts.count
            //Check for the section number
        } else if section == 1 {
            //Add the value from the count from customworkouts to count
            count = customWorkouts.count
        }
        return count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellID-01", for: indexPath) as? CreateCell
            
            else {return tableView.dequeueReusableCell(withIdentifier: "CellID-01", for: indexPath)}
        
        // Configure the cell...
        
        //Check for the section and make sure the array count is not zero
        if indexPath.section == 0 && allWorkouts.count > 0{
            //Set the cell values to the value from allworkouts at the index path
            cell.titleLabel.text = " \(allWorkouts[indexPath.row].title)"
            cell.bodypartLabel.text = " \(allWorkouts[indexPath.row].bodyPart)"
            cell.descriptionLabel.text = " \(allWorkouts[indexPath.row].description)"
            //No cell accessory needed for this section
            cell.accessoryType = .none
            
            //Check for the section and make sure the array count is not zero
        } else if indexPath.section == 1 && customWorkouts.count > 0{
            //Set the cell values to the value from customworkouts at the index path
            cell.titleLabel.text = " \(customWorkouts[indexPath.row].title)"
            cell.bodypartLabel.text = " \(customWorkouts[indexPath.row].bodyPart)"
            cell.descriptionLabel.text = " \(customWorkouts[indexPath.row].description)"
            //Set the accessory to a check mark
            cell.accessoryType = .checkmark
        }
        return cell
    }
    
    func UpperBody() {
        //Get the data from the database and save it in the correct array based on description
        db.collection("upperbody").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "upperbody" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.upperBody.append(workoutData)
                    }
                }
                //Add the data from each array to the allworkouts arr
                for data in self.upperBody {
                    self.allWorkouts.append(data)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func LowerBody() {
        //Get the data from the database and save it in the correct array based on description
        db.collection("lowerbody").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "lowerbody" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.lowerBody.append(workoutData)
                    }
                }
                //Add the data from each array to the allworkouts array
                for data in self.lowerBody {
                    self.allWorkouts.append(data)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func TotalBody() {
        //Get the data from the database and save it in the correct array based on description
        db.collection("totalbody").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "totalbody" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.totalBody.append(workoutData)
                    }
                }
                //Add the data from each array to the allworkouts array
                for data in self.totalBody {
                    self.allWorkouts.append(data)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func Cardio() {
        //Get the data from the database and save it in the correct array based on description
        db.collection("cardio").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "cardio" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.cardio.append(workoutData)
                    }
                }
                //Add the data from each array to the allworkouts array
                for data in self.cardio {
                    self.allWorkouts.append(data)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    dynamic func SaveCustom() {
        // identify the persistentContainer context using 'UserWorkout' as the Entity class
        let workouts = UserWorkout(context: context)
        // assign it a value
        for workout in customWorkouts{
            workouts.title = workout.title
            workouts.bodyPart = workout.bodyPart
            workouts.workoutDescription = workout.description
        }
        // We've only assigned values so far, and have not saved yet
        do {
            // try saving using the persistentContainer context
            try context.save()
            print("Saving data worked")
        } catch {
            print("Saving data Failed", error.localizedDescription)
        }
    }
   
    func LoadCustom(){
        let data = NSFetchRequest<NSFetchRequestResult>(entityName: "UserWorkout")
        do {
            // We will receive an array of workouts saved
            if let workoutArray = try context.fetch(data) as? [UserWorkout] {
                print(workoutArray.count)
                // lets test to see what has been saved
                for workout in workoutArray {
                    if let wTitle = workout.title, let wBodyPart = workout.bodyPart, let wDescription = workout.workoutDescription {
                        let savedWorkout = Workout.init(title: wTitle, bodyPart: wBodyPart, description: wDescription)
                        customWorkouts.append(savedWorkout)
                        print(wTitle)
                    }
                }
            }
        } catch {
            print("Fetching data Failed", error.localizedDescription)
        }
    }
   
    @IBAction func saveCustom (segue: UIStoryboardSegue) {
        self.SaveCustom()
    }
    @IBAction func clearSaves (segue: UIStoryboardSegue) {let delete = UIAlertController(title: "Delete", message: "Are you sure you want to delete. This cannot be undone.", preferredStyle: UIAlertController.Style.alert)
        //Create the ok title for the button
        delete.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            let data = NSFetchRequest<NSFetchRequestResult>(entityName: "UserWorkout")
            let delete = NSBatchDeleteRequest(fetchRequest: data)
            do
            {
                try self.context.execute(delete)
                self.customWorkouts.removeAll()
                print("Cleared all data")
            } catch {
                print("Failed to clear all data")
                
            }
            self.tableView.reloadData()
            print("Tableview should reload")
        }))
        delete.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            //Dismiss the alert
            delete.dismiss(animated: true, completion: nil)
        }))
        //Present the alert
        present(delete, animated: true, completion: nil)
    }
}
