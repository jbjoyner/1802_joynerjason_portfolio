//
//  WorkoutDisplayViewController.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/17/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit

class WorkoutDisplayViewController: UIViewController {
    
    //Declare the arrays needed
    var upperBody: [Workout] = []
    var lowerBody: [Workout] = []
    var totalBody: [Workout] = []
    var cardio: [Workout] = []
    var usersChoice: [Workout] = []
    
    //Set the counters needed
    var counter = 0
    var workoutCounter = 0
    
    //Set the istimerrunning boolean
    var isTimerRunning = false
    
    //Declare the timer
    var timer = Timer()
    
    //Set the isrunning boolean
    var isRunning = false
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var pauseButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the tableview title to the app name
        title = "Better Body"
        
        // Do any additional setup after loading the view.
        
        //Set the textview with the workout information
        textView.text = "Title: \(usersChoice[workoutCounter].title)\n\nBody Part: \(usersChoice[workoutCounter].bodyPart)\n\nDescription: \(usersChoice[workoutCounter].description)"
        timerLabel.text = ""
    }
    
    @IBAction func start(_ sender: UIButton) {
        //Set the counter to the zero and call the updateworkout method
        if isTimerRunning == false && workoutCounter < usersChoice.count {
            workoutCounter = 0
            UpdateWorout()
        }
    }
    
    @IBAction func pause(_ sender: UIButton) {
        //Check for the conditions to pause the app if it has been started and block if it has not
        if isTimerRunning == true{
            if isRunning == true {
                timer.invalidate()
                isRunning = false
            } else if isRunning == false {
                runTimer()
                isRunning = true
            }
        }
    }
    
    @IBAction func previous(_ sender: UIButton) {
        if workoutCounter > 0 {
            workoutCounter -= 1
            textView.text = "Title: \(usersChoice[workoutCounter].title)\n\nBody Part: \(usersChoice[workoutCounter].bodyPart)\n\nDescription: \(usersChoice[workoutCounter].description)"
            //Reset the timer
            timer.invalidate()
            UpdateWorout()
        }
    }
    
    @IBAction func next(_ sender: UIButton) {
        if workoutCounter < usersChoice.count - 1 {
            workoutCounter += 1
            textView.text = "Title: \(usersChoice[workoutCounter].title)\n\nBody Part: \(usersChoice[workoutCounter].bodyPart)\n\nDescription: \(usersChoice[workoutCounter].description)"
            //Reset the timer
            timer.invalidate()
            UpdateWorout()
        }
    }
    
    //Declare a timer for keeping track of the workout time
    func runTimer () {
        timerLabel.text = "Go!\n\(counter)"
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    //Decrease the timer by one second and print the time to the timer label
    @objc func updateTimer () {
        if counter > 0 {
            counter -= 1
            timerLabel.text = "\(counter)"
        } else {
            timer.invalidate()
            isTimerRunning = false
            UpdateWorout()
        }
    }
    //Declare a timer for keeping track of the workout time
    func countdownTimer () {
        
        timerLabel.text = "Get Ready!\n\(counter)"
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCountdownTimer), userInfo: nil, repeats: true)
    }
    //Decrease the timer by one second and print the time to the timer label
    @objc func updateCountdownTimer () {
        if counter > 0 {
            counter -= 1
            timerLabel.text = "\(counter)"
        } else {
            timer.invalidate()
            isTimerRunning = false
            UpdateWorout()
        }
    }
    func StartWorkout() {
        //start the workout
        counter = 30
        isRunning = true
        self.countdownTimer()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(30), execute: {
            self.timerLabel.text = self.counter.description
            self.timer.invalidate()
            self.counter = 60
            self.runTimer()
            self.workoutCounter += 1
        })
    }
        func UpdateWorout() {
        //Check if there are any workouts left in the array and update the textview information with the workout information
        if workoutCounter < usersChoice.count {
            isTimerRunning = true
            textView.text = "Title: \(usersChoice[workoutCounter].title)\n\nBody Part: \(usersChoice[workoutCounter].bodyPart)\n\nDescription: \(usersChoice[workoutCounter].description)"
            //Reset the timer
            timerLabel.text = "0"
            //Call the startworkout method to move to the next workout if any are left in the array
            StartWorkout()
        }
    }
}
