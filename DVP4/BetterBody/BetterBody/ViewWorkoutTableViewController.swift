//
//  ViewWorkoutTableViewController.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/17/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit
import Firebase

class ViewWorkoutTableViewController: UITableViewController {

    //Declare the arrays needed
    var upperBody: [Workout] = []
    var lowerBody: [Workout] = []
    var totalBody: [Workout] = []
    var cardio: [Workout] = []
    var allWorkouts: [Workout] = []
    var filteredWorkouts = [[Workout](), [Workout](), [Workout](), [Workout]()]
    
    //Set the constant for the database reference
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Better Body"
        
        //Call the functions to load the data from the database
        UpperBody()
        LowerBody()
        TotalBody()
        Cardio()
        
        //Set the header nib identifier and assign it
        let headerNib = UINib.init(nibName: "SecondHeaderView", bundle: nil)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "header_ID2")
        
    }

    // MARK: - Table view data source
    
    //Set header height
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    //Set header view
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header_ID2") as? SecondCustomHeaderView
        
        //Check the description
        switch section {
        case 0:
            header?.titleLabel.text = "Upper Body"
            header?.countLabel.text = upperBody.count.description
        case 1:
            header?.titleLabel.text = "Lower Body"
            header?.countLabel.text = lowerBody.count.description
        case 2:
            header?.titleLabel.text = "Total Body"
            header?.countLabel.text = totalBody.count.description
        case 3:
            header?.titleLabel.text = "Cardio"
            header?.countLabel.text = cardio.count.description
        default:
            print("Oops, this should not happen")
        }
        return header
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filteredWorkouts[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Set up a custom cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellID-01", for: indexPath) as? WorkoutViewCell
            //Use the default if the custom fails
            else {return tableView.dequeueReusableCell(withIdentifier: "CellID-01", for: indexPath)}
        
        //Stored the filtered post in a constant
        let currentWorkout = filteredWorkouts[indexPath.section][indexPath.row]
        
        //Split up the workouts based on the description filter
        cell.titleLabel.text = " \(currentWorkout.title)"
        cell.bodypartLabel.text = " \(currentWorkout.bodyPart)"
        cell.descriptionLabel.text = " \(currentWorkout.description)"
       
        return cell
        
    }
    
    func UpperBody() {
        //Set the upperbody array to the database for upperbody
        db.collection("upperbody").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "upperbody" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.upperBody.append(workoutData)
                    }
                }
                for data in self.upperBody {
                    self.allWorkouts.append(data)
                }
                self.filterPostsByDescription()
                self.tableView.reloadData()
            }
        }
    }
    
    func LowerBody() {
        //Set the lowerbody array to the database for lowerbody
        db.collection("lowerbody").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "lowerbody" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.lowerBody.append(workoutData)
                    }
                }
                for data in self.lowerBody {
                    self.allWorkouts.append(data)
                }
                self.filterPostsByDescription()
                self.tableView.reloadData()
            }
        }
    }
    func TotalBody() {
        //Set the totalbody array to the database for totalbody
        db.collection("totalbody").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "totalbody" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.totalBody.append(workoutData)
                    }
                }
                for data in self.totalBody {
                    self.allWorkouts.append(data)
                }
                self.filterPostsByDescription()
                self.tableView.reloadData()
            }
        }
    }
    func Cardio() {
        //Set the totalbody array to the database for totalbody
        db.collection("cardio").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "cardio" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.cardio.append(workoutData)
                    }
                }
                for data in self.cardio {
                    self.allWorkouts.append(data)
                }
                self.filterPostsByDescription()
                self.tableView.reloadData()
            }
        }
    }
    
    func filterPostsByDescription() {
        //Filter the posts by description
        filteredWorkouts[0] = allWorkouts.filter({ $0.description == "upperbody" })
        filteredWorkouts[1] = allWorkouts.filter({ $0.description == "lowerbody" })
        filteredWorkouts[2] = allWorkouts.filter({ $0.description == "totalbody" })
        filteredWorkouts[3] = allWorkouts.filter({ $0.description == "cardio" })
    }
}
