//
//  SignUpViewController.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/22/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class SignUpViewController: UIViewController, UITextFieldDelegate, GIDSignInUIDelegate {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance()?.uiDelegate = self
        GIDSignIn.sharedInstance()?.delegate = self
        
    }
    
    @IBAction func googleSignIn(_ sender: UIButton) {
        //Call the google signin
        GIDSignIn.sharedInstance()?.signIn()
        
    }
    @IBAction func signUp(_ sender: UIButton) {
        //Capture the data from the text fields
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        guard let username = userNameTextField.text else { return }
        
        //Pass the data into the create user initializer
        CreateUser(withEmail: email, password: password, username: username)
        
        //Call the begin segue if validation is approved
        self.performSegue(withIdentifier: "begin", sender: self)
    }
    
    //Create user function
    func CreateUser(withEmail email: String, password: String, username: String){
        //Pass in the data to the database for verification
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            
            //Save the user id
            guard let uid = result?.user.uid else { return }
            
            //Assign the values of email and username
            let values = ["email": email, "username": username]
            
            //Add the user to the database using the values saved
            Database.database().reference().child("users").child(uid).updateChildValues(values, withCompletionBlock: { (error, ref) in
                if let error = error {
                    print("Failed to update database values with error: ", error.localizedDescription)
                    return
                }
            })
        }
    }
}
extension SignUpViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("Failed to sign in with error:", error)
            return
        }
        
        //Get the authentication and credentials for the database
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        
        //Sign in with the credentials
        Auth.auth().signIn(with: credential) { (result, error) in
            
            
            if let error = error {
                print("Failed to sign in and retrieve data with error:", error)
                return
            }
            
            //Get the userid, email, display name from the google user
            guard let uid = result?.user.uid else { return }
            guard let email = result?.user.email else { return }
            guard let username = result?.user.displayName else { return }
            
            //Save the values for email and username
            let values = ["email": email, "username": username]
            
            //Add the user to the database
            Database.database().reference().child("users").child(uid).updateChildValues(values, withCompletionBlock: { (error, ref) in
                
                self.performSegue(withIdentifier: "begin", sender: self)
                
            })
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Dismiss the keyboard
        self.view.endEditing(true)
    }
}
