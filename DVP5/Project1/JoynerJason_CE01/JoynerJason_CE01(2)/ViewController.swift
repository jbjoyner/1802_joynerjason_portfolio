//
//  ViewController.swift
//  JoynerJason_CE01(2)
//
//  Created by Jason Joyner on 2/8/20.
//  Copyright © 2020 Jason Joyner. All rights reserved.
//

import UIKit
import CoreBluetooth

let service = CBUUID.init(string: "06B280C1-419D-4D87-810E-00D88B506717")
let characteristic = CBUUID.init(string: "CD570797-087C-4008-B692-7835A1246377")

class ViewController: UIViewController , CBPeripheralManagerDelegate {
    
    //Outlets for the buttons to control whether they are enabled or not based on connection
    @IBOutlet var valueButtons: [UIButton]!
    
    //This function checks for the poweredOn state and then adds the service and characteristic and starts advertising
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        if peripheral.state == CBManagerState.poweredOn {
            
            let props : CBCharacteristicProperties = [.read, .notify, .write]
            let permissions : CBAttributePermissions = [.readable, .writeable]
            myChar = CBMutableCharacteristic.init(type: characteristic, properties: props, value: nil, permissions: permissions)
            svc = CBMutableService.init(type: service, primary: true)
            svc?.characteristics = [myChar!]
            pMgr?.add(svc!)
            
            pMgr?.delegate = self
            
            let adData = [CBAdvertisementDataLocalNameKey:"Jason's iPad", CBAdvertisementDataServiceUUIDsKey:[CBUUID.init(string: "06B280C1-419D-4D87-810E-00D88B506717")]] as [String:Any]
            pMgr?.startAdvertising(adData)
        }
    }
    
    //This function responds to receiveWrite request and updates the value
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
        if let req = requests.first {
            myVal = req.value
            peripheral.respond(to: req, withResult: .success)
        }
    }
    //This function responds to didSubscribe request, starts the timer to perform the updates and makes the buttons enabled
    func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didSubscribeTo characteristic: CBCharacteristic) {
        Timer.scheduledTimer(withTimeInterval: 5, repeats: true) {(timer) in peripheral.updateValue(self.myVal!, for: self.myChar!, onSubscribedCentrals: nil)
            for index in 0...5 {
                self.valueButtons[index].isEnabled = true
            }
        }
    }
    
    //This function responds to receiveRead request and updates the value
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest) {
        request.value = myVal
        peripheral.respond(to: request, withResult: .success)
    }
//
//    func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?) {
//        print("Added Service")
//    }
//
//    func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
//        print("Advertising")
//    }
//
    //Variables to hold the PeripheralManager, service, value and characteristics
    var pMgr : CBPeripheralManager?
    var svc : CBMutableService?
    var myVal : Data? = "initial".data(using: .ascii)
    var myChar : CBMutableCharacteristic?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pMgr = CBPeripheralManager.init(delegate: self, queue: nil)
    }
    
    //Actions for the buttons to update the values
    @IBAction func button1(_ sender: UIButton) {
        myVal = "1".data(using: .ascii)
    }
    @IBAction func button2(_ sender: UIButton) {
        myVal = "2".data(using: .ascii)
    }
    @IBAction func button3(_ sender: UIButton) {
        myVal = "3".data(using: .ascii)
    }
    @IBAction func button4(_ sender: UIButton) {
        myVal = "4".data(using: .ascii)
    }
    @IBAction func button5(_ sender: UIButton) {
        myVal = "5".data(using: .ascii)
    }
    @IBAction func button6(_ sender: UIButton) {
        myVal = "6".data(using: .ascii)
    }
}

