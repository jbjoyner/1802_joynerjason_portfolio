//
//  UserWorkout+CoreDataProperties.swift
//  
//
//  Created by Jason on 5/30/19.
//
//

import Foundation
import CoreData


extension UserWorkout {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserWorkout> {
        return NSFetchRequest<UserWorkout>(entityName: "UserWorkout")
    }

    @NSManaged public var title: String?
    @NSManaged public var bodyPart: String?
    @NSManaged public var workoutDescription: String?

}
