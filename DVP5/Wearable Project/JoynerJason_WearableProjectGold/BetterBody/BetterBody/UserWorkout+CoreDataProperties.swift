//
//  UserWorkout+CoreDataProperties.swift
//  
//
//  Created by Jason Joyner on 5/19/19.
//
//

import Foundation
import CoreData


extension UserWorkout {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserWorkout> {
        return NSFetchRequest<UserWorkout>(entityName: "UserWorkout")
    }

    @NSManaged public var array: NSObject?

}
