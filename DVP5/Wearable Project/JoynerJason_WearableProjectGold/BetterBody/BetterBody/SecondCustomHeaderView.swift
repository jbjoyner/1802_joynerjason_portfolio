//
//  SecondCustomHeaderView.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/24/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit

class SecondCustomHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var countLabel: UILabel!
  
}
