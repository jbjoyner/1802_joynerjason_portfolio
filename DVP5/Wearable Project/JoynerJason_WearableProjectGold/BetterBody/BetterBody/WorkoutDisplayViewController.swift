//
//  WorkoutDisplayViewController.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/17/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit
import WatchConnectivity

class WorkoutDisplayViewController: UIViewController, WCSessionDelegate {
    
    //Declare the arrays needed
    var upperBody: [Workout] = []
    var lowerBody: [Workout] = []
    var totalBody: [Workout] = []
    var cardio: [Workout] = []
    var usersChoice: [Workout] = []
    
    //Set the counters needed
    var counter = 0
    var workoutCounter = 0
    
    //Set the istimerrunning boolean
    var isTimerRunning = false
    
    //Declare the timer
    var timer = Timer()
    
    //Set the isrunning boolean
    var isRunning = false
    
    //Set the boolean to check for the watch
    var isPhoneReady = false
    
    //Set the message dictionary
    var message:[String:Any] {
        return["Start":"Start", "Pause":"Pause", "Next":"Next", "Previous":"Previous"]
    }
    
    let session = WCSession.default
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var pauseButton: UIButton!
    
    func interactiveMessage() {
        session.sendMessage(message, replyHandler: nil, errorHandler: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the tableview title to the app name
        title = "Better Body"
        
        // Do any additional setup after loading the view.
        
        //Set the textview with the workout information
        textView.text = "Title: \(usersChoice[workoutCounter].title)\n\nBody Part: \(usersChoice[workoutCounter].bodyPart)\n\nDescription: \(usersChoice[workoutCounter].description)"
        timerLabel.text = ""
        
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }
    
    @IBAction func start(_ sender: UIButton) {
        //Set the counter to the zero and call the updateworkout method
        startWorkout()
    }
    
    @IBAction func pause(_ sender: UIButton) {
        //Check for the conditions to pause the app if it has been started and block if it has not
        pauseWorkout()
    }
    
    @IBAction func previous(_ sender: UIButton) {
        previousWorkout()
    }
    
    @IBAction func next(_ sender: UIButton) {
        nextWorkout()
    }
    
    //Declare a timer for keeping track of the workout time
    func runTimer () {
        DispatchQueue.main.async {
            self.timerLabel.text = "Go!\n\(self.counter)"
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        }
    }
    
    //Decrease the timer by one second and print the time to the timer label
    @objc func updateTimer () {
        if counter > 0 {
            counter -= 1
            timerLabel.text = "\(counter)"
        } else {
            timer.invalidate()
            isTimerRunning = false
            UpdateWorout()
        }
    }
    
    //Declare a timer for keeping track of the workout time
    func countdownTimer () {
        DispatchQueue.main.async {
            self.timerLabel.text = "Get Ready!\n\(self.counter)"
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountdownTimer), userInfo: nil, repeats: true)
        }
    }
    
    //Decrease the timer by one second and print the time to the timer label
    @objc func updateCountdownTimer () {
        if counter > 0 {
            counter -= 1
            timerLabel.text = "\(counter)"
        } else {
            timer.invalidate()
            isTimerRunning = false
            UpdateWorout()
        }
    }
    
    func StartWorkout() {
        //start the workout
        counter = 30
        isRunning = true
        self.countdownTimer()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(30), execute: {
            self.timerLabel.text = self.counter.description
            self.timer.invalidate()
            self.counter = 60
            self.runTimer()
            self.workoutCounter += 1
        })
    }
    
    func UpdateWorout() {
        //Check if there are any workouts left in the array and update the textview information with the workout information
        if isTimerRunning == false && isRunning == true {
            if workoutCounter < usersChoice.count {
                isTimerRunning = true
                DispatchQueue.main.async {
                    self.textView.text = "Title: \(self.usersChoice[self.workoutCounter].title)\n\nBody Part: \(self.usersChoice[self.workoutCounter].bodyPart)\n\nDescription: \(self.usersChoice[self.workoutCounter].description)"
                    //Reset the timer
                    self.timerLabel.text = "0"
                }
                //Call the startworkout method to move to the next workout if any are left in the array
                timer.invalidate()
                StartWorkout()
            }
        }
    }
    
    //MARK: - Delegates
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        switch activationState {
        case .activated:
            isPhoneReady = true
            session.sendMessage(["Ready":true], replyHandler: nil, errorHandler: nil)
            print("Phone WCSession Activated")
        case .inactive:
            print("Phone WCSession Inactive")
        case .notActivated:
            print("Phone WCSession Not Activated")
        default:
            print("Phone WCSession Not Activated")
        }
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("Session went inactive")
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print("Session deactivated")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        if let doStart = message["Start"] as? Bool {
            if doStart {
                if isPhoneReady == true {
                startWorkout()
                }
            }
        }
        
        if let doPause = message["Pause"] as? Bool {
            if doPause {
                if isPhoneReady == true {
                pauseWorkout()
                }
            }
        }
        
        if let doNext = message["Next"] as? Bool {
            if doNext {
                if isPhoneReady == true {
                nextWorkout()
                }
            }
        }
        
        if let doPrevious = message["Previous"] as? Bool {
            if doPrevious {
                if isPhoneReady == true {
                previousWorkout()
                }
            }
        }
        
        if let isReady = message["Ready"] as? Bool {
            if isReady {
                session.sendMessage(["Ready":true], replyHandler: nil, errorHandler: nil) 
            }
        }
        replyHandler(message)
    }
    
    func startWorkout() {
        if isTimerRunning == false && workoutCounter < usersChoice.count {
            workoutCounter = 0
            isRunning = true
            UpdateWorout()
            interactiveMessage()
        }
    }
    
    func pauseWorkout() {
        if isTimerRunning == true{
            if isRunning == true {
                timer.invalidate()
                isRunning = false
                interactiveMessage()
            } else if isRunning == false {
                runTimer()
                isRunning = true
                interactiveMessage()
            }
        }
    }
    
    func nextWorkout() {
        if workoutCounter < usersChoice.count - 1 {
            workoutCounter += 1
            DispatchQueue.main.async {
                self.textView.text = "Title: \(self.usersChoice[self.workoutCounter].title)\n\nBody Part: \(self.usersChoice[self.workoutCounter].bodyPart)\n\nDescription: \(self.usersChoice[self.workoutCounter].description)"
                //Reset the timer
                self.timer.invalidate()
                self.isTimerRunning = false
                self.UpdateWorout()
                self.interactiveMessage()
            }
        }
    }
    
    func previousWorkout() {
        if workoutCounter > 0 {
            workoutCounter -= 1
            DispatchQueue.main.async {
                self.textView.text = "Title: \(self.usersChoice[self.workoutCounter].title)\n\nBody Part: \(self.usersChoice[self.workoutCounter].bodyPart)\n\nDescription: \(self.usersChoice[self.workoutCounter].description)"
                //Reset the timer
                self.timer.invalidate()
                self.isTimerRunning = false
                self.UpdateWorout()
                self.interactiveMessage()
            }
        }
    }
}

