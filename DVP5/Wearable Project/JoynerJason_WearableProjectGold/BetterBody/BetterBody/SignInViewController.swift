//
//  SignInViewController.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/21/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FirebaseUI

class SignInViewController: UIViewController, UITextFieldDelegate, GIDSignInUIDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance()?.signInSilently()
        GIDSignIn.sharedInstance()?.delegate = self
        
    }
    
    @IBAction func signIn(_ sender: UIButton) {
        //Call the google signin
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func emailSignIn(_ sender: UIButton) {
        //Capture the data from the text fields
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        //Pass in the values to the loguserin method
        logUserIn(withEmail: email, password: password)
    }
    
    //Loguserin function
    func logUserIn(withEmail email: String, password: String) {
        //Pass the data in to the database for verification
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            
            if let error = error {
                let errorMessage = UIAlertController(title: "Error", message: "Wrong username/password or the user does not exist.", preferredStyle: UIAlertController.Style.alert)
                
                //Add the ok button
                errorMessage.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    errorMessage.dismiss(animated: true, completion: nil)
                }))
                //Present the alert
                self.present(errorMessage, animated: true, completion: nil)
                
                print("Failed to sign user in with error: ", error.localizedDescription)
                return
            } else {
                //If everything passes, go the main scrren
                self.performSegue(withIdentifier: "begin", sender: self)
                
                //Clear the textfields
                self.emailTextField.text = nil
                self.passwordTextField.text = nil
            }
        }  
    }
}

extension SignInViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("Failed to sign in with error:", error)
            return
        }
        
        //Get the google authentication and credentials
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        
        //Sign in with the credentials
        Auth.auth().signIn(with: credential) { (result, error) in
            
            
            if let error = error {
                print("Failed to sign in and retrieve data with error:", error)
                return
            }
            
            //Get the uid, email and display name from google
            guard let uid = result?.user.uid else { return }
            guard let email = result?.user.email else { return }
            guard let username = result?.user.displayName else { return }
            
            //Set the value to the email and username
            let values = ["email": email, "username": username]
            
            //Pass the data into the database
            Database.database().reference().child("users").child(uid).updateChildValues(values, withCompletionBlock: { (error, ref) in
                
                self.performSegue(withIdentifier: "begin", sender: self)
                
            })
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Dismiss the keyboard
        self.view.endEditing(true)
    }
}
