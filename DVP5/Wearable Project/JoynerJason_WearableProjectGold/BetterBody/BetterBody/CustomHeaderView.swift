//
//  CustomHeaderView.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/23/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit

class CustomHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
  
}
