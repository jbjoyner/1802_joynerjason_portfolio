//
//  TimerInterfaceController.swift
//  BetterBodyWatch Extension
//
//  Created by Jason Joyner on 2/27/20.
//  Copyright © 2020 Jason Joyner. All rights reserved.
//

import Foundation
import WatchKit

class TimerInterfaceController: InterfaceController {
    
    
    @IBOutlet weak var countDownTimer: WKInterfaceTimer!
    @IBOutlet weak var buttonText: WKInterfaceButton!
    
    var isTimerRunning: Bool = false
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        buttonText.setTitle("Start")
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    @IBAction func startTimerButton() {
        if isTimerRunning == false {
        countDownTimer.setDate(NSDate(timeIntervalSinceNow: 1800) as Date)
        countDownTimer.start()
            isTimerRunning = true
            buttonText.setTitle("Stop")
        } else {
            countDownTimer.stop()
            isTimerRunning = false
            buttonText.setTitle("Start")
        }
        
    }
}
