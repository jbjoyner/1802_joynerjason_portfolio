//
//  InterfaceController.swift
//  BetterBodyWatch Extension
//
//  Created by Jason Joyner on 2/18/20.
//  Copyright © 2020 Jason Joyner. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    @IBOutlet weak var statusLabel: WKInterfaceLabel!
    @IBOutlet weak var timer: WKInterfaceTimer!
    @IBOutlet weak var startButtonEnabled: WKInterfaceButton!
    @IBOutlet weak var pauseButtonEnabled: WKInterfaceButton!
    @IBOutlet weak var previousButtonEnabled: WKInterfaceButton!
    @IBOutlet weak var nextButtonEnabled: WKInterfaceButton!
    
    @IBAction func startButton() {
        if WCSession.default.isReachable {
            WCSession.default.sendMessage(["Start":true], replyHandler: { (reply) in
                if let didStart = reply["Start"] as? Bool {
                    if didStart {
                        self.statusLabel.setText("Workout Started")
                        self.timer.start()
                    }
                }
            }) { (error) in
                print("Error:\(error.localizedDescription)")
            }
        }
    }
    
    @IBAction func pauseButton() {
        if WCSession.default.isReachable {
            WCSession.default.sendMessage(["Pause":true], replyHandler: { (reply) in
                if let didPause = reply["Paused"] as? Bool {
                    if didPause {
                        self.statusLabel.setText("Workout Paused")
                    }
                }
            }) { (error) in
                print("Error:\(error.localizedDescription)")
            }
        }
    }
    
    @IBAction func nextButton() {
        if WCSession.default.isReachable {
            WCSession.default.sendMessage(["Next":true], replyHandler: { (reply) in
                if let didNext = reply["Next"] as? Bool {
                    if didNext {
                        self.statusLabel.setText("Next Workout")
                    }
                }
            }) { (error) in
                print("Error:\(error.localizedDescription)")
            }
        }
    }
    
    @IBAction func previousButton() {
        if WCSession.default.isReachable {
            WCSession.default.sendMessage(["Previous":true], replyHandler: { (reply) in
                if let didPrevious = reply["Previous"] as? Bool {
                    if didPrevious {
                        self.statusLabel.setText("Previous Workout")
                    }
                }
            }) { (error) in
                print("Error:\(error.localizedDescription)")
            }
        }
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        let session = WCSession.default
        session.delegate = self
        session.activate()
     
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        self.startButtonEnabled.setEnabled(true)
        self.pauseButtonEnabled.setEnabled(true)
        self.previousButtonEnabled.setEnabled(true)
        self.nextButtonEnabled.setEnabled(true)
    }
    
    //MARK: - Delegates
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        switch activationState {
        case .activated:
            print("Watch WCSession Activated")
        case .inactive:
            print("Watch WCSession Inactive")
        case .notActivated:
            print("Watch WCSession Not Activated")
        default:
            print("Watch WCSession Not Activated")
        }
    }
    @IBAction func timerButton() {
        pushController(withName: "TimerInterfaceController", context: nil)
    }
}
