//
//  ChooseViewController.swift
//  
//
//  Created by Jason Joyner on 5/18/19.
//

import UIKit
import Firebase
import CoreData

class ChooseViewController: UIViewController {

    //Declare the arrays needed to hold the workout data
    var upperBody: [Workout] = []
    var lowerBody: [Workout] = []
    var totalBody: [Workout] = []
    var cardio: [Workout] = []
    var custom: [Workout] = []
    var usersChoice: [Workout] = []
    var allWorkouts: [Workout] = []
    
    //Set a constant for firestore reference
    let db = Firestore.firestore()
    
    //Set the constants for the coredata
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //Call the methods to load the data from the database to the arrays
        UpperBody()
        LowerBody()
        TotalBody()
        Cardio()
        
        //Load the coredata to the app
        LoadCustom()
    }
    
    @IBAction func buttonPress(_ sender: UIButton) {
        
        //Check for the tag of the button press to assign the correct array to the userchoice array
        switch sender.tag {
        case 0:
            usersChoice = upperBody
        case 1:
            usersChoice = lowerBody
        case 2:
            usersChoice = totalBody
        case 3:
            usersChoice = cardio
        case 4:
            usersChoice = custom
        default:
            print("Something went wrong")
        }
        //Segue to the plan tableview controller
        performSegue(withIdentifier: "plan", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //Set the constant for the destination
        let destination = segue.destination as! PlanTableViewController
        
        //Assign the userchoice array to the destination userchoice array
        destination.usersChoice = usersChoice
    }
    
    func UpperBody() {
        
        //Load the upperbody data from the database to the upperbody array
        db.collection("upperbody").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                //Loop through the array retreived and add the data to the workout class object and save it to the correct array
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "upperbody" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.upperBody.append(workoutData)
                    }
                }
                for data in self.upperBody {
                    //Load the data from the upperbody array to the allworkouts array
                    self.allWorkouts.append(data)
                }
            }
        }
    }
    
    func LowerBody() {
        //Load the lowerbody data from the database to the lowerbody array
        db.collection("lowerbody").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                //Loop through the array retreived and add the data to the workout class object and save it to the correct array
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "lowerbody" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.lowerBody.append(workoutData)
                    }
                }
                for data in self.lowerBody {
                    //Load the data from the lowerbody array to the allworkouts array
                    self.allWorkouts.append(data)
                }
            }
        }
    }
    
    func TotalBody() {
        //Load the totalbody data from the database to the totalbody array
        db.collection("totalbody").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                //Loop through the array retreived and add the data to the workout class object and save it to the correct array
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "totalbody" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.totalBody.append(workoutData)
                    }
                }
                for data in self.totalBody {
                    //Load the data from the totalbody array to the allworkouts array
                    self.allWorkouts.append(data)
                }
            }
        }
    }
    
    func Cardio() {
        //Load the cardi0 data from the database to the cardio array
        db.collection("cardio").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                //Loop through the array retreived and add the data to the workout class object and save it to the correct array
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let title = data["title"] as? String ?? ""
                    let bodyPart = data["bodyPart"] as? String ?? ""
                    let description = data["description"] as? String ?? ""
                    print("\(document.documentID) => \(document.data())")
                    print("\(title) \(bodyPart) \(description)")
                    if description == "cardio" {
                        let  workoutData = Workout.init(title: title, bodyPart: bodyPart, description: description)
                        self.cardio.append(workoutData)
                    }
                }
                for data in self.cardio {
                    //Load the data from the cardio array to the allworkouts array
                    self.allWorkouts.append(data)
                }
            }
        }
    }
    
    func LoadCustom(){
        let data = NSFetchRequest<NSFetchRequestResult>(entityName: "UserWorkout")
        do {
            // We will receive an array of workouts saved
            if let workoutArray = try context.fetch(data) as? [UserWorkout] {
                print(workoutArray.count)
                // lets test to see what has been saved
                for workout in workoutArray {
                    if let wTitle = workout.title, let wBodyPart = workout.bodyPart, let wDescription = workout.workoutDescription {
                        let savedWorkout = Workout.init(title: wTitle, bodyPart: wBodyPart, description: wDescription)
                        custom.append(savedWorkout)
                        print(wTitle)
                    }
                }
            }
        } catch {
            print("Fetching data Failed", error.localizedDescription)
        }
    }
}
