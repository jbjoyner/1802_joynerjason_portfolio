//
//  ThirdCustomHeader.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/26/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit

class ThirdCustomHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var countLabel: UILabel!
   
}
