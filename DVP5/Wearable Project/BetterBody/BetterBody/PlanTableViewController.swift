//
//  PlanTableViewController.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/18/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit

class PlanTableViewController: UITableViewController {

    //Declare the arrays needed
    var allWorkouts:[Workout] = []
    var usersChoice: [Workout] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Set the title of the tableview to the app name
        title = "Better Body"
        
        
        //Set the header nib identifier and assign it
        let headerNib = UINib.init(nibName: "ThirdHeaderView", bundle: nil)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "header_ID3")
    }

    // MARK: - Table view data source
    
    //Set header height
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 120
    }
    
    //Set header view
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header_ID3") as? ThirdCustomHeader
        
        header?.titleLabel.text = "Tap on any workout to begin."
        header?.countLabel.text = "Number of workouts: \(usersChoice.count.description)"
        return header
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return usersChoice.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Set up a custom cell
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellID-01", for: indexPath) as? PlanCell{
            cell.titleLabel.text = " \(usersChoice[indexPath.row].title)"
            cell.bodyPartLabel.text = " \(usersChoice[indexPath.row].bodyPart)"
            cell.descriptionLabel.text = " \(usersChoice[indexPath.row].description)"
            return cell
        } else {
            //If the setup fails, use the default
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellID-01", for: indexPath)
            cell.textLabel?.text = " \(usersChoice[indexPath.row].title)"
            cell.detailTextLabel?.text = " \(usersChoice[indexPath.row].bodyPart)"
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Move to the workout view controller if cells are touched
        performSegue(withIdentifier: "workout", sender: indexPath)
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as? WorkoutDisplayViewController
        
        //Pass the userchoice array to the workout view controller
        destination?.usersChoice = usersChoice
    }
}
