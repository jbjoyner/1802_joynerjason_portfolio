//
//  CreateCell.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/23/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit

class CreateCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodypartLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
