//
//  Workouts.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/17/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import Foundation

class Workout {
    var title: String
    var bodyPart: String
    var description: String
    
    init(title: String, bodyPart: String, description: String) {
        self.title = title
        self.bodyPart = bodyPart
        self.description = description
    }
}
