//
//  AddViewController.swift
//  BetterBody
//
//  Created by Jason Joyner on 5/17/19.
//  Copyright © 2019 Jason Joyner. All rights reserved.
//

import UIKit
import Firebase
import CoreData

class AddViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource {
    
    var customWorkout: [Workout] = []
    var allWorkouts: [Workout] = []
    let descriptions = ["upperbody", "lowerbody", "totalbody", "cardio"]
    let defaults = UserDefaults.standard
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return descriptions[row]
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return descriptions.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        workoutDescription.text = descriptions[row]
    }
    
    var ref: DatabaseReference!
    let db = Firestore.firestore()
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var workoutTitle: UITextField!
    @IBOutlet weak var bodyPart: UITextField!
    @IBOutlet weak var workoutDescription: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Better Body"
        textView.text = nil
        
        // Do any additional setup after loading the view.
        
        ref = Database.database().reference()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customWorkout.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        // Configure the cell...
        
        cell.textLabel?.text = customWorkout[indexPath.row].title
        cell.detailTextLabel?.text = customWorkout[indexPath.row].bodyPart
        
        
        return cell
    }
    
    @IBAction func addWorkout(_ sender: UIButton) {
        
        //Grabs the data from the textfields and saves them to the database based on the description text
        if let wText = workoutTitle.text, let bPart = bodyPart.text, let dScription = workoutDescription.text {
            let newWorkout = Workout.init(title: wText, bodyPart: bPart, description: dScription)
            
            //Check to make sure no fields are left blank
            if wText != "" && bPart != "" && dScription != "" {
                
                //Add the data to the customworkout array
                customWorkout.append(newWorkout)
                
                //Switch on the description
                switch dScription {
                    
                case "upperbody":
                    // Add a new document with a generated ID
                    var ref: DocumentReference? = nil
                    ref = db.collection("upperbody").addDocument(data: [
                        "title": wText,
                        "bodyPart": bPart,
                        "description": dScription
                    ]) { err in
                        if let err = err {
                            print("Error adding document: \(err)")
                        } else {
                            print("Document added with ID: \(ref!.documentID)")
                        }
                    }
                case "lowerbody":
                    // Add a new document with a generated ID
                    var ref: DocumentReference? = nil
                    ref = db.collection("lowerbody").addDocument(data: [
                        "title": wText,
                        "bodyPart": bPart,
                        "description": dScription
                    ]) { err in
                        if let err = err {
                            print("Error adding document: \(err)")
                        } else {
                            print("Document added with ID: \(ref!.documentID)")
                        }
                    }
                case "totalbody":
                    // Add a new document with a generated ID
                    var ref: DocumentReference? = nil
                    ref = db.collection("totalbody").addDocument(data: [
                        "title": wText,
                        "bodyPart": bPart,
                        "description": dScription
                    ]) { err in
                        if let err = err {
                            print("Error adding document: \(err)")
                        } else {
                            print("Document added with ID: \(ref!.documentID)")
                        }
                    }
                case "cardio":
                    // Add a new document with a generated ID
                    var ref: DocumentReference? = nil
                    ref = db.collection("cardio").addDocument(data: [
                        "title": wText,
                        "bodyPart": bPart,
                        "description": dScription
                    ]) { err in
                        if let err = err {
                            print("Error adding document: \(err)")
                        } else {
                            print("Document added with ID: \(ref!.documentID)")
                        }
                    }
                default:
                    print("Something went wrong")
                }
                
                //Calls the displaynewworkout method to show the data that was just added
                DisplayNewWorkout()
                workoutTitle.text = nil
                bodyPart.text = nil
                workoutDescription.text = nil
            }
        }
    }
    
    func DisplayNewWorkout() {
        
        var textToDisplay: String = ""
        
        for item in customWorkout {
            let tempText = "Title: \(item.title)\nBody Part: \(item.bodyPart)\nDescription: \(item.description)\n\n"
            textToDisplay.append(tempText)
        }
        //Add the new data to the custom string for display
        textView.text = textToDisplay
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Dismiss the keyboard
        self.view.endEditing(true)
    }
}
