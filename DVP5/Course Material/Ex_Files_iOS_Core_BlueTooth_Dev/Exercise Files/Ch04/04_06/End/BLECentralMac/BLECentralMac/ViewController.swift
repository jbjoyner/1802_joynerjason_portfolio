//
//  ViewController.swift
//  BLECentralMac
//
//  Created by Bear Cahill on 2/14/18.
//  Copyright © 2018 Bear Cahill. All rights reserved.
//

import Cocoa

import CoreBluetooth

let svcLight = CBUUID.init(string: "F000AA70-0451-4000-B000-000000000000")
let charLightConfig = CBUUID.init(string: "F000AA72-0451-4000-B000-000000000000")
let charLightData = CBUUID.init(string: "F000AA71-0451-4000-B000-000000000000")

let svcIO = CBUUID.init(string: "F000AA64-0451-4000-B000-000000000000")
let charIOConfig = CBUUID.init(string: "F000AA66-0451-4000-B000-000000000000")
let charIOData = CBUUID.init(string: "F000AA65-0451-4000-B000-000000000000")

let svcKeys = CBUUID.init(string: "FFE0")
let charKeys = CBUUID.init(string: "FFE1")

class ViewController: NSViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var charIO : CBCharacteristic?
    var ioVal : UInt8 = 0x0
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state.rawValue == 5 {
            if let pUUIDString = UserDefaults.standard.object(forKey: "PUUID") as? String {
                if let pUUID = UUID.init(uuidString: pUUIDString) {
                    let peripherals = centralManager.retrievePeripherals(withIdentifiers: [pUUID])
                    if let p = peripherals.first {
                        connect(toPeripheral: p)
                        return
                    }
                }
            }
            
            let peripherals = centralManager.retrieveConnectedPeripherals(withServices: [CBUUID.init(string: "AA80")])
            if let p = peripherals.first {
                connect(toPeripheral: p)
                return
            }
            
            central.scanForPeripherals(withServices: [CBUUID.init(string: "AA80")], options: nil)
            print ("scanning...")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if peripheral.name?.contains("SensorTag") == true {
            print (advertisementData)
            connect(toPeripheral: peripheral)
        }
    }
    
    func connect(toPeripheral : CBPeripheral) {
        print (toPeripheral.name ?? "no name")
        centralManager.stopScan()
        centralManager.connect(toPeripheral, options: nil)
        myPeripheral = toPeripheral
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        central.scanForPeripherals(withServices: [CBUUID.init(string: "AA80")], options: nil)
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        central.scanForPeripherals(withServices: [CBUUID.init(string: "AA80")], options: nil)
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print ("connected \(peripheral.name)")
        peripheral.discoverServices(nil)
        peripheral.delegate = self
//        UserDefaults.standard.set(peripheral.identifier.uuidString, forKey: "PUUID")
//        UserDefaults.standard.synchronize()
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if let services = peripheral.services {
            for svc in services {
                if svc.uuid == svcLight {
                    print (svc.uuid.uuidString)
                    peripheral.discoverCharacteristics(nil, for: svc)
                }
                else if svc.uuid == svcKeys {
                    peripheral.discoverCharacteristics(nil, for: svc)
                }
                else if svc.uuid == svcIO {
                    peripheral.discoverCharacteristics(nil, for: svc)
                }
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if let chars = service.characteristics {
            for char in chars {
                print (char.uuid.uuidString)
                if char.uuid == charLightConfig {
                    if char.properties.contains(CBCharacteristicProperties.writeWithoutResponse) {
                        peripheral.writeValue("new val".data(using: .ascii)!, for: char, type: CBCharacteristicWriteType.withoutResponse)
                    }
                    else {
                        peripheral.writeValue("new val".data(using: .ascii)!, for: char, type: CBCharacteristicWriteType.withResponse)
                    }
//                    peripheral.readValue(for: char)
                    peripheral.setNotifyValue(true, for: char)
                }
                else if char.uuid == charIOConfig {
                    if char.properties.contains(CBCharacteristicProperties.writeWithoutResponse) {
                        peripheral.writeValue(Data.init(bytes: [01]), for: char, type: CBCharacteristicWriteType.withoutResponse)
                    }
                    else {
                        peripheral.writeValue(Data.init(bytes: [01]), for: char, type: CBCharacteristicWriteType.withResponse)
                    }
                }
                else if char.uuid == charIOData {
                    charIO = char
                    ioVal = 0x07
                    toggleIO()
                }
                else if char.uuid == charLightData {
                    checkLight(curChar: char)
                }
                else if char.uuid == charKeys {
                    peripheral.setNotifyValue(true, for: char)
                }
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print ("wrote value")
    }
    
    func dataToUnsignedBytes16(value : Data) -> [UInt16] {
        let count = value.count
        var array = [UInt16](repeating: 0, count: count)
        (value as NSData).getBytes(&array, length:count * MemoryLayout<UInt16>.size)
        return array
    }
    
    func luxConvert(value : Data) -> Double {
        let rawData = dataToUnsignedBytes16(value: value)
        var e :UInt16 = 0
        var m :UInt16 = 0
        
        m = rawData[0] & 0x0FFF;
        e = (rawData[0] & 0xF000) >> 12;
        
        /** e on 4 bits stored in a 16 bit unsigned => it can store 2 << (e - 1) with e < 16 */
        e = (e == 0) ? 1 : 2 << (e - 1);
        
        return Double(m) * (0.01 * Double(e));
    }
    
    func checkLight(curChar : CBCharacteristic) {
        Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true) { (timer) in
            self.myPeripheral?.readValue(for: curChar)
        }
    }
    
    func toggleIO() {
        ioVal += 0x01
        if ioVal == 0x08 { ioVal = 0x0 }
        print (ioVal)
        let data = Data.init(bytes: [ioVal])
        if charIO?.properties.contains(CBCharacteristicProperties.writeWithoutResponse) == true {
            myPeripheral?.writeValue(data, for: charIO!, type: CBCharacteristicWriteType.withoutResponse)
        }
        else {
            myPeripheral?.writeValue(data, for: charIO!, type: CBCharacteristicWriteType.withResponse)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if let val = characteristic.value {
            if characteristic.uuid == charLightConfig {
                print (String.init(data: val as Data, encoding: .ascii) ?? "no val")
            }
            if characteristic.uuid == charLightData {
                let luxVal = luxConvert(value: val)
                print (luxVal)
            }
            else if characteristic.uuid == charKeys {
                print ("keys: \([UInt8](val))")
                if val != Data.init(bytes: [0x0]) {
                    toggleIO()
                }
            }
        }
    }
    
    var centralManager : CBCentralManager!
    var myPeripheral : CBPeripheral?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        centralManager = CBCentralManager.init(delegate: self, queue: nil)
    }
    
    
    
}

