//
//  ViewController.swift
//  BLEiOSPeripheral
//
//  Created by Bear Cahill on 2/14/18.
//  Copyright © 2018 Bear Cahill. All rights reserved.
//

import UIKit
import CoreBluetooth

let svcLight = CBUUID.init(string: "F000AA70-0451-4000-B000-000000000000")
let charLightConfig = CBUUID.init(string: "F000AA72-0451-4000-B000-000000000000")


class ViewController: UIViewController, CBPeripheralManagerDelegate {
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        if peripheral.state == CBManagerState.poweredOn {
            svc = CBMutableService.init(type: svcLight, primary: true)
            pMgr?.add(svc!)
            
            pMgr?.delegate = self
            
            let adData = [CBAdvertisementDataLocalNameKey:"Not SensorTag", CBAdvertisementDataServiceUUIDsKey:[CBUUID.init(string: "AA80")]] as [String:Any]
            pMgr?.startAdvertising(adData)
        }
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?) {
        print ("added service")
    }
    
    func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
        print ("advertising")
    }
    

    var pMgr : CBPeripheralManager?
    var svc : CBMutableService?
    var myVal : Data? = "initial".data(using: .ascii)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        pMgr = CBPeripheralManager.init(delegate: self, queue: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

