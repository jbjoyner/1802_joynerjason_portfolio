//
//  AppDelegate.swift
//  BLECentralMac
//
//  Created by Bear Cahill on 2/14/18.
//  Copyright © 2018 Bear Cahill. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

