func dataToUnsignedBytes16(value : Data) -> [UInt16] {
    let count = value.count
    var array = [UInt16](repeating: 0, count: count)
    (value as NSData).getBytes(&array, length:count * MemoryLayout<UInt16>.size)
    return array
}

func luxConvert(value : Data) -> Double {
    let rawData = dataToUnsignedBytes16(value: value)
    var e :UInt16 = 0
    var m :UInt16 = 0
    
    m = rawData[0] & 0x0FFF;
    e = (rawData[0] & 0xF000) >> 12;
    
    /** e on 4 bits stored in a 16 bit unsigned => it can store 2 << (e - 1) with e < 16 */
    e = (e == 0) ? 1 : 2 << (e - 1);
    
    return Double(m) * (0.01 * Double(e));
}
