//
//  AppDelegate.swift
//  1702BluetoothTest
//
//  Created by Joseph Sheckels on 2/2/17.
//  Copyright © 2017 FullSail. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

