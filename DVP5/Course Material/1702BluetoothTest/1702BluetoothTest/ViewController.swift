//
//  ViewController.swift
//  1702BluetoothTest
//
//  Created by Joseph Sheckels on 2/2/17.
//  Copyright © 2017 FullSail. All rights reserved.
//

//--1 For this demonstration we're creating a Central side object in a Bluetooth Low Energy connection. The purpose of this demo is to show you the basic functionality that my app will be using to find and connect to your periperal side in Lab 2. You will notice too that this app was written as an OSX application and not an iOS one. This is because the simulator can not access the Bluetooth radio on your Macbook for testing. You can only do that on real hardware such as your iPad.

//  To start we'll import the CoreBluetooth framework into our file. Be sure to add the CoreBluetooth.framework to your project before attempting to use any of the CB prefix classes.
import Cocoa
import CoreBluetooth

//--2 We need our class to be a CBCentralManagerDelegate in order to find and make the connection with a peripheral device and the CBPeripheralDelegate to recieve callbacks from a connected peripheral such and responses to requests for the peripheral's services, characteristics, or when the peripheral updates a characteristic that this central is subscribed to.
class ViewController: NSViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    //Test UUIDs for finding specific services and characteristics. We typically use these in order to prevent methods like didDiscover from returning every single Bluetooth device in range of ours.
    let TRANSFER_SERVICE_UUID = "FB694B90-F49E-4597-8306-171BBA78F846"
    let TRANSFER_CHARACTERISTIC_UUID = "EB6727C4-F184-497A-A656-76B0CDAC633A"
    
    //The CBCentralManager is the main member we'll use for this class to find and communicate with peripheral objects. If we were a peripheral we would instead use a CBPeripheralManager.
    var centralManager: CBCentralManager?
    //An instance of a peripheral object. We hang on to it once we connect so we can utilize it's data whereever we need to.
    var discoveredPeripheral: CBPeripheral?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initialize our central manager and set the delegate. We set the queue to nil in order to use the default queue but this can be subsituted for your own queue based on the application's needs.
        centralManager = CBCentralManager(delegate: self, queue: nil)

        // Do any additional setup after loading the view.
    }
    
    //--3 This method gets called automatically once we initialize our central manager. It helps us determine the current state of our Bluetooth hardware
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        //If we're in any state but poweredOn we can't do much in here. Certain states such as poweredOff are a good oportunity to display a notification to the end user that they need to turn Bluetooth on for their device or at least to let them know there is some sort of error.
        if (central.state != .poweredOn){
            return
        }
        else{
            //Otherwise we can call scanForPeripherals which will find any Bluetooth device in range that meets our specifications. You can see commented out here an array of CBUUID objects which would be the first parameter in that method call. Now because this is just a test I have commented that out and instead used nil. This will force the return of any BT device in range of ours. See the documentation for more including the options available for this method.
            //let serviceUUID:[CBUUID] = [CBUUID(string:self.TRANSFER_SERVICE_UUID)]
            centralManager!.scanForPeripherals(withServices: nil, options: nil)
        }
    }
    
    //--4 This method will return for each device found by scanForPeripherals
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("Discovered a peripheral")
        //The identifier can be a useful way to idenfify the object we have found
        print(peripheral.identifier)
        //The name parameter here is optional in a BT peripheral's advertisment packet and as such might be empty so simply printing an unwrapped one can often lead to a crash. This and the identifier are good data to display to your user if they have the option of selecting between multiple devices.
        //print(peripheral.name!)
        //The RSSI is a signal strength value that tells us how strong a connection we have to this device. These values are written as negative numbers approaching 0. The closer to 0, the stronger the signal is.
        print(RSSI)
        
        //If we haven't connected to a peripheral yet let's go ahead and do so.
        if (discoveredPeripheral != peripheral){
            
            //Stop scanning first. We do this for two reasons, one we don't want to keep scanning while we're trying to connect as it's taxing on the system to do both. The other is how much battery the scanning process can utilize if just left on. I have left this commented out for now so you can see all the local devices print out to the console.
            //centralManager?.stopScan()
            
            //Connect lets us attempts to connect our app to a peripheral device. Again, I have left this commented out so that you can see the print statements. Go ahead and test making actual connections.
            //centralManager?.connect(discoveredPeripheral!, options: nil)
        }
    }
    
    //If we fail to connect, simply cancel the attempt. This is a good place to let your user know there was an issue. Also you can start scanning again here which will take us back to didDiscover.
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print(error!.localizedDescription)
        
        centralManager?.cancelPeripheralConnection(discoveredPeripheral!)
    }
    
    //--5 This method will fire if the connection was successful.
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected")
        //Set our local parameter first and make sure it's delegate is set to ourselves for the next 3 methods.
        discoveredPeripheral = peripheral
        discoveredPeripheral!.delegate = self
        
        //Again, we have an array of service UUIDs so we can get the associated CBService objects from the peripheral. If you want all of the services, as we do for this demonstration, pass nil to the discoverServices method instead of this array.
        //let serviceUUID:[CBUUID] = [CBUUID(string:self.TRANSFER_SERVICE_UUID)]
        discoveredPeripheral!.discoverServices(nil)
    }
    
    //--6 this method will return any matching services from our discoverServices call. You'll note that this does not pass us back any services directly. Instead it will populate the services array of our peripheral device with any matching services from our discover call.
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        //If there is an error staying connected to the device isn't nessesary so we can break off the connection.
        if error != nil{
            centralManager?.cancelPeripheralConnection(discoveredPeripheral!)
        }
        
        //If we loop through the services in our peripheral object we can then ask for the characteristics of each service. Here in this example we let every single service return all of their characteristics. Typically this is not nessesary. Instead you can match the CBService objects UUID to any that you need the characteristics for and you can further narrow down any results by passing in an array of UUID objects for specific characteristics.
        for service:CBService in discoveredPeripheral!.services as [CBService]!{
            
            discoveredPeripheral!.discoverCharacteristics(nil, for: service)
        }
    }
    
    //--7 this method will return any matching characteristics from our discoverCharacteristics call. You'll note that this does not pass us back any characteristics directly. Instead it will populate the characteristics array of the returned service with any matching characteristics from our discover call.
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        //If there is an error staying connected to the device isn't nessesary so we can break off the connection.
        if error != nil{
            centralManager?.cancelPeripheralConnection(discoveredPeripheral!)
        }
        
        //If we loop through the characteristics in our service object we can then make any calls nessesary to interact with those characteristics. We match UUIDs using isEqual to look for specific data we wish to interact with. We can call any characteristic write, read, or subscribe requests.
        for characteristic:CBCharacteristic in service.characteristics as [CBCharacteristic]!{
            if characteristic.uuid.isEqual(CBUUID(string:self.TRANSFER_CHARACTERISTIC_UUID)){
                //Here we're subscribing to the characteristic. If the characteristic is properly set up this means that any time the value for that characteristic is updated we'll get a call back with the new value in didWriteValueFor, as below.
                peripheral.setNotifyValue(true, for: characteristic)
            }
        }
    }
    
    //--8 This method will call any time the peripheral updates a characteristic's value that this central is subscribed to.
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        //In this example, we're assuming we've been sent back a value that was a string with UTF8 encoding. What is sent back in the value parameter is simply a Data object though and can be any kind of data so it's important that you ensure you're using the correct data types.
        let stringFromData:String = String(data: characteristic.value!, encoding: .utf8)!
        //From here the characteristic data can be used in any way that the central object needs to use it. 
    }

    //An OSX app method, you can safely ignore this.
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

}

