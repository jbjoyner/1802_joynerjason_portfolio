//
//  InterfaceController.swift
//  JoynerJason_CE02 WatchKit Extension
//
//  Created by Jason Joyner on 2/16/20.
//  Copyright © 2020 Jason Joyner. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    @IBOutlet weak var tableView: WKInterfaceTable!
    
    var tableData: [DataObject] = []
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        let session = WCSession.default
        session.delegate = self
        session.activate()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        pushController(withName: "DetailsInterfaceController", context: tableData[rowIndex])
    }
    
    func updateTable(message:[String:Any]) {
        if let data = message["Data"] as? [DataObject] {
            tableData = data
            loadTableData()
        }
    }
    
    private func loadTableData() {
        tableView.setNumberOfRows(tableData.count, withRowType: "RowController")
        
        for(index, rowModel) in tableData.enumerated() {
            if let rowController = tableView.rowController(at: index) as? RowController {
                rowController.rowLabel.setText(rowModel.title)
            }
        }
    }
        
    //MARK: - Delegates
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        switch activationState {
        case .activated:
            print("Watch Activated")
            getDataFromPhone()
        case .notActivated:
            print("Watch Not Activated")
        case .inactive:
            print("Watch Inactive")
        default:
            print("Watch Not Activated")
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        updateTable(message: message)
    }
    
    func getDataFromPhone() {
        if WCSession.default.isReachable {
            WCSession.default.sendMessage(["Data":true], replyHandler: { (reply) in
                if  let didReceive = reply["Data"] as? Bool {
                    if didReceive {
                        self.updateTable(message: ["Data":"Data"])
                    }
                }
            }) { (error) in
                print("Messaging Error: \(error.localizedDescription)")
            }
        }
    }
}
