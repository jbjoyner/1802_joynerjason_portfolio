//
//  DetailsInterfaceController.swift
//  JoynerJason_CE02 WatchKit Extension
//
//  Created by Jason Joyner on 2/16/20.
//  Copyright © 2020 Jason Joyner. All rights reserved.
//

import Foundation
import WatchKit

class DetailsInterfaceController : WKInterfaceController {
    
    @IBOutlet weak var detailsLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        if let detailData = context as? String {
            detailsLabel.setText(detailData)
        }
    }
    
    override func willActivate() {
        super.willActivate()
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
}
