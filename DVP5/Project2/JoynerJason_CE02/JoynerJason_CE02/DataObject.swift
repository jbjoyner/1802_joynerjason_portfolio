//
//  DataObject.swift
//  JoynerJason_CE02
//
//  Created by Jason Joyner on 2/16/20.
//  Copyright © 2020 Jason Joyner. All rights reserved.
//

import Foundation

class DataObject: NSObject, NSCoding {
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "Title")
        coder.encode(desc, forKey: "Desc")
        coder.encode(image, forKey: "Image")
    }
    
    required convenience init?(coder: NSCoder) {
        self.init(title: "Title", desc: "Description", image: "Image")
        
        title = coder.decodeObject(forKey: "Title") as? String
        desc = coder.decodeObject(forKey: "Description") as? String
        image = coder.decodeObject(forKey: "Image") as? String
    }
    
    var title: String?
    var desc: String?
    var image: String?
    
    init(title: String, desc: String, image: String) {
        self.title = title
        self.desc = desc
        self.image = image
    }
}
