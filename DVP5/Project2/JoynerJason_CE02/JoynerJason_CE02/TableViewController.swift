//
//  TableViewController.swift
//  JoynerJason_CE02
//
//  Created by Jason Joyner on 2/16/20.
//  Copyright © 2020 Jason Joyner. All rights reserved.
//

import UIKit
import WatchConnectivity

class TableViewController: UITableViewController, WCSessionDelegate {
    
    var objects: [DataObject] = []
    
    var message:[String:Any] {
        return ["Data": objects]
    }
    
    let session = WCSession.default
    
    func interactiveMessage() {
        session.sendMessage(message, replyHandler: nil, errorHandler: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Watch Connectivity
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        loadTheArray()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return objects.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        // Configure the cell...
        cell.textLabel?.text = (objects[indexPath.row].title)
        return cell
    }
    
    func loadTheArray() {
        objects.append(DataObject(title: "iOS", desc: "Learn to Code", image: "iPhone"))
        objects.append(DataObject(title: "Android", desc: "Learn to Code", image: "iPhone"))
        objects.append(DataObject(title: "Windows", desc: "Learn to use Windows", image: "iPhone"))
        objects.append(DataObject(title: "Mac", desc: "Learn to use a Mac", image: "iPhone"))
        objects.append(DataObject(title: "Linux", desc: "Learn to use Linux", image: "iPhone"))
        objects.append(DataObject(title: "Math", desc: "Learn to do Math", image: "iPhone"))
        objects.append(DataObject(title: "English", desc: "Learn English", image: "iPhone"))
        objects.append(DataObject(title: "History", desc: "Learn about History", image: "iPhone"))
        objects.append(DataObject(title: "Biology", desc: "Learn Biology", image: "iPhone"))
        objects.append(DataObject(title: "Science", desc: "Learn Science", image: "iPhone"))
    }
    
    //MARK: - Delegates
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        switch activationState {
        case .activated:
            print("Phone Activated")
            interactiveMessage()
        case .notActivated:
            print("Phone Not Activated")
        case .inactive:
            print("Phone Inactive")
        default:
            print("Phone Not Activated")
        }
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("Session went inactive")
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print("Session deactivated")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        if let sendData = message["Data"] as? Bool {
            if sendData {
                interactiveMessage()
            }
        }
    }
}
