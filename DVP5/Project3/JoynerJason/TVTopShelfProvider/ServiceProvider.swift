//
//  ContentProvider.swift
//  TVTopShelfProvider
//
//  Created by Jason Joyner on 2/23/20.
//  Copyright © 2020 Jason Joyner. All rights reserved.
//

import Foundation
import TVServices

//--2 This TVTopShelfProvider is our extension that lets us supply data to the top shelf. To create one in a new TV project go to File->New->Target->TV Services Extension and that will create the basic shell of this class for you. You'll note that a fresh one has just the init and 2 variables, the style and the items. You should never create an instance of this class yourself, the system will do so automatically outside of your app running.
class ServiceProvider: NSObject, TVTopShelfProvider {

    override init() {
        super.init()
    }

    // MARK: - TVTopShelfProvider protocol

    //There are two styles here, the inset (which this example is using) and the sectioned (which is what you'll be doing for lab). If need be make some basic checks here to determine which type to use at a particular time.
    var topShelfStyle: TVTopShelfContentStyle {
        // Return desired Top Shelf style.
        return .inset
    }

    //This is where you actually create your TVContentItem array based on the data from your TVTopShelfData struct. Here I've created a separate var where the data is actually created as it will be cleaner should this project have multiple checks/data points that it needs to potentially return here so that this var doesn't get cluttered visually.
    var topShelfItems: [TVContentItem] {
        // Create an array of TVContentItems.
        return insetTopShelfItems
    }

    //The array for our inset top shelf.
    fileprivate var insetTopShelfItems: [TVContentItem]{
        //Grab the predefined data set we created in the extension for our data struct
        let itemToDisplay = TVTopShelfData.sampleItemsForInsetTopShelf
        
        //Create the array of TVContentItems. First up to do that we need a TVContentIdentifier. Don't forget, this needs to be unique across all past, present, and future objects for your top shelf. We're also being careful here to use guard and drop errors to let us know if anything goes wrong here.
        let contentArray: [TVContentItem] = itemToDisplay.map
            {dataItem in
                 let contentIdentifier = TVContentIdentifier(identifier: dataItem.identifier, container: nil)
                
                //Next we can create the actual content item using the identifier
                let contentItem = TVContentItem(contentIdentifier: contentIdentifier)
                
                //Once those are both done we can populate the content item using the data from our struct and then return it to the new contentArray. That will then get returned to the insetTopShelfItems and used for the top shelf.
                contentItem.title = dataItem.title
                contentItem.displayURL = dataItem.displayURL
                contentItem.imageURL = dataItem.imageUrl
                contentItem.imageShape = .extraWide
                //The topShelfItems piece isn't important here but in a sectioned top shelf you need to use it to define each section. Your main array should be 1 TVContentItem for each section and then items in that section can be added to this variable.
                //contentItem.topShelfItems
                return contentItem
        }//end map
        return contentArray
    }// end fileprivate
}
//--3 NOTE WHEN RUNNING: When you start this app up you'll just see a blank screen, this is because the app is running but we didn't do anything in the actual app, just for the top shelf. Hit cmd + shift + H to exit the app and return to the home screen. Then hit cmd + shift + R to display the simulated TV remote. After that you can use the keyboard arrow keys and enter to navigate the UI.
