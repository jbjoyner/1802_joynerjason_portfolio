//
//  TVTopShelfData.swift
//  JoynerJason_CE04
//
//  Created by Jason Joyner on 2/22/20.
//  Copyright © 2020 Jason Joyner. All rights reserved.
//

import Foundation

struct TVTopShelfData {
    
    //The groups here will be used to define and separate the types of content for our top shelf. This makes it easy for us to separate content, especially for the segmented style of top shelf.
    enum Group:String {
        case Animals
        case Skylines
        case Vehicles
        
        static let allGroups: [Group] = [.Animals, .Skylines, .Vehicles]
    }
    
    //Each item will be part of one of our two defined groups
    let group: Group
    
    //Each item will need a number to identify it within the group
    let number: Int
    
    //Each item will have a title we can use for display purposes.
    let title: String
    
    //This image name variable is constructed to exactly match the image names in the project. We need the group and number for other variables as well, hence why we are creating the image string this way instead of just providing it when we create an instance of TVTopShelfData
    var imageName: String {
        return "\(group.rawValue)\(number).jpg"
    }
    //The identifier we'll use in the top shelf system. Each item past present and future must have a unique id for the OS to use for top shelf display.
    var identifier: String {
        return "\(group.rawValue)\(number)"
    }
    
    //Create a URL path for the system to use for display purposes
    var displayURL: URL {
        var components = URLComponents()
        components.scheme = "uikitcatalog"
        components.path = "tvTopShelfItem"
        components.queryItems = [URLQueryItem(name: "identifier", value: identifier)]
        
        return components.url!
    }
    
    //Create an image URL so that the top shelf can find our images in the file system of the OS as the top shelf runs when our app is asleep
    var imageUrl: URL {
        let mainBundle = Bundle.main
        guard let imageUrl = mainBundle.url(forResource: imageName, withExtension: nil) else {
            fatalError("Error getting local image URL")
        }
        return imageUrl
    }
}

//This extension is used to construct all of our TVTopShelfData items and create some data sets for use later in either our top shelf or the app
extension TVTopShelfData {
    //All of the data items
    static var sampleItems: [TVTopShelfData] = {
        return[TVTopShelfData(group: .Animals, number: 1, title: "Animals1"),
               TVTopShelfData(group: .Animals, number: 2, title: "Animals2"),
               TVTopShelfData(group: .Animals, number: 3, title: "Animals3"),
               TVTopShelfData(group: .Animals, number: 4, title: "Animals4"),
               TVTopShelfData(group: .Animals, number: 5, title: "Animals5"),
               
               TVTopShelfData(group: .Vehicles, number: 1, title: "Vehicles1"),
               TVTopShelfData(group: .Vehicles, number: 2, title: "Vehicles2"),
               TVTopShelfData(group: .Vehicles, number: 3, title: "Vehicles3"),
               TVTopShelfData(group: .Vehicles, number: 4, title: "Vehicles4"),
               TVTopShelfData(group: .Vehicles, number: 5, title: "Vehicles5"),
               
               TVTopShelfData(group: .Skylines, number: 1, title: "Skylines1"),
               TVTopShelfData(group: .Skylines, number: 2, title: "Skylines2"),
               TVTopShelfData(group: .Skylines, number: 3, title: "Skylines3"),
               TVTopShelfData(group: .Skylines, number: 4, title: "Skylines4"),
               TVTopShelfData(group: .Skylines, number: 5, title: "Skylines5"),
               ]
    }()
    
    //Create a sample array for displaying on an inset style top shelf.
    static var sampleItemsForInsetTopShelf: [TVTopShelfData] = {
        //Filter our main data set by the Pillars group. Try changing this to the rocks group to see different results on the top shelf.
        let pillarItems = TVTopShelfData.sampleItems.filter {$0.group == .Vehicles}
        //Return an array using the prefix method. This means that however many items for the number we pass in, starting at the front of the array, are put in a new array and passed back. It's redundant in this case as we only have 3 total but if you had 100 it wouldn't make sense to put all of them in the top shelf since the user won't want to scroll through 100 items before even getting into the app.
         return Array(pillarItems.prefix(3))
    }()
}
