﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodingExercise5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variable to control our loop
            bool run = true;

            //Loop to control our game
            while (run)
            {
                //Create a variable to use as a counter to control our loop
                int counter = 1;

                //Variable to hold the user wins
                int userWins = 0;

                //Variable to hold the computer wins
                int computerWins = 0;

                //Variable to hold the number of ties
                int ties = 0;

                //Array to hold the choices
                string[] game = { "Rock", "Paper", "Scissors", "Lizard", "Spock" };

                Random random = new Random();

                //Create a loop to run our menu
                while (counter <= 10)
                {
                    //Clear the screen for easy reading
                    Console.Clear();

                    //The Rock, Paper, Scissor, Lizard, Spock game
                    Console.WriteLine("Let's play Rock, Paper, Scissor, Lizard, Spock.\n");


                    //Display the menu
                    Console.WriteLine
                        (
                        "1. Rock\n" +
                        "2. Paper\n" +
                        "3. Scissors\n" +
                        "4. Lizard\n" +
                        "5. Spock\n"
                        );

                    //Ask the user for their selection
                    string choice = Validation.IsNull("What is your choice? Rock, Paper, Scissor, Lizard or Spock: ");

                    //Switch statement to run the option chosen
                    switch (choice)
                    {
                        case "1":
                        case "rock":
                            {
                                //Set our integer to a random number
                                int index = random.Next(game.Length);

                                //Conditionals to check for the outcome
                                if (index == 0)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks rock!");

                                    //Show the result
                                    Console.WriteLine("\nTie!");

                                    //Increase the counter
                                    ties++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 1)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks paper!");

                                    //Show the result
                                    Console.WriteLine("\nComputer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 2)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks scissors!");

                                    //Show the result
                                    Console.WriteLine("\nPlayer wins!");

                                    //Increase the counter
                                    userWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 3)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks lizard!");

                                    //Show the result
                                    Console.WriteLine("\nPlayer wins!");

                                    //Increase the counter
                                    userWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 4)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks spock!");

                                    //Show the result
                                    Console.WriteLine("\nComputer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                            }
                            break;
                        case "2":
                        case "paper":
                            {
                                //Set our integer to a random number
                                int index = random.Next(game.Length);

                                //Conditionals to check for the outcome
                                if (index == 0)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks rock!");

                                    //Show the result
                                    Console.WriteLine("\nPlayer wins!");

                                    //Increase the counter
                                    userWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 1)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks paper!");

                                    //Show the result
                                    Console.WriteLine("\nTie!");

                                    //Increase the counter
                                    ties++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 2)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks scissors!");

                                    //Show the result
                                    Console.WriteLine("\nComputer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 3)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks lizard!");

                                    //Show the result
                                    Console.WriteLine("\nComputer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 4)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks spock!");

                                    //Show the result
                                    Console.WriteLine("\nPlayer wins!");

                                    //Increase the counter
                                    userWins++;

                                    //Increase the counter
                                    counter++;
                                }
                            }
                            break;
                        case "3":
                        case "scissor":
                            {
                                //Set our integer to a random number
                                int index = random.Next(game.Length);

                                //Conditionals to check for the outcome
                                if (index == 0)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks rock!");

                                    //Show the result
                                    Console.WriteLine("\nComputer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 1)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks paper!");

                                    //Show the result
                                    Console.WriteLine("\nPlayer wins!");

                                    //Increase the counter
                                    userWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 2)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks scissors!");

                                    //Show the result
                                    Console.WriteLine("\nTie!");

                                    //Increase the counter
                                    ties++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 3)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks lizard!");

                                    //Show the result
                                    Console.WriteLine("\nPlayer wins!");

                                    //Increase the counter
                                    userWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 4)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks spock!");

                                    //Show the result
                                    Console.WriteLine("\nComputer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                            }
                            break;
                        case "4":
                        case "lizard":
                            {
                                //Set our integer to a random number
                                int index = random.Next(game.Length);

                                //Conditionals to check for the outcome
                                if (index == 0)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks rock!");

                                    //Show the result
                                    Console.WriteLine("\nComputer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 1)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks paper!");

                                    //Show the result
                                    Console.WriteLine("\nPlayer wins!");

                                    //Increase the counter
                                    userWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 2)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks scissors!");

                                    //Show the result
                                    Console.WriteLine("\nConputer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 3)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks lizard!");

                                    //Show the result
                                    Console.WriteLine("\nTie!");

                                    //Increase the counter
                                    ties++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 4)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks spock!");

                                    //Show the result
                                    Console.WriteLine("\nPlayer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                            }
                            break;
                        case "5":
                        case "spock":
                            {
                                //Set our integer to a random number
                                int index = random.Next(game.Length);

                                //Conditionals to check for the outcome
                                if (index == 0)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks rock!");

                                    //Show the result
                                    Console.WriteLine("\nPlayer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 1)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks paper!");

                                    //Show the result
                                    Console.WriteLine("\nComputer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 2)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks scissors!");

                                    //Show the result
                                    Console.WriteLine("\nPlayer wins!");

                                    //Increase the counter
                                    userWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 3)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks lizard!");

                                    //Show the result
                                    Console.WriteLine("\nComputer wins!");

                                    //Increase the counter
                                    computerWins++;

                                    //Increase the counter
                                    counter++;
                                }
                                else if (index == 4)
                                {
                                    //Show the computers choice
                                    Console.WriteLine("\nComputer picks spock!");

                                    //Show the result
                                    Console.WriteLine("\nTie!");

                                    //Increase the counter
                                    ties++;

                                    //Increase the counter
                                    counter++;
                                }
                            }
                            break;
                    }

                    //Pause and wait for user input
                    Utility.PauseBeforeContinuing();
                    
                }

                //Check for number of wins
                if (userWins > computerWins)
                {
                    //Display the outcome of the game
                    Console.WriteLine($"\nPlayer had a total of {userWins} wins, Computer had a total of {computerWins} wins and a total of {ties} ties. Player wins the game!");
                }
                else
                {
                    //Display the outcome of the game
                    Console.WriteLine($"\nPlayer had a total of {userWins} wins, Computer had a total of {computerWins} wins and a total of {ties} ties. Computer wins the game!");
                }

                //Ask the user if they would like to play again
                int response = Validation.GetInt("\nWould you like to play again? 1: Yes or 2: No: ");

                //Switch statement to run the chosen response
                switch (response)
                {
                    case 1:
                        {
                            run = true;
                        }
                        break;
                    case 2:
                        {
                            run = false;
                        }
                        break;
                }

                //Pause and wait for user input
                Utility.PauseBeforeContinuing();

            }
        }
    }
}
