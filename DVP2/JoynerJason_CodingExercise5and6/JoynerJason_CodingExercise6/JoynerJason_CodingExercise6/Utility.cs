﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodingExercise6
{
    class Utility
    {
        //Creates a stopping point in the code and waits for user input before continuing
        public static void PauseBeforeContinuing()
        {
            Console.WriteLine();
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }
    }
}
