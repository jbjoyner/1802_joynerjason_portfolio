﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodingExercise6
{
    class Validation
    {
        //Converts user input to an integer
        public static int GetInt(string message = "Enter an Integer: ")
        {
            int validationInt;
            string input = null;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            } while (!Int32.TryParse(input, out validationInt));

            return validationInt;
        }

        //Converts user input to an integer when a min and max are needed
        public static int GetInt(int min, int max, string message = "Enter an integer: ")
        {
            int validationInt;
            string input = null;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            } while (!(Int32.TryParse(input, out validationInt) && (validationInt >= min && validationInt <= max)));

            return validationInt;
        }

        //Converts user input to a bool of true or false
        public static bool GetBool(string message = "Enter yes or no: ")
        {
            bool answer = false;
            string input = null;

            bool needValidResponse = true;

            while (needValidResponse)
            {
                Console.Write(message);
                input = Console.ReadLine().ToLower();

                switch (input)
                {
                    case "yes":
                    case "y":
                    case "true":
                    case "t":
                        {
                            answer = true;
                            needValidResponse = false;
                        }
                        break;
                    case "no":
                    case "n":
                    case "false":
                    case "f":
                        {
                            needValidResponse = false;
                        }
                        break;
                }
            }
            return answer;
        }

        //Converts user input to a double
        public static double GetDouble(string message = "Enter an number: ")
        {
            double validationDouble;
            string input = null;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            } while (!Double.TryParse(input, out validationDouble));

            return validationDouble;
        }

        //Converts user input to a double when a min and max are neeed
        public static double GetDouble(double min, double max, string message = "Enter an number: ")
        {
            double validationDouble;
            string input = null;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            } while (!(Double.TryParse(input, out validationDouble) && (validationDouble >= min && validationDouble <= max)));

            return validationDouble;

        }

        //Checks to make sure user input is not left blank
        public static string IsNull(string message = "Please do not leave this blank: ")
        {
            
            string input;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(input));
            return input;
        }

        //Converts user input to a float
        public static float GetFloat(string message = "Enter an Number: ")
        {
            float validationFloat;
            string input = null;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            } while (!float.TryParse(input, out validationFloat));

            return validationFloat;
        }

        //Converts user input to a decimal
        public static decimal GetDecimal(string message = "Enter an number: ")
        {
            decimal validationDecimal;
            string input = null;

            do
            {
                Console.Write(message);
                input = Console.ReadLine();
            } while (!Decimal.TryParse(input, out validationDecimal));

            return validationDecimal;
        }
    }
}
