﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodingExercise4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Call the Colors method
            Dictionary<string, Facts> facts = Colors();

            //Call the menu method
            Menu(facts);
        }
        static void Menu(Dictionary<string, Facts> facts)
        {
            //Variable to control our menu
            bool run = true;

            //While loop to control our ment
            while (run)
            {
                //Clear the screen for easy reading
                Console.Clear();

                //Display the menu choices
                Console.Write
                    (
                    "What is your favorite color?\n\n" +
                    "1. Red\n" +
                    "2. Orange\n" +
                    "3. Yellow\n" +
                    "4. Green\n" +
                    "5. Blue\n" +
                    "6. Indigo\n" +
                    "7. Violet\n" +
                    "8. Exit\n\n" +
                    "Please make your selection from the choices: "
                    );

                //Capture the user input
                string input = Console.ReadLine().ToLower();

                //Switch statements to perform the user actions chosen
                switch (input)
                {
                    case "1":
                    case "red":
                        {
                            Red(facts);
                        }
                        break;
                    case "2":
                    case "orange":
                        {
                            Orange(facts);
                        }
                        break;
                    case "3":
                    case "yellow":
                        {
                            Yellow(facts);
                        }
                        break;
                    case "4":
                    case "green":
                        {
                            Green(facts);
                        }
                        break;
                    case "5":
                    case "blue":
                        {
                            Blue(facts);
                        }
                        break;
                    case "6":
                    case "indigo":
                        {
                            Indigo(facts);
                        }
                        break;
                    case "7":
                    case "violet":
                        {
                            Violet(facts);
                        }
                        break;
                    case "8":
                    case "exit":
                        {
                            run = false;
                        }
                        break;
                }
                //Pause the program and wait for user response
                //Utility.PauseBeforeContinuing();
            }
        }
        static Dictionary<string, Facts> Colors()
        {
            //Create the dictionary to hold our colors and the facts about the color
            Dictionary<string, Facts> facts = new Dictionary<string, Facts>();

            //Red Facts
            string fact1 = "Apples are red at their peak of freshness.";
            string fact2 = "Fire trucks are often red.";
            string fact3 = "Red often represents heat as in \"Red Hot\".";

            Facts redFacts = new Facts(fact1, fact2, fact3);

            facts.Add("Red", redFacts);

            //Orange Facts
            fact1 = "Orange is often used to describe heat as well \"The ambers in the fire are glowing orange\".";
            fact2 = "Orange can be achieved by mixing red and yellow together.";
            fact3 = "There is no other word that rhymes with orange.";

            Facts orangeFacts = new Facts(fact1, fact2, fact3);

            facts.Add("Orange", orangeFacts);

            //Yellow Facts
            fact1 = "Bananas are yellow.";
            fact2 = "Minions are yellow.";
            fact3 = "Yellow is used to describe the color of the sun.";

            Facts yellowFacts = new Facts(fact1, fact2, fact3);

            facts.Add("Yellow", yellowFacts);

            //Green Facts
            fact1 = "Green is the color of money.";
            fact2 = "Green is used to describe envy.";
            fact3 = "Green is the color for St. Patrick's Day.";

            Facts greenFacts = new Facts(fact1, fact2, fact3);

            facts.Add("Green", greenFacts);

            //Blue Facts
            fact1 = "Blue is color the sky.";
            fact2 = "Blue is used to describe cold.";
            fact3 = "Police lights are blue.";

            Facts blueFacts = new Facts(fact1, fact2, fact3);

            facts.Add("Blue", blueFacts);

            //Indigo Facts
            fact1 = "Indigo is traditionaly known as one of the colors in a rainbow.";
            fact2 = "Egyptians dyed mummy cases with indigo.";
            fact3 = "Indigo is a deep blue with a hint of red.";

            Facts indigoFacts = new Facts(fact1, fact2, fact3);

            facts.Add("Indigo", indigoFacts);

            //Violet Facts
            fact1 = "The color violet represents relaxation.";
            fact2 = "Violet stimulates creativity.";
            fact3 = "Violet stimulates imagination.";

            Facts violetFacts = new Facts(fact1, fact2, fact3);

            facts.Add("Violet", violetFacts);

            return facts;
        }
        static void Red(Dictionary<string, Facts> facts)
        {
            //Clear the screen for easy reading
            Console.Clear();

            //Display the menu
            Console.WriteLine($"{ facts["Red"].Fact1}\n\n{ facts["Red"].Fact2}\n\n{ facts["Red"].Fact3}\n");

            //Pause the program and wait for user response
            Utility.PauseBeforeContinuing();
        }
        static void Orange(Dictionary<string, Facts> facts)
        {
            //Clear the screen for easy reading
            Console.Clear();

            //Display the menu
            Console.WriteLine($"{ facts["Orange"].Fact1}\n\n{ facts["Orange"].Fact2}\n\n{ facts["Orange"].Fact3}\n");

            //Pause the program and wait for user response
            Utility.PauseBeforeContinuing();
        }
        static void Yellow(Dictionary<string, Facts> facts)
        {
            //Clear the screen for easy reading
            Console.Clear();

            //Display the menu
            Console.WriteLine($"{ facts["Yellow"].Fact1}\n\n{ facts["Yellow"].Fact2}\n\n{ facts["Yellow"].Fact3}\n");

            //Pause the program and wait for user response
            Utility.PauseBeforeContinuing();
        }
        static void Green(Dictionary<string, Facts> facts)
        {
            //Clear the screen for easy reading
            Console.Clear();

            //Display the menu
            Console.WriteLine($"{ facts["Green"].Fact1}\n\n{ facts["Green"].Fact2}\n\n{ facts["Green"].Fact3}\n");

            //Pause the program and wait for user response
            Utility.PauseBeforeContinuing();
        }
        static void Blue(Dictionary<string, Facts> facts)
        {
            //Clear the screen for easy reading
            Console.Clear();

            //Display the menu
            Console.WriteLine($"{ facts["Blue"].Fact1}\n\n{ facts["Blue"].Fact2}\n\n{ facts["Blue"].Fact3}\n");

            //Pause the program and wait for user response
            Utility.PauseBeforeContinuing();
        }
        static void Indigo(Dictionary<string, Facts> facts)
        {
            //Clear the screen for easy reading
            Console.Clear();

            //Display the menu
            Console.WriteLine($"{ facts["Indigo"].Fact1}\n\n{ facts["Indigo"].Fact2}\n\n{ facts["Indigo"].Fact3}\n");

            //Pause the program and wait for user response
            Utility.PauseBeforeContinuing();
        }
        static void Violet(Dictionary<string, Facts> facts)
        {
            //Clear the screen for easy reading
            Console.Clear();

            //Display the menu
            Console.WriteLine($"{ facts["Violet"].Fact1}\n\n{ facts["Violet"].Fact2}\n\n{ facts["Violet"].Fact3}\n");

            //Pause the program and wait for user response
            Utility.PauseBeforeContinuing();
        }
    }
}
