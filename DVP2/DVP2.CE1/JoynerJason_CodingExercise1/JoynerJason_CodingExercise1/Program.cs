﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodingExercise1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Jason Joyner
             DVP2
             Coding Exercise 1
             05/28/2018
             */

            Random random = new Random();

            //List with all our coffee choices
            List<string> list = new List<string>();
            list.Add("Foldgers");
            list.Add("Maxwell House");
            list.Add("Starbucks");
            list.Add("Dunkin Donuts");
            list.Add("Tim Hortons");
            list.Add("Green Mountain Coffee");
            list.Add("Seattles Best Coffee");
            list.Add("Caribou Coffee");
            list.Add("Community Coffee");
            list.Add("Eight O'Clock Coffee");
            list.Add("Hills Bros. Coffee");
            list.Add("Tully's Coffee");
            list.Add("Top Shelf Coffee");
            list.Add("Second Cup");
            list.Add("Revelator Coffee");
            list.Add("Red Diamond");
            list.Add("New England Coffee");
            list.Add("Koa Coffee Plantation");
            list.Add("Gloria Jean's Coffee's");
            list.Add("Dutch Bros. Coffee "); 
            
            //Variable to control our loop for the menu
            bool run = true;

            //Loop to run our menu
            while (run)
            {
                //Clear the console for easy reading
                Console.Clear();

                //Display the title
                Console.WriteLine("Menu\n");

                //Print out of list
                Console.Write
                    ("1. Print the list in A-Z order\n" +
                    "2. Print the list in Z-A order\n" +
                    "3. Randomly delete one item from the list\n" +
                    "4. Exit\n\n" +
                    "Please chose an option from the menu: ");

                //Capture the user input
                String input = Console.ReadLine().ToLower();

                //Switch statements to run the option the user chooses
                switch (input)
                {
                    case "1":
                    case "print the list in a-z order":
                        {
                            AZ(list);
                        }
                        break;
                    case "2":
                    case "print the list in z-a order":
                        {
                            ZA(list);
                        }
                        break;
                    case "3":
                    case "randomly delete one item from the list":
                        {
                            Random(list, random);
                        }
                        break;
                    case "4":
                    case "exit":
                        {
                            run = false;
                        }
                        break;
                }
                //Creates a break and waits for user input before continuing
                Utility.PauseBeforeContinuing();
            }
        }
        public static void AZ(List<string> list)
        {
            //Clear the screen for easy reading
            Console.Clear();

            //Sort the list alphabetically
            list.Sort();

            //Creates a counter to number ourt items
            int x = 1;

            //Loop to print out all the items in the list
            foreach (var item in list)
            {
                Console.WriteLine($"{x}.{ item }");
                x++;
            }
        }
        public static void ZA(List<string> list)
        {
            //Clear the screen for easy reading
            Console.Clear();

            //Sort the list alphabetically
            list.Sort();

            //Reverse the list
            list.Reverse();

            //Creates a counter to show the numbers of our itmes
            int x = 1;

            //Prints out the items in our list
            foreach (var item in list)
            {
                Console.WriteLine($"{x}. { item }");
                x++;
            }
        }
        public static void Random(List<string> list, Random random)
        {
            //Clear the screen for easy reading
            Console.Clear();

            //Inserts a blank line
            Console.WriteLine();

            //Loop to choose a random number from the list to remove
            while (list.Count > 0)
            {
                //Set our integer to a random number
                int index = random.Next(list.Count);

                //Creates a counter to list the number of the item
                int x = 1;
            
                //Prints out the items in the list
                foreach (var item in list)
                {
                    Console.WriteLine($"{x}. {item}");
                    x++;
                }

                //Inserts a blank line
                Console.WriteLine();

                //Let's the user know which item is going to be removed
                Console.WriteLine($"Removing {list[index]} from list");

                //Inserts a blank line
                Console.WriteLine();

                //Remove the item from the list
                list.RemoveAt(index);
            }

            //Let the user know that no more items are left
            Console.WriteLine("No more items in the list");
        }
    }
}
