﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodingExercise3
{
    class Classes
    {
        private string firstCourse;
        private string secondCourse;
        private string thirdCourse;
        private string forthCourse;
        private string fifthCourse;

        public string FirstCourse
        {
            get { return firstCourse; }
        } 
        public string SecondCourse
        {
            get { return secondCourse; }
        }
        public string ThirdCourse
        {
            get { return thirdCourse; }
        }
        public string ForthCourse
        {
            get { return forthCourse; }
        }
        public string FifthCourse
        {
            get { return fifthCourse; }
        }

        public Classes()
        {

        }
        public Classes(string _firstCourse, string _secondCourse, string _thirdCourse, string _forthCourse, string _fifthCourse)
        {
            firstCourse = _firstCourse;
            secondCourse = _secondCourse;
            thirdCourse = _thirdCourse;
            forthCourse = _forthCourse;
            fifthCourse = _fifthCourse;
        }
    }
}
