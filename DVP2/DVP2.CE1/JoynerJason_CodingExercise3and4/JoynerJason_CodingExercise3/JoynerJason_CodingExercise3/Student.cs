﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodingExercise3
{
    class Student
    {
        private string firstName;
        private double firstGrade;
        private double secondGrade;
        private double thirdGrade;
        private double forthGrade;
        private double fifthGrade;

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        
        public double FirstGrade
        {
            get { return firstGrade; }
            set { firstGrade = value; }
        }
        public double SecondGrade
        {
            get { return secondGrade; }
            set { secondGrade = value; }
        }
        public double ThirdGrade
        {
            get { return thirdGrade; }
            set { thirdGrade = value; }
        }
        public double ForthGrade
        {
            get { return forthGrade; }
            set { forthGrade = value; }
        }
        public double FifthGrade
        {
            get { return fifthGrade; }
            set { fifthGrade = value; }
        }
        public Student(string _firstName, double _firstGrade, double _secongGrade, double _thirdGrade, double _forthGrade, double _fifthGrade)
        {
            firstName = _firstName;
            firstGrade = _firstGrade;
            secondGrade = _secongGrade;
            thirdGrade = _thirdGrade;
            forthGrade = _forthGrade;
            fifthGrade = _fifthGrade;
        }
    }
}
