﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodingExercise4
{
    class Facts
    {
        private string fact1;
        private string fact2;
        private string fact3;

        public string Fact1
        {
            get { return fact1; }
        }
        public string Fact2
        {
            get { return fact2; }
        }
        public string Fact3
        {
            get { return fact3; }
        }

        public Facts(string _fact1, string _fact2, string _fact3)
        {
            fact1 = _fact1;
            fact2 = _fact2;
            fact3 = _fact3;
        }
    }
}
