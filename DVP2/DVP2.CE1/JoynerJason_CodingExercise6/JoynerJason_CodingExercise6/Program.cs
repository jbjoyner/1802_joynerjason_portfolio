﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodingExercise6
{
    class Program
    {
        static void Main(string[] args)
        {
            //List of all of the available card
            List<string> playingCards = new List<string>();

            //Send the list to the playing cards method to fill the list
            PlayingCards(playingCards);
            
            //Create a list for each of the 4 players to hold their cards
            List<string> player1Cards = new List<string>();
            List<string> player2Cards = new List<string>();
            List<string> player3Cards = new List<string>();
            List<string> player4Cards = new List<string>();

            //Assign variables to hold the players points
            int player1Points = 0;
            int player2Points = 0;
            int player3Points = 0;
            int player4Points = 0;

            //List to hold the player points
            List<int> playerPoints = new List<int>();

            //Greet the user
            Console.WriteLine("Let's play cards!");

            //Ask the user for the name of each of the 4 players
            string player1 = Validation.IsNull("\nPlease enter the name of player 1: ");
            string player2 = Validation.IsNull("\nPlease enter the name of player 2: ");
            string player3 = Validation.IsNull("\nPlease enter the name of player 3: ");
            string player4 = Validation.IsNull("\nPlease enter the name of player 4: ");

            //Create the random number generator
            Random random = new Random();

            //Let the user know that we are dealing the cards
            Console.WriteLine("\nNow the cards are being dealt to all 4 players");

            //Creates a pause and waits for user input
            Console.WriteLine("\nPress Enter to get the results");
            Console.ReadKey();

            //Pass out the cards to the 4 players
            for (int i = 0; i <= 12; i++)
            {
                //Set our integer to a random number
                int index = random.Next(playingCards.Count);

                //Assign the card to the first player
                player1Cards.Add(playingCards[index]);

                //Remove the card from the list to avoid duplication
                playingCards.RemoveAt(index);

                //Set our integer to a random number
                index = random.Next(playingCards.Count);

                //Assign the card to the first player
                player2Cards.Add(playingCards[index]);

                //Remove the card from the list to avoid duplication
                playingCards.RemoveAt(index);

                //Set our integer to a random number
                index = random.Next(playingCards.Count);

                //Assign the card to the first player
                player3Cards.Add(playingCards[index]);

                //Remove the card from the list to avoid duplication
                playingCards.RemoveAt(index);

                //Set our integer to a random number
                index = random.Next(playingCards.Count);

                //Assign the card to the first player
                player4Cards.Add(playingCards[index]);

                //Remove the card from the list to avoid duplication
                playingCards.RemoveAt(index);
            }

            //Send player 1's hand to the method to calculte the points and add them to the list
            player1Points = Player1CardsPoints(player1Cards, player1Points);
            playerPoints.Add(player1Points);

            //Send player 2's hand to the method to calculte the points
            player2Points = Player2CardsPoints(player2Cards, player2Points);
            playerPoints.Add(player2Points);

            //Send player 3's hand to the method to calculte the points
            player3Points = Player3CardsPoints(player3Cards, player3Points);
            playerPoints.Add(player3Points);

            //Send player 4's hand to the method to calculte the points
            player4Points = Player4CardsPoints(player4Cards, player4Points);
            playerPoints.Add(player4Points);

            //Sort the list in order
            playerPoints.Sort();

            //Reverse the list so it is in highest to lowest order
            playerPoints.Reverse();

            //Clear the screen for easy reading
            Console.Clear();

            //Find out who is the winner
            if (playerPoints[0] == player1Points)
            {
                Console.WriteLine($"Place 1st \n{player1} \nCards: {player1Cards[0]}, {player1Cards[1]}, {player1Cards[2]}, {player1Cards[3]}, {player1Cards[4]}, {player1Cards[5]}, {player1Cards[6]}, {player1Cards[7]}, {player1Cards[8]}, {player1Cards[9]}, {player1Cards[10]}, {player1Cards[11]}, {player1Cards[12]} \nTotal Value: {playerPoints[0]}");
            }
            else if (playerPoints[0] == player2Points)
            {
                Console.WriteLine($"\nPlace 1st \n{player2} \nCards: {player2Cards[0]}, {player2Cards[1]}, {player2Cards[2]}, {player2Cards[3]}, {player2Cards[4]}, {player2Cards[5]}, {player2Cards[6]}, {player2Cards[7]}, {player2Cards[8]}, {player2Cards[9]}, {player2Cards[10]}, {player2Cards[11]}, {player2Cards[12]} \nTotal Value: {playerPoints[0]}");
            }
            else if (playerPoints[0] == player3Points)
            {
                Console.WriteLine($"\nPlace 1st \n{player3} \nCards: {player3Cards[0]}, {player3Cards[1]}, {player3Cards[2]}, {player3Cards[3]}, {player3Cards[4]}, {player3Cards[5]}, {player3Cards[6]}, {player3Cards[7]}, {player3Cards[8]}, {player3Cards[9]}, {player3Cards[10]}, {player3Cards[11]}, {player3Cards[12]} \nTotal Value: {playerPoints[0]}");
            }
            else if (playerPoints[0] == player4Points)
            {
                Console.WriteLine($"\nPlace 1st \n{player4} \nCards: {player4Cards[0]}, {player4Cards[1]}, {player4Cards[2]}, {player4Cards[3]}, {player4Cards[4]}, {player4Cards[5]}, {player4Cards[6]}, {player4Cards[7]}, {player4Cards[8]}, {player4Cards[9]}, {player4Cards[10]}, {player4Cards[11]}, {player4Cards[12]} \nTotal Value: {playerPoints[0]}");
            }
            if (playerPoints[1] == player1Points)
            {
                Console.WriteLine($"\nPlace 2nd \n{player1} \nCards: {player1Cards[0]}, {player1Cards[1]}, {player1Cards[2]}, {player1Cards[3]}, {player1Cards[4]}, {player1Cards[5]}, {player1Cards[6]}, {player1Cards[7]}, {player1Cards[8]}, {player1Cards[9]}, {player1Cards[10]}, {player1Cards[11]}, {player1Cards[12]} \nTotal Value: {playerPoints[1]}");
            }
            else if (playerPoints[1] == player2Points)
            {
                Console.WriteLine($"\nPlace 2nd \n{player2} \nCards: {player2Cards[0]}, {player2Cards[1]}, {player2Cards[2]}, {player2Cards[3]}, {player2Cards[4]}, {player2Cards[5]}, {player2Cards[6]}, {player2Cards[7]}, {player2Cards[8]}, {player2Cards[9]}, {player2Cards[10]}, {player2Cards[11]}, {player2Cards[12]} \nTotal Value: {playerPoints[1]}");
            }
            else if (playerPoints[1] == player3Points)
            {
                Console.WriteLine($"\nPlace 2nd \n{player3} \nCards: {player3Cards[0]}, {player3Cards[1]}, {player3Cards[2]}, {player3Cards[3]}, {player3Cards[4]}, {player3Cards[5]}, {player3Cards[6]}, {player3Cards[7]}, {player3Cards[8]}, {player3Cards[9]}, {player3Cards[10]}, {player3Cards[11]}, {player3Cards[12]} \nTotal Value: {playerPoints[1]}");
            }
            else if (playerPoints[1] == player4Points)
            {
                Console.WriteLine($"\nPlace 2nd \n{player4} \nCards: {player4Cards[0]}, {player4Cards[1]}, {player4Cards[2]}, {player4Cards[3]}, {player4Cards[4]}, {player4Cards[5]}, {player4Cards[6]}, {player4Cards[7]}, {player4Cards[8]}, {player4Cards[9]}, {player4Cards[10]}, {player4Cards[11]}, {player4Cards[12]} \nTotal Value: {playerPoints[1]}");
            }
            if (playerPoints[2] == player1Points)
            {
                Console.WriteLine($"\nPlace 3rd \n{player1} \nCards: {player1Cards[0]}, {player1Cards[1]}, {player1Cards[2]}, {player1Cards[3]}, {player1Cards[4]}, {player1Cards[5]}, {player1Cards[6]}, {player1Cards[7]}, {player1Cards[8]}, {player1Cards[9]}, {player1Cards[10]}, {player1Cards[11]}, {player1Cards[12]} \nTotal Value: {playerPoints[2]}");
            }
            else if (playerPoints[2] == player2Points)
            {
                Console.WriteLine($"\nPlace 3rd \n{player2} \nCards: {player2Cards[0]}, {player2Cards[1]}, {player2Cards[2]}, {player2Cards[3]}, {player2Cards[4]}, {player2Cards[5]}, {player2Cards[6]}, {player2Cards[7]}, {player2Cards[8]}, {player2Cards[9]}, {player2Cards[10]}, {player2Cards[11]}, {player2Cards[12]} \nTotal Value: {playerPoints[2]}");
            }
            else if (playerPoints[2] == player3Points)
            {
                Console.WriteLine($"\nPlace 3rd \n{player3} \nCards: {player3Cards[0]}, {player3Cards[1]}, {player3Cards[2]}, {player3Cards[3]}, {player3Cards[4]}, {player3Cards[5]}, {player3Cards[6]}, {player3Cards[7]}, {player3Cards[8]}, {player3Cards[9]}, {player3Cards[10]}, {player3Cards[11]}, {player3Cards[12]} \nTotal Value: {playerPoints[2]}");
            }
            else if (playerPoints[2] == player4Points)
            {
                Console.WriteLine($"\nPlace 3rd \n{player4} \nCards: {player4Cards[0]}, {player4Cards[1]}, {player4Cards[2]}, {player4Cards[3]}, {player4Cards[4]}, {player4Cards[5]}, {player4Cards[6]}, {player4Cards[7]}, {player4Cards[8]}, {player4Cards[9]}, {player4Cards[10]}, {player4Cards[11]}, {player4Cards[12]} \nTotal Value: {playerPoints[2]}");
            }
            if (playerPoints[3] == player1Points)
            {
                Console.WriteLine($"\nPlace 4th \n{player1} \nCards: {player1Cards[0]}, {player1Cards[1]}, {player1Cards[2]}, {player1Cards[3]}, {player1Cards[4]}, {player1Cards[5]}, {player1Cards[6]}, {player1Cards[7]}, {player1Cards[8]}, {player1Cards[9]}, {player1Cards[10]}, {player1Cards[11]}, {player1Cards[12]} \nTotal Value: {playerPoints[3]}");
            }
            else if (playerPoints[3] == player2Points)
            {
                Console.WriteLine($"\nPlace 4th \n{player2} \nCards: {player2Cards[0]}, {player2Cards[1]}, {player2Cards[2]}, {player2Cards[3]}, {player2Cards[4]}, {player2Cards[5]}, {player2Cards[6]}, {player2Cards[7]}, {player2Cards[8]}, {player2Cards[9]}, {player2Cards[10]}, {player2Cards[11]}, {player2Cards[12]} \nTotal Value: {playerPoints[3]}");
            }
            else if (playerPoints[3] == player3Points)
            {
                Console.WriteLine($"\nPlace 4th \n{player3} \nCards: {player3Cards[0]}, {player3Cards[1]}, {player3Cards[2]}, {player3Cards[3]}, {player3Cards[4]}, {player3Cards[5]}, {player3Cards[6]}, {player3Cards[7]}, {player3Cards[8]}, {player3Cards[9]}, {player3Cards[10]}, {player3Cards[11]}, {player3Cards[12]} \nTotal Value: {playerPoints[3]}");
            }
            else if (playerPoints[3] == player4Points)
            {
                Console.WriteLine($"\nPlace 4th \n{player4} \nCards: {player4Cards[0]}, {player4Cards[1]}, {player4Cards[2]}, {player4Cards[3]}, {player4Cards[4]}, {player4Cards[5]}, {player4Cards[6]}, {player4Cards[7]}, {player4Cards[8]}, {player4Cards[9]}, {player4Cards[10]}, {player4Cards[11]}, {player4Cards[12]} \nTotal Value: {playerPoints[3]}");
            }

            //Add up all the points
            int total = player1Points + player2Points + player3Points + player4Points;

            //Print out the total
            Console.WriteLine($"\nTotal Deck Score: {total}");


            //Creates a pause and waits for user input
            Utility.PauseBeforeContinuing();
        }
        public static List<string> PlayingCards(List<string> playingCards)
        {
            //Fill the list with the cards for hearts
            playingCards.Add("2 of Hearts");
            playingCards.Add("3 of Hearts");
            playingCards.Add("4 of Hearts");
            playingCards.Add("5 of Hearts");
            playingCards.Add("6 of Hearts");
            playingCards.Add("7 of Hearts");
            playingCards.Add("8 of Hearts");
            playingCards.Add("9 of Hearts");
            playingCards.Add("10 of Hearts");
            playingCards.Add("J of Hearts");
            playingCards.Add("Q of Hearts");
            playingCards.Add("K of Hearts");
            playingCards.Add("A of Hearts");

            //Fill the list with the cards for diamonds
            playingCards.Add("2 of Diamonds");
            playingCards.Add("3 of Diamonds");
            playingCards.Add("4 of Diamonds");
            playingCards.Add("5 of Diamonds");
            playingCards.Add("6 of Diamonds");
            playingCards.Add("7 of Diamonds");
            playingCards.Add("8 of Diamonds");
            playingCards.Add("9 of Diamonds");
            playingCards.Add("10 of Diamonds");
            playingCards.Add("J of Diamonds");
            playingCards.Add("Q of Diamonds");
            playingCards.Add("K of Diamonds");
            playingCards.Add("A of Diamonds");

            //Fill the list with the cards for spades
            playingCards.Add("2 of Spades");
            playingCards.Add("3 of Spades");
            playingCards.Add("4 of Spades");
            playingCards.Add("5 of Spades");
            playingCards.Add("6 of Spades");
            playingCards.Add("7 of Spades");
            playingCards.Add("8 of Spades");
            playingCards.Add("9 of Spades");
            playingCards.Add("10 of Spades");
            playingCards.Add("J of Spades");
            playingCards.Add("Q of Spades");
            playingCards.Add("K of Spades");
            playingCards.Add("A of Spades");

            //Fill the list with the cards for clubs
            playingCards.Add("2 of Clubs");
            playingCards.Add("3 of Clubs");
            playingCards.Add("4 of Clubs");
            playingCards.Add("5 of Clubs");
            playingCards.Add("6 of Clubs");
            playingCards.Add("7 of Clubs");
            playingCards.Add("8 of Clubs");
            playingCards.Add("9 of Clubs");
            playingCards.Add("10 of Clubs");
            playingCards.Add("J of Clubs");
            playingCards.Add("Q of Clubs");
            playingCards.Add("K of Clubs");
            playingCards.Add("A of Clubs");

            //Return the list back to main
            return playingCards;
        }
        public static int Player1CardsPoints(List<string> player1Cards, int player1Points)
        {
            foreach (string item in player1Cards)
            {
                if (item == "2 of Hearts" || item == "2 of Diamonds" || item == "2 of Spades" || item == "2 of Clubs")
                {
                    player1Points += 2;
                }
                else if (item == "3 of Hearts" || item == "3 of Diamonds" || item == "3 of Spades" || item == "3 of Clubs")
                {
                    player1Points += 3;
                }
                else if (item == "4 of Hearts" || item == "4 of Diamonds" || item == "4 of Spades" || item == "4 of Clubs")
                {
                    player1Points += 4;
                }
                else if (item == "5 of Hearts" || item == "5 of Diamonds" || item == "5 of Spades" || item == "5 of Clubs")
                {
                    player1Points += 5;
                }
                else if (item == "6 of Hearts" || item == "6 of Diamonds" || item == "6 of Spades" || item == "6 of Clubs")
                {
                    player1Points += 6;
                }
                else if (item == "7 of Hearts" || item == "7 of Diamonds" || item == "7 of Spades" || item == "7 of Clubs")
                {
                    player1Points += 7;
                }
                else if (item == "8 of Hearts" || item == "8 of Diamonds" || item == "8 of Spades" || item == "8 of Clubs")
                {
                    player1Points += 8;
                }
                else if (item == "9 of Hearts" || item == "9 of Diamonds" || item == "9 of Spades" || item == "9 of Clubs")
                {
                    player1Points += 9;
                }
                else if (item == "10 of Hearts" || item == "10 of Diamonds" || item == "10 of Spades" || item == "10 of Clubs")
                {
                    player1Points += 10;
                }
                else if (item == "J of Hearts" || item == "J of Diamonds" || item == "J of Spades" || item == "J of Clubs")
                {
                    player1Points += 12;
                }
                else if (item == "Q of Hearts" || item == "Q of Diamonds" || item == "Q of Spades" || item == "Q of Clubs")
                {
                    player1Points += 12;
                }
                else if (item == "K of Hearts" || item == "K of Diamonds" || item == "K of Spades" || item == "K of Clubs")
                {
                    player1Points += 12;
                }
                else if (item == "A of Hearts" || item == "A of Diamonds" || item == "A of Spades" || item == "A of Clubs")
                {
                    player1Points += 15;
                }
            }
            return player1Points;
        }
        public static int Player2CardsPoints(List<string> player2Cards, int player2Points)
        {
            foreach (string item in player2Cards)
            {
                if (item == "2 of Hearts" || item == "2 of Diamonds" || item == "2 of Spades" || item == "2 of Clubs")
                {
                    player2Points += 2;
                }
                else if (item == "3 of Hearts" || item == "3 of Diamonds" || item == "3 of Spades" || item == "3 of Clubs")
                {
                    player2Points += 3;
                }
                else if (item == "4 of Hearts" || item == "4 of Diamonds" || item == "4 of Spades" || item == "4 of Clubs")
                {
                    player2Points += 4;
                }
                else if (item == "5 of Hearts" || item == "5 of Diamonds" || item == "5 of Spades" || item == "5 of Clubs")
                {
                    player2Points += 5;
                }
                else if (item == "6 of Hearts" || item == "6 of Diamonds" || item == "6 of Spades" || item == "6 of Clubs")
                {
                    player2Points += 6;
                }
                else if (item == "7 of Hearts" || item == "7 of Diamonds" || item == "7 of Spades" || item == "7 of Clubs")
                {
                    player2Points += 7;
                }
                else if (item == "8 of Hearts" || item == "8 of Diamonds" || item == "8 of Spades" || item == "8 of Clubs")
                {
                    player2Points += 8;
                }
                else if (item == "9 of Hearts" || item == "9 of Diamonds" || item == "9 of Spades" || item == "9 of Clubs")
                {
                    player2Points += 9;
                }
                else if (item == "10 of Hearts" || item == "10 of Diamonds" || item == "10 of Spades" || item == "10 of Clubs")
                {
                    player2Points += 10;
                }
                else if (item == "J of Hearts" || item == "J of Diamonds" || item == "J of Spades" || item == "J of Clubs")
                {
                    player2Points += 12;
                }
                else if (item == "Q of Hearts" || item == "Q of Diamonds" || item == "Q of Spades" || item == "Q of Clubs")
                {
                    player2Points += 12;
                }
                else if (item == "K of Hearts" || item == "K of Diamonds" || item == "K of Spades" || item == "K of Clubs")
                {
                    player2Points += 12;
                }
                else if (item == "A of Hearts" || item == "A of Diamonds" || item == "A of Spades" || item == "A of Clubs")
                {
                    player2Points += 15;
                }
            }
            return player2Points;
        }
        public static int Player3CardsPoints(List<string> player3Cards, int player3Points)
        {
            foreach (string item in player3Cards)
            {
                if (item == "2 of Hearts" || item == "2 of Diamonds" || item == "2 of Spades" || item == "2 of Clubs")
                {
                    player3Points += 2;
                }
                else if (item == "3 of Hearts" || item == "3 of Diamonds" || item == "3 of Spades" || item == "3 of Clubs")
                {
                    player3Points += 3;
                }
                else if (item == "4 of Hearts" || item == "4 of Diamonds" || item == "4 of Spades" || item == "4 of Clubs")
                {
                    player3Points += 4;
                }
                else if (item == "5 of Hearts" || item == "5 of Diamonds" || item == "5 of Spades" || item == "5 of Clubs")
                {
                    player3Points += 5;
                }
                else if (item == "6 of Hearts" || item == "6 of Diamonds" || item == "6 of Spades" || item == "6 of Clubs")
                {
                    player3Points += 6;
                }
                else if (item == "7 of Hearts" || item == "7 of Diamonds" || item == "7 of Spades" || item == "7 of Clubs")
                {
                    player3Points += 7;
                }
                else if (item == "8 of Hearts" || item == "8 of Diamonds" || item == "8 of Spades" || item == "8 of Clubs")
                {
                    player3Points += 8;
                }
                else if (item == "9 of Hearts" || item == "9 of Diamonds" || item == "9 of Spades" || item == "9 of Clubs")
                {
                    player3Points += 9;
                }
                else if (item == "10 of Hearts" || item == "10 of Diamonds" || item == "10 of Spades" || item == "10 of Clubs")
                {
                    player3Points += 10;
                }
                else if (item == "J of Hearts" || item == "J of Diamonds" || item == "J of Spades" || item == "J of Clubs")
                {
                    player3Points += 12;
                }
                else if (item == "Q of Hearts" || item == "Q of Diamonds" || item == "Q of Spades" || item == "Q of Clubs")
                {
                    player3Points += 12;
                }
                else if (item == "K of Hearts" || item == "K of Diamonds" || item == "K of Spades" || item == "K of Clubs")
                {
                    player3Points += 12;
                }
                else if (item == "A of Hearts" || item == "A of Diamonds" || item == "A of Spades" || item == "A of Clubs")
                {
                    player3Points += 15;
                }
            }
            return player3Points;
        }
        public static int Player4CardsPoints(List<string> player4Cards, int player4Points)
        {
            foreach (string item in player4Cards)
            {
                if (item == "2 of Hearts" || item == "2 of Diamonds" || item == "2 of Spades" || item == "2 of Clubs")
                {
                    player4Points += 2;
                }
                else if (item == "3 of Hearts" || item == "3 of Diamonds" || item == "3 of Spades" || item == "3 of Clubs")
                {
                    player4Points += 3;
                }
                else if (item == "4 of Hearts" || item == "4 of Diamonds" || item == "4 of Spades" || item == "4 of Clubs")
                {
                    player4Points += 4;
                }
                else if (item == "5 of Hearts" || item == "5 of Diamonds" || item == "5 of Spades" || item == "5 of Clubs")
                {
                    player4Points += 5;
                }
                else if (item == "6 of Hearts" || item == "6 of Diamonds" || item == "6 of Spades" || item == "6 of Clubs")
                {
                    player4Points += 6;
                }
                else if (item == "7 of Hearts" || item == "7 of Diamonds" || item == "7 of Spades" || item == "7 of Clubs")
                {
                    player4Points += 7;
                }
                else if (item == "8 of Hearts" || item == "8 of Diamonds" || item == "8 of Spades" || item == "8 of Clubs")
                {
                    player4Points += 8;
                }
                else if (item == "9 of Hearts" || item == "9 of Diamonds" || item == "9 of Spades" || item == "9 of Clubs")
                {
                    player4Points += 9;
                }
                else if (item == "10 of Hearts" || item == "10 of Diamonds" || item == "10 of Spades" || item == "10 of Clubs")
                {
                    player4Points += 10;
                }
                else if (item == "J of Hearts" || item == "J of Diamonds" || item == "J of Spades" || item == "J of Clubs")
                {
                    player4Points += 12;
                }
                else if (item == "Q of Hearts" || item == "Q of Diamonds" || item == "Q of Spades" || item == "Q of Clubs")
                {
                    player4Points += 12;
                }
                else if (item == "K of Hearts" || item == "K of Diamonds" || item == "K of Spades" || item == "K of Clubs")
                {
                    player4Points += 12;
                }
                else if (item == "A of Hearts" || item == "A of Diamonds" || item == "A of Spades" || item == "A of Clubs")
                {
                    player4Points += 15;
                }
            }
            return player4Points;
        }
    }
}
