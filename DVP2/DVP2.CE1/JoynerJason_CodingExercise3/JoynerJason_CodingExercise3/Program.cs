﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodingExercise3
{
    class Program
    {
        static void Main(string[] args)
        {
            //List to hold the students and their grades
            List<Student> students = new List<Student>();

            //Class to hold our classes
            Classes classes = new Classes();
            
            //Call the CreateStudents method
            CreateStudents(students);

            //Call the CreateClasses method
            classes = CreateClasses(classes);

            //Call the menu method
            Menu(students, classes); 
        }
        public static void Menu(List<Student>students, Classes classes)
        {
            //Variable to control our menu
            bool run = true;

            //Loop to run our menu
            while (run)
            {
                //Clear the screen for easy reading
                Console.Clear();

                //Display the menu
                Console.Write
                    (
                    "1. Select a Student\n" +
                    "2. Change a Grade\n" +
                    "3. View Student GPA\n" +
                    "4. View Class GPA\n" +
                    "5. Exit\n\n"
                    );

                //Capture the user input from the menu choice
                string input = Validation.IsNull("Please make your selection: ").ToLower();

                //Switch statements to perform the chosen action
                switch (input)
                {
                    case "1":
                    case "select a student":
                        {
                            students = SelectStudent(students, classes);
                        }
                        break;
                    case "2":
                    case "change a grade":
                        {
                            students =students = ChangeGrade(students, classes);
                        }
                        break;
                    case "3":
                    case "view student gpa":
                        {
                            StudentGpa(students, classes);
                        }
                        break;
                    case "4":
                    case "view class gpa":
                        {
                            ClassGpa(students);
                        }
                        break;
                    case "5":
                    case "exit":
                        {
                            run = false;
                        }
                        break;
                }
                //Creates a pause and waits for a key press to continue
                Utility.PauseBeforeContinuing();
            }
        }
        public static List<Student> CreateStudents(List<Student>students)
        {
            //Fill in the variable to create our five students and their grades for the classes
            string firstName = "Mike";
            double firstGrade = 98.50;
            double secondGrade = 99.00;
            double thirdGrade = 100.00;
            double forthGrade = 97.00;
            double fifthGrade = 95.50;
            Student firstStudent = new Student(firstName, firstGrade, secondGrade, thirdGrade, forthGrade, fifthGrade);
            students.Add(firstStudent);

            firstName = "Chris";
            firstGrade = 92.25;
            secondGrade = 94.00;
            thirdGrade = 96.5;
            forthGrade = 87.00;
            fifthGrade = 91.00;
            Student secondStudent = new Student(firstName, firstGrade, secondGrade, thirdGrade, forthGrade, fifthGrade);
            students.Add(secondStudent);

            firstName = "Taylor";
            firstGrade = 100.00;
            secondGrade = 93.00;
            thirdGrade = 94.25;
            forthGrade = 96.75;
            fifthGrade = 99.00;
            Student thirdStudent = new Student(firstName, firstGrade, secondGrade, thirdGrade, forthGrade, fifthGrade);
            students.Add(thirdStudent);

            firstName = "James";
            firstGrade = 75.00;
            secondGrade = 81.00;
            thirdGrade = 83.50;
            forthGrade = 96.25;
            fifthGrade = 95.00;
            Student forthStudent = new Student(firstName, firstGrade, secondGrade, thirdGrade, forthGrade, fifthGrade);
            students.Add(forthStudent);

            firstName = "David";
            firstGrade = 100.00;
            secondGrade = 100.00;
            thirdGrade = 97.50;
            forthGrade = 96.00;
            fifthGrade = 92.00;
            Student fifthStudent = new Student(firstName, firstGrade, secondGrade, thirdGrade, forthGrade, fifthGrade);
            students.Add(fifthStudent);

            return students;
        }
        public static Classes CreateClasses(Classes classes)
        {
            //Fill in the classes with the names of the courses
            string firstCourse = "Intro to Coffee";
            string secondCourse = "History of Coffee";
            string thirdCourse = "How to grow your own Coffee";
            string forthCourse = "How to grind coffee for the best flavor";
            string fifthCourse = "Becoming a coffee expert";

            classes = new Classes(firstCourse, secondCourse, thirdCourse, forthCourse, fifthCourse);

            return classes;
        }
        public static List<Student> SelectStudent(List<Student> students, Classes classes)
        {
            //Clear the screen for easy reading
            Console.Clear();
            
            //Display a list of the students name
            int x = 1;
            foreach (Student item in students)
            {
                Console.WriteLine($"{x}. {item.FirstName}");
                x++;
            }

            //Prompt the user for their response
            int choice = Validation.GetInt("\nPlease select a student to view their grades for their classes: ");
            
            //Validate the choice is within the available choices
            while (choice > students.Count || choice <= 0)
            {
                Console.WriteLine("\nYou have made an invalid selection. Please try again.");

                choice = Validation.GetInt("\nPlease select a student to view their grades for their classes: ");
            }
            
            //Display the students name and their scores in the classes
            Console.WriteLine($"\n{students[choice - 1].FirstName} has a {students[choice - 1].FirstGrade} in \"{classes.FirstCourse}\", a { students[choice - 1].SecondGrade} in \"{classes.SecondCourse}\", a {students[choice - 1].ThirdGrade} in \"{classes.ThirdCourse}\", a {students[choice - 1].ForthGrade} in \"{classes.ForthCourse}\" and a {students[choice - 1].FifthGrade} in \"{classes.FifthCourse}\".");
            
            return students;

        }
        public static List<Student> ChangeGrade(List<Student> students, Classes classes)
        {
            //Clear the screen for easy reading
            Console.Clear();

            //Display a list of the students name
            int x = 1;
            foreach (Student item in students)
            {
                Console.WriteLine($"{x}. {item.FirstName}");
                x++;
            }

            //Prompt the user for their response
            int choice = Validation.GetInt("\nPlease select a student to change their grades for their classes: ");

            //Validate the choice is within the available choices
            while (choice > students.Count || choice <= 0)
            {
                Console.WriteLine("\nYou have made an invalid selection. Please try again.");

                choice = Validation.GetInt("\nPlease select a student to view their grades for their classes: ");
            }

            //Display the students name and their scores in the classes
            Console.WriteLine($"{students[choice - 1].FirstName}: 1. {classes.FirstCourse}: {students[choice - 1].FirstGrade} 2. {classes.SecondCourse}: {students[choice - 1].SecondGrade} 3. {classes.ThirdCourse}: {students[choice - 1].ThirdGrade} 4. {classes.ForthCourse}: {students[choice - 1].ForthGrade} 5. {classes.FifthCourse}: {students[choice - 1].FifthGrade}");

            //Prompt the user for their input
            int selection = Validation.GetInt("Please select a class to change the grade of: ");

            //Variable to hold the new grade
            double newGrade = Validation.GetDouble("What is the new grade: ");

            //Switch statements to perform the chosen action
            switch (selection)
            {
                case 1:
                    {
                        students[choice - 1].FirstGrade = newGrade;
                    }
                    break;
                case 2:
                    {
                        students[choice - 1].SecondGrade = newGrade;
                    }
                    break;
                case 3:
                    {
                        students[choice - 1].ThirdGrade = newGrade;
                    }
                    break;
                case 4:
                    {
                        students[choice - 1].ForthGrade = newGrade;
                    }
                    break;
                case 5:
                    {
                        students[choice - 1].FifthGrade = newGrade;
                    }
                    break;
            }

            return students;

        }
        public static void StudentGpa(List<Student> students, Classes classes)
        {
            //Clear the console for easy reading
            Console.Clear();
            
            //Variables for our gpa
            double gpa = 0;
            double total = 0;
            string grade = null;

            //Display a list of the students name
            int x = 1;
            foreach (Student item in students)
            {
                Console.WriteLine($"{x}. {item.FirstName}");
                x++;
            }

            //Prompt the user for their response
            int choice = Validation.GetInt("Please select a student to view their grades for their classes: ");

            //Validate the user input
            while (choice > students.Count || choice <= 0)
            {
                Console.WriteLine("You have made an invalid seleciont. Please try again.");

                choice = Validation.GetInt("Please select a student to view their grades for their classes: ");
            }

            //Get the GPA from our scores
            total = students[choice-1].FirstGrade + students[choice-1].SecondGrade + students[choice-1].ThirdGrade + students[choice-1].ForthGrade + students[choice-1].FifthGrade;

            //Perform the calculation to get our GPA
            gpa = total / 5;

            //Check for the matching grade
            if (gpa > 89.5)
            {
                grade = "A";
            }
            else if (gpa > 79.5 && gpa < 89.4)
            {
                grade = "B";
            }
            else if (gpa > 72.5 && gpa < 79.4)
            {
                grade = "C";
            }
            else if (gpa > 69.5 && gpa < 72.4)
            {
                grade = "D";
            }
            else if (gpa < 69.4)
            {
                grade = "F";
            }

            //Print the final GPA and the letter grade
            Console.WriteLine($"The final GPA for {students[choice - 1].FirstName} is {gpa} and the Grade is {grade}.");

        }
        public static void ClassGpa(List<Student> students)
        {
            //Clear the console for easy reading
            Console.Clear();

            //Variables for our GPA
            double finalTotal = 0;
            double total = 0;
            double gpa = 0;

            //Loop to add up the scores
            for (int i = 0; i < students.Count; i++)
            {
                total = students[i].FirstGrade + students[i].SecondGrade + students[i].ThirdGrade + students[i].ForthGrade + students[i].FifthGrade;
                
                finalTotal += total;
            }

            //Perform the calculations to get our GPA
            gpa = finalTotal / 25;
            
            //Variables for our letter grade
            string grade = null;

            //Check for the matching letter grade
            if (gpa > 89.5)
            {
                grade = "A";
            }
            else if (gpa > 79.5 && gpa < 89.4)
            {
                grade = "B";
            }
            else if (gpa > 72.5 && gpa <79.4)
            {
                grade = "C";
            }
            else if (gpa > 69.5 && gpa < 72.4)
            {
                grade = "D";
            }
            else if (gpa < 69.4)
            {
                grade = "F";
            }

            //Print out the final GPA and the letter grade
            Console.WriteLine($"The final GPA for the class is {gpa} and the Grade is {grade}.");

        }
    }
}
