﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodingExercise2
{
    class Program
    {
        static void Main(string[] args)
        {
             /*
             Jason Joyner
             DVP2
             Coding Exercise 2
             05/29/2018
             */

            //Allow us to give clues at random
            Random random = new Random();

            //Welcome the user to the program
            Console.WriteLine("Welcome to the Guess the Answer Program\n");

            //Let the customer know what to expect
            Console.WriteLine("You will be given 10 clues at random to help you guess the correct answer.\nYou may get the same clue more than once since they are chosen at random.\nLet's begin with the first clue.\n");

            //List of the clues to help figure out the answer
            List<string> list = new List<string>();
            list.Add("This coffee brand started out in Seattle Washington");
            list.Add("This coffee brand has a word often used to describe money in its name");
            list.Add("This coffee brand is popular all over the country");
            list.Add("This coffee brand has been around for over 40 years");
            list.Add("This company is named after the chief mate in the book Moby-Dick");
            list.Add("This coffee company has over 28,000 locations worldwide");
            list.Add("The companies original logo was an image of a siren from a 16th century Nordic woodcut");
            list.Add("68 of Manhattan's 124 of these coffe shops are located within two blocks of another one");
            list.Add("The company was started by three former students of the University of San Francisco");
            list.Add("The company name partly refers to something seen in the night sky");

            //Variable to control our loop
            bool run = true;

            //Loop to continue giving clues until the user guesses the correct answer
            while (run)
            {
                //Generate a random number
                int index = random.Next(list.Count);

                //Give the customer a clue
                Console.WriteLine($"{list[index]}");

                //Prompt the customer for their guess
                string input = Validation.IsNull("What your guess? ");

                //Conditionals to see if the customer guessed the right answer
                if (input == "Starbucks")
                {
                    Console.WriteLine("Congratulations, you guessed the correct answer!!");
                    run = false;
                }               
                else
                {
                    Console.WriteLine("I'm sorry, your guess was incorrect. Please try again with another clue.\nHint: Try spelling with a capital letter and/or check your spelling for any errors. Good luck.");
                    run = true;
                }
                Utility.PauseBeforeContinuing();
            }
        }
    }
}
