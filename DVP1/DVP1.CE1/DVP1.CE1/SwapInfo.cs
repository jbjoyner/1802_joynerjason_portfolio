﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public class SwapInfo
    {
        public SwapInfo()
        {
            //Name:Jason Joyner
            //Date:1802
            //Course:Project and Portfolii 1
            //Synopsis:Take a users name and reverse it so first name becomes last and last becomes first and display it to the user

            //Ask the user to type in their first name
            Console.WriteLine("Please type in your first name and press enter.");

            //Capture the user input and store it in a variable
            string userFirstName = Console.ReadLine();

            //Create a loop to ensure the user enters their first name
            while (string.IsNullOrWhiteSpace(userFirstName))
            {
                //Inform the user what they did wrong and ask them to try again
                Console.WriteLine("Please do not leave this blank, please enter your name.");

                //Recapture the user input
                userFirstName = Console.ReadLine();
            }

            //Ask the user to type in their last name
            Console.WriteLine("Please type in your last name and press enter.");

            //Capture the user input and store it in a variable
            string userLastName = Console.ReadLine();

            //Create a loop to ensure the user enters their first name
            while (string.IsNullOrWhiteSpace(userLastName))
            {
                //Inform the user what they did wrong and ask them to try again
                Console.WriteLine("Please do not leave this blank, please enter your name.");

                //Recapture the user input
                userLastName = Console.ReadLine();
            }

            //Output the user first name to the console
            Console.WriteLine("The first name entered is " + userFirstName);

            //Output the user last name to the console
            Console.WriteLine("The last name entered is " + userLastName);

            //Declare the variables and swap them with the others
            string tempName1 = userFirstName;
            string tempName2 = userLastName;
            userFirstName = tempName2;
            userLastName = tempName1;

            //Output to the console
            Console.WriteLine("The user first name is now " + userFirstName);
            Console.WriteLine("The user last name is now " + userLastName);

        }
    }
}
