﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public class Menu
    {
        public Menu()
        {
            //Name:Jason Joyner
            //Date:1802
            //Course:Project and Portfolio 1
            //Synopsis:Menu to ask the user what program they want to run

            Console.Clear();
            //Ask the user to make a selection from the menu
            Console.WriteLine("Please choose a selection from the menu\r\n(1) for Swapinfo \r\n(2) for Backward \r\n(3) for AgeConverter \r\n(4) for TempConverter \r\n(5) for exit");

            //Capture the user input and store it in a variable
            string choice = Console.ReadLine();

            //Declare a variable to hold our converted value
            int userChoice;

            //While loop to ensure the user enters a number
            while (!(int.TryParse(choice, out userChoice)))
            {
                //Enform the user what they did wrong and ask them to try again
                Console.WriteLine("Please only choose a number. Try again.");

                //Recapture the user input
                choice = Console.ReadLine();
            }

            //Loop to figure out which choice the user has made
            while (!(userChoice == 5))
            {
                switch (userChoice)
                {
                    case 1:
                        SwapInfo Swap = new SwapInfo();
                        break;
                    case 2:
                        Backwards Reverse = new Backwards();
                        break;
                    case 3:
                        AgeConverter Age = new AgeConverter();
                        break;
                    case 4:
                        TempConverter Temperature = new TempConverter();
                        break;
                }
                
                //Ask the user to make a selection from the menu
                Console.WriteLine("Please choose a selection from the menu\r\n(1) for Swapinfo \r\n(2) for Backward \r\n(3) for AgeConverter \r\n(4) for TempConverter \r\n(5) for exit");

                //Capture the user input and store it in a variable
                choice = Console.ReadLine();

                //While loop to ensure the user enters a number
                while (!(int.TryParse(choice, out userChoice)))
                {
                    //Enform the user what they did wrong and ask them to try again
                    Console.WriteLine("Please only choose a number. Try again.");

                    //Recapture the user input
                    choice = Console.ReadLine();
                }
                Console.Clear();
            }
        }
    }
}
