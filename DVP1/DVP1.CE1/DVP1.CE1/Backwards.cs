﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public class Backwards
    {

        public Backwards()
        {
            //Name:Jason Joyner
            //Date:1802
            //Course:Project and Portfolio 1
            //Synopsis:Take a sentence put in by the user and display it backwards

            //Ask the user to enter a sentence with at least 6 words
            Console.WriteLine("Please type in a sentence with at least 6 words in it and the program will display the sentence backwards.");

            //Capture the user input
            string sentence = Console.ReadLine();

            //While loop to ensure the user typed in a sentence and did not leave it blank
            while (string.IsNullOrWhiteSpace(sentence))
            {
                //Inform the user what they did wrong and ask them to try again
                Console.WriteLine("Please do not leave this blank. Please try again.");

                //Recapture the user input
                sentence = Console.ReadLine();
            }

            //Declare a variable to hold our count
            int count = 0;

            //Loop to check to see if 6 words were entered
                for (int i = 0; i < sentence.Length; i++)
                {
                    if (sentence[i] != ' ')
                    {
                        if ((i + 1) == sentence.Length)
                        {
                            count = count + 1;
                        }
                        else
                        {
                            if (sentence[i + 1] == ' ')
                            {
                                count = count + 1;
                            }
                        }
                    }
                }

            //Add up the count and verify that 6 words are entered
            if (count < 6) 
            {
                //Inform the user what they did wrong
                Console.WriteLine("You need to enter at least 6 words");
            }
            else
            {
                string backwardSentence = "";

                //Loop to put our sentence in a backward order
                for (int x = sentence.Length - 1; x >= 0; x--)
                {
                    backwardSentence += sentence[x];
                }

                //Output the original sentence to the user
                Console.WriteLine("Your original sentence was " + sentence);

                //Output the new Backward sentence to the user
                Console.WriteLine("The new sentence is " + backwardSentence);
            }
        }
    }
}
