﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public class AgeConverter
    {
        public AgeConverter()
        {
            //Name:Jason Joyner
            //Date:1802
            //Course:Project and Portfolio 1
            //Synopsis:Convert the users age into how many days, hours, minutes and seconds they have been alive

            //Ask the user to type in their name
            Console.WriteLine("Please type in your name and press enter");

            //Capture the user input
            string name = Console.ReadLine();

            //Verify the user input
            while (string.IsNullOrWhiteSpace(name))
            {
                //Inform the user what they did wrong and ask them to try again
                Console.WriteLine("It appears that your entry was incorrect, please try again.");

                //Recapture the user input
                name = Console.ReadLine();
            }

            //Ask the user to type in their name
            Console.WriteLine("Please type in your age and press enter.");

            //Capture the user input
            string age = Console.ReadLine();

            //Declare a variable to hold the conversion
            double ageConverted = 0;

            //Convert the input from string to integer
            while (!(double.TryParse(age, out ageConverted)))
            {
                //Inform the user what they did wrong and ask them to try again
                Console.WriteLine("It appears that you did not enter a number, please try again.");

                //Recapture the user inputa
                age = Console.ReadLine();
            }

            //Declare the variables we need for the conversion
            double ageDays = ageConverted * 365;
            double ageHours = ageDays * 24;
            double ageMinutes = ageHours * 60;
            double ageSeconds = ageMinutes * 60;

            //Tell the user the inputs they have made
            Console.WriteLine("You entered {0} for your name and {1} for your age.", name , ageConverted);

            //Give the user their age in days
            Console.WriteLine("Your age in days is "+ageDays);

            //Give the user their age in hours
            Console.WriteLine("Your age in hours is "+ageHours);

            //Give the user their age in minutes
            Console.WriteLine("Your age is minutes "+ageMinutes);

            //Give the user their age in seconds
            Console.WriteLine("Your age in seconds is "+ageSeconds);
            
        }
    }
}
