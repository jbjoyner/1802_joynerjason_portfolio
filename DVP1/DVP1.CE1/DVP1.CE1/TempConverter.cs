﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVP1.CE1
{
    public class TempConverter
    {
        public TempConverter()
        {
            //Name:Jason Joyner
            //Date:1802
            //Course:Project and Portfolio 1
            //Synopsis:Convert a temperature from F to C or C to F

            //Declare a variable for the do while loop
            string answer;

            //Start the do while loop
            do
            {
                //Ask the user to input the temperature they would like to convert
                Console.WriteLine("Please type in the temperature you would like to convert, then press enter.");

                //Capture the user input and store it in a variable
                string temp = Console.ReadLine();

                //Declare the variable to hold the converted information
                double temperature;

                //Convert the user input from a string to an int
                while (!(double.TryParse(temp, out temperature)))
                {
                    //Inform the user what they did wrong and ask them to try again
                    Console.WriteLine("It appears that you entered the wrong information, please try again");

                    //Recapture the user input
                    temp = Console.ReadLine();
                }

                //Ask the user to choose whether they input fahrenheit or celsius
                Console.WriteLine("Please enter the correct number for your choice. Enter (1) for Fahrenheit or (2) for Celsius.");

                //Capture the user input and store it in a variable
                string userChoice = Console.ReadLine();

                //Declare the variable to store the converted user input
                int userChoiceNumber;

                //Convert the user input from a string to an integer
                while (!(int.TryParse(userChoice, out userChoiceNumber)))
                {
                    //Inform the user of what they did wrong and ask them to try again
                    Console.WriteLine("It appears that you did not enter the correct choice, please try again");

                    //Recapture the user input
                    userChoice = Console.ReadLine();
                }

                //Declare new variables
                double newTemperature = 0;
                string temperatureType = "";
                string reversedTemperatureType = "";

                if (userChoiceNumber == 1)
                {
                    newTemperature = (temperature - 32) * .5556;
                }
                else if (userChoiceNumber == 2)
                {
                    newTemperature = temperature * 1.8 + 32;
                }
                if (userChoiceNumber == 1)
                {
                    temperatureType = "Fahrenheit";
                }
                else if (userChoiceNumber == 2)
                {
                    temperatureType = "Celsius";
                }
                if (userChoiceNumber == 1)
                {
                    reversedTemperatureType = "Celsius";
                }
                else if (userChoiceNumber == 2)
                {
                    reversedTemperatureType = "Fahrenheit";
                }

                //Show the user their temp choice
                Console.WriteLine("You entered " + temperature + " for the temperature.");

                //Show the user their choice
                Console.WriteLine("You entered " + temperatureType + " for your choice.");

                //Print to the console the return temp
                Console.WriteLine("The new temp is {0} {1}", Math.Round(newTemperature, 2), reversedTemperatureType);

                //Give the user a choice to run another conversion or exit
                Console.WriteLine("\r\nWould you like to enter another conversion or exit? \r\nEnter q to exit or any other key to perform another conversion.");

                //Capture the user input
                answer = Console.ReadLine();

                // Verify the user choice to stay or exit
            } while (answer != "q");
        }
    }
}
