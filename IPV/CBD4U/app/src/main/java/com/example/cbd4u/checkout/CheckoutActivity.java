package com.example.cbd4u.checkout;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.cbd4u.profile.EditProfileActivity;
import com.example.cbd4u.MainActivity;
import com.example.cbd4u.R;
import com.example.cbd4u.login.LoginActivity;
import com.example.cbd4u.objects.Checkout;
import com.example.cbd4u.objects.StoreItems;
import com.example.cbd4u.objects.StoreUser;
import com.example.cbd4u.orders.OrderActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class CheckoutActivity extends AppCompatActivity {

    private static class ViewHolder {
        TextView nameCheckout;
        TextView addressCheckout;
        TextView phoneCheckout;
        TextView emailCheckout;
        TextView quantityCheckout;
        TextView subtotalCheckout;
        TextView feeCheckout;
        TextView totalCheckout;
        Button submit;
        Button edit;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    private StoreUser mStoreUser;

    private static final String CHANNEL_ID_EXAMPLE = "Example_ID";

    private static final int STANDARD_NOTIFICATION = 0x10000000;

    private static final String ORDER_TO_SEND = "Order to Send";

    private int quantity = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout_screen);

        addNotificationChannel(CHANNEL_ID_EXAMPLE, "Example Channel", "Example description...", NotificationManager.IMPORTANCE_DEFAULT);

        loadStoreUSer();

        initializeViews();

        mViewHolder.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkOut();
            }
        });

        mViewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckoutActivity.this, EditProfileActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadStoreUSer();
        initializeViews();
    }

    private void checkOut() {
        final String name = mViewHolder.nameCheckout.getText().toString();
        final String address = mViewHolder.addressCheckout.getText().toString();
        final String phone = mViewHolder.phoneCheckout.getText().toString();
        final String email = mViewHolder.emailCheckout.getText().toString();


        if (name.isEmpty() || address.isEmpty() || phone.isEmpty() || email.isEmpty()) {
            Toast.makeText(CheckoutActivity.this, R.string.blank_field, Toast.LENGTH_SHORT).show();
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle(R.string.success);
            dialog.setMessage(R.string.submitted);
            dialog.setNeutralButton(R.string.cancel, null);
            dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ArrayList<StoreItems> items = mStoreUser.getItems();
                    float subTotal = 0f;
                    quantity = 0;
                    for (StoreItems item:items
                    ) {
                        subTotal += item.getPrice() * item.getQuantityOrdered();
                        quantity += item.getQuantityOrdered();
                    }
                    float fee =  quantity * 2f;
                    float total = subTotal + fee;
                    Checkout checkout = new Checkout(name, address, phone, email, quantity, subTotal, fee, mStoreUser.getCheckins(), total, items);
                    mStoreUser.setItems( new ArrayList<StoreItems>());
                    saveData();
                    loadStoreUSer();
                    onStandardNotification(checkout);
                    finishAffinity();
                    Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });
            dialog.show();
        }
    }

    private void initializeViews() {
        mViewHolder.nameCheckout = findViewById(R.id.name_checkout);
        mViewHolder.addressCheckout = findViewById(R.id.address_checkout);
        mViewHolder.phoneCheckout = findViewById(R.id.phone_checkout);
        mViewHolder.emailCheckout = findViewById(R.id.email_checkout);
        mViewHolder.quantityCheckout = findViewById(R.id.quantity_checkout);
        mViewHolder.subtotalCheckout = findViewById(R.id.subtotal_checkout);
        mViewHolder.feeCheckout = findViewById(R.id.fee_checkout);
        mViewHolder.totalCheckout = findViewById(R.id.total_checkout);
        mViewHolder.submit = findViewById(R.id.btnEnter);
        mViewHolder.edit = findViewById(R.id.btnEditDelivery);
        ArrayList<StoreItems> items = mStoreUser.getItems();
        float subTotal = 0f;
        quantity = 0;
        if (items != null) {
            for (StoreItems item : items
            ) {
                subTotal += item.getPrice() * item.getQuantityOrdered();
                quantity += item.getQuantityOrdered();
            }
        }
        mViewHolder.quantityCheckout.setText(String.valueOf(quantity));
        mViewHolder.nameCheckout.setText(mStoreUser.getName());
        mViewHolder.addressCheckout.setText(mStoreUser.getAddress());
        mViewHolder.phoneCheckout.setText(mStoreUser.getPhone());
        mViewHolder.emailCheckout.setText(mStoreUser.getEmail());
        String COUNTRY = "US";
        String LANGUAGE = "en";
        String subtotalFormatted = NumberFormat.getCurrencyInstance(new Locale(LANGUAGE, COUNTRY)).format(subTotal);
        mViewHolder.subtotalCheckout.setText(subtotalFormatted);
        float fee = 0;
        if (items != null) {
            fee = quantity * 2f;
        }
        float total = subTotal + fee;
        String feeFormatted = NumberFormat.getCurrencyInstance(new Locale(LANGUAGE, COUNTRY)).format(fee);
        String totalFormatted = NumberFormat.getCurrencyInstance(new Locale(LANGUAGE, COUNTRY)).format(total);
        mViewHolder.feeCheckout.setText(feeFormatted);
        mViewHolder.totalCheckout.setText(totalFormatted);
    }

    void addNotificationChannel(String _ID, String _name, String _description, int _importance) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(_ID, _name, _importance);
            channel.setDescription(_description);
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

    private void onStandardNotification(Checkout checkout) {
        Intent intent = new Intent(CheckoutActivity.this, OrderActivity.class);
        intent.putExtra(ORDER_TO_SEND, checkout);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(CheckoutActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(CheckoutActivity.this, CHANNEL_ID_EXAMPLE);
        builder.setSmallIcon(R.drawable.delivery);
        builder.setContentTitle("New Order");
        builder.setContentText(checkout.getName());
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);

        NotificationManagerCompat mgr = NotificationManagerCompat.from(this);
        mgr.notify(STANDARD_NOTIFICATION, builder.build());
    }

    private void loadStoreUSer() {
        SharedPreferences sharedPreferences = CheckoutActivity.this.getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(LoginActivity.STORE_USER, null);
        Type type = new TypeToken<StoreUser>() {}.getType();
        mStoreUser = gson.fromJson(json, type);

        if (mStoreUser == null) {
            mStoreUser = new StoreUser();
        }
    }

    private void saveData() {
        SharedPreferences sharedPreferences = CheckoutActivity.this.getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mStoreUser);
        editor.putString(LoginActivity.STORE_USER, json);
        editor.apply();
    }
}
