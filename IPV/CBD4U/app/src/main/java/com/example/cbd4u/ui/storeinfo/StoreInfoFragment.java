package com.example.cbd4u.ui.storeinfo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.cbd4u.R;

public class StoreInfoFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_store, container, false);
        final TextView textView = root.findViewById(R.id.text_store);

        textView.setText(getString(R.string.store_info));

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            ListView lv = view.findViewById(R.id.store_info_listview);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    switch (position) {
                        case 0:
                            openMaps();
                            break;
                        case 1:
                            phone();
                            break;
                        case 2:
                            sendEmail();
                            break;
                        case 3:
                            AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                            builder.setTitle(R.string.hours);
                            builder.setMessage(R.string.hours_of_operation);
                            builder.setNegativeButton(R.string.close, null);
                            builder.show();
                            break;
                    }
                }
            });
        }
    }

    private void sendEmail() {

        String[] TO = {getString(R.string.email)};
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);

        emailIntent.setData(Uri.parse(getString(R.string.email))).setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void phone() {

        Uri uri = Uri.parse("tel:" + getString(R.string.phone_number));

        Intent i = new Intent(Intent.ACTION_DIAL, uri);

        try {
            startActivity(i);
        } catch (SecurityException s) {
            s.printStackTrace();
        }
    }

    private void openMaps() {

        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + getString(R.string.address));

        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        startActivity(mapIntent);
    }
}