package com.example.cbd4u.admin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cbd4u.R;
import com.example.cbd4u.database.DataBaseHelper;
import com.example.cbd4u.login.LoginActivity;
import com.example.cbd4u.objects.StoreItems;
import com.example.cbd4u.objects.StoreUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Objects;

public class AdminActivity extends AppCompatActivity {

    private static final String PRODUCTS = "Products";
    private static final String TAG = "HomeFragment.tag";
    public static final String ITEM = "Item";
    private FirebaseStorage mStorage;
    private DataBaseHelper mHelper;
    private Cursor mCursor;
    private GridView mGridView;
    private TodoCursorAdapter mAdapter;
    private StoreUser mStoreUser;

    private static class ViewHolder {
        TextView nameText;
        TextView mgText;
        TextView categoryText;
        TextView promotionText;
        TextView quantityText;
        TextView priceText;
    }

    private final ViewHolder mViewHolder = new ViewHolder();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_layout);
        mHelper = DataBaseHelper.getINSTANCE(this);
        mHelper.DeleteAllItems();
        mStorage = FirebaseStorage.getInstance();
        loadStoreUSer();
        loadViews();
    }

    private void loadViews() {
        getAllItems();

        mGridView = findViewById(R.id.admin_gridView);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mStoreUser.isAdmin()) {
                    if (mCursor.moveToPosition(position)) {
                        final String name = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_NAME));
                        final String itemId = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_ITEM_ID));
                        final String mg = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_MG));
                        final double price = mCursor.getDouble(mCursor.getColumnIndex(DataBaseHelper.COLUMN_PRICE));
                        final String description = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_DESCRIPTION));
                        final String category = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_CATEGORY));
                        final String promotion = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_PROMOTION));
                        final String quantity = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_QUANTITY));
                        final String image = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_IMAGE));
                        StoreItems item = new StoreItems(name, itemId, description, price, quantity, 0, mg, category, promotion, image);
                        Intent intent = new Intent(AdminActivity.this, AdminEditing.class);
                        intent.putExtra(ITEM, item);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Toast.makeText(AdminActivity.this, R.string.not_admin, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loadStoreUSer() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(LoginActivity.STORE_USER, null);
        Type type = new TypeToken<StoreUser>() {}.getType();
        mStoreUser = gson.fromJson(json, type);

        if (mStoreUser == null) {
            mStoreUser = new StoreUser();
        }
    }

    private void getAllItems() {
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection(PRODUCTS)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                String id = document.getId();
                                StoreItems items = document.toObject(StoreItems.class);
                                mHelper.insertItem(items.getName(), id, items.getMgStrength(), items.getPrice(), items.getDescription(), items.getCategory(), items.getPromotion(), items.getQuantity(), items.getImage());
                                mCursor = mHelper.getAllData();
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        mCursor = mHelper.SortNameAscending();
                        mAdapter = new TodoCursorAdapter(AdminActivity.this,  mCursor);
                        mGridView.setAdapter(mAdapter);
                    }
                });
    }

    private class TodoCursorAdapter extends CursorAdapter {
        public TodoCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor, 0);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return LayoutInflater.from(context).inflate(R.layout.admin_grid_layout, parent, false);
        }

        @Override
        public void bindView(View view, final Context context, Cursor cursor) {
            mViewHolder.nameText = view.findViewById(R.id.admin_item_name_text);
            mViewHolder.mgText = view.findViewById(R.id.admin_mg_text);
            mViewHolder.priceText = view.findViewById(R.id.admin_price_text);
            mViewHolder.categoryText = view.findViewById(R.id.admin_category_label);
            mViewHolder.promotionText = view.findViewById(R.id.admin_promo_text);
            mViewHolder.quantityText = view.findViewById(R.id.admin_quantity_text);
            final ImageView imageView = view.findViewById(R.id.image_view);

            String name = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_NAME));
            String mg = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_MG));
            double price = cursor.getDouble(cursor.getColumnIndex(DataBaseHelper.COLUMN_PRICE));
            String image = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_IMAGE));
            String category = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_CATEGORY));
            String promotion = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_PROMOTION));
            String quantity = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_QUANTITY));

            StorageReference gsReference = mStorage.getReferenceFromUrl(image);

            gsReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).into(imageView);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    exception.printStackTrace();
                }
            });

            mViewHolder.categoryText.setText(category);
            mViewHolder.nameText.setText(name);
            mViewHolder.mgText.setText(String.format("%smg", mg));
            mViewHolder.priceText.setText(String.format("$%s", price));
            mViewHolder.promotionText.setText(promotion);
            mViewHolder.quantityText.setText(quantity);
        }
    }
}
