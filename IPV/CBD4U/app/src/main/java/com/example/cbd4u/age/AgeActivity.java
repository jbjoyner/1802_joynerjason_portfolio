package com.example.cbd4u.age;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cbd4u.R;
import com.example.cbd4u.login.LoginActivity;
import com.example.cbd4u.objects.StoreUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class AgeActivity extends AppCompatActivity {

    private Button mBtnEnter;
    private Button mBtnAdmin;
    private StoreUser mStoreUser;
    private CheckBox ageCheckbox;
    private CheckBox adminCheckbox;
    private boolean adminChecked;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.age_verification);
        mBtnEnter = findViewById(R.id.btnEnter);
        mBtnEnter.setVisibility(View.INVISIBLE);
        mBtnAdmin = findViewById(R.id.btnAdmin);
        mBtnAdmin.setVisibility(View.INVISIBLE);
        loadStoreUSer();
        if (mStoreUser.isAge() || mStoreUser.isAdmin()) {
                launchLogIn();
        }
        ageCheckbox = findViewById(R.id.age_checkBox);
        adminCheckbox = findViewById(R.id.admin_checkbox);
    }

    public void checkAge(View view) {
        switch (view.getId()) {

            case R.id.age_checkBox:
                boolean ageChecked = ageCheckbox.isChecked();
                if (ageChecked) {
                    if (adminChecked) {
                    mBtnAdmin.setVisibility(View.VISIBLE);
                    mBtnEnter.setVisibility(View.INVISIBLE);
                        } else {
                        mBtnAdmin.setVisibility(View.INVISIBLE);
                        mBtnEnter.setVisibility(View.VISIBLE);
                        mBtnEnter.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mStoreUser.setAge(true);
                                mStoreUser.setAdmin(false);
                                saveData();
                                launchLogIn();
                            }
                        });
                    }
                } else {
                    if (adminCheckbox.isChecked()) {
                        mBtnEnter.setVisibility(View.INVISIBLE);
                        mBtnAdmin.setVisibility(View.VISIBLE);
                    } else {
                        mBtnEnter.setVisibility(View.INVISIBLE);
                        mBtnAdmin.setVisibility(View.INVISIBLE);
                    }
                }
                break;

            case R.id.admin_checkbox:
                adminChecked = adminCheckbox.isChecked();
                if (adminChecked) {
                    mBtnEnter.setVisibility(View.INVISIBLE);
                    mBtnAdmin.setVisibility(View.VISIBLE);
                    mBtnAdmin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mStoreUser.setAge(true);
                            mStoreUser.setAdmin(true);
                            saveData();
                            launchLogIn();
                        }
                    });
                } else {
                    if (ageCheckbox.isChecked()) {
                        mBtnEnter.setVisibility(View.VISIBLE);
                        mBtnAdmin.setVisibility(View.INVISIBLE);
                    } else {
                        mBtnAdmin.setVisibility(View.INVISIBLE);
                        mBtnEnter.setVisibility(View.INVISIBLE);
                    }
                }
                break;
        }
    }

    private void saveData() {
        SharedPreferences sharedPreferences = AgeActivity.this.getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mStoreUser);
        editor.putString(LoginActivity.STORE_USER, json);
        editor.apply();
    }

    private void loadStoreUSer() {
        SharedPreferences sharedPreferences = AgeActivity.this.getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(LoginActivity.STORE_USER, null);
        Type type = new TypeToken<StoreUser>() {}.getType();
        mStoreUser = gson.fromJson(json, type);

        if (mStoreUser == null) {
            mStoreUser = new StoreUser();
        }
    }

    private void launchLogIn() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
