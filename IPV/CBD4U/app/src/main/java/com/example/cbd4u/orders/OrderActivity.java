package com.example.cbd4u.orders;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cbd4u.R;
import com.example.cbd4u.adapters.OrderAdapter;
import com.example.cbd4u.objects.Checkout;
import com.example.cbd4u.objects.StoreItems;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class OrderActivity extends AppCompatActivity {

    private static final String ORDER_TO_SEND = "Order to Send";

    private Checkout checkout;

    private static class ViewHolder {
        TextView nameText;
        TextView addressText;
        TextView phoneText;
        TextView emailText;
        TextView numItems;
        TextView checkins;
        TextView totalText;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_received_layout);

        Intent intent = getIntent();
        checkout = (Checkout) intent.getSerializableExtra(ORDER_TO_SEND);

        initializeViews();
    }

    private void initializeViews() {
        mViewHolder.nameText = findViewById(R.id.nameTxt);
        mViewHolder.addressText = findViewById(R.id.addTxt);
        mViewHolder.phoneText = findViewById(R.id.phTxt);
        mViewHolder.emailText = findViewById(R.id.emailTxt);
        mViewHolder.numItems = findViewById(R.id.noTxt);
        mViewHolder.checkins = findViewById(R.id.checkins_textview);
        mViewHolder.totalText = findViewById(R.id.totTxt);
        mViewHolder.numItems.setText(String.valueOf(checkout.getQuantity()));
        mViewHolder.nameText.setText(checkout.getName());
        mViewHolder.addressText.setText(checkout.getAddress());
        mViewHolder.phoneText.setText(checkout.getPhone());
        mViewHolder.checkins.setText(String.valueOf(checkout.getCheckins()));
        mViewHolder.emailText.setText(checkout.getEmail());
        String COUNTRY = "US";
        String LANGUAGE = "en";
        String totalFormatted = NumberFormat.getCurrencyInstance(new Locale(LANGUAGE, COUNTRY)).format(checkout.getTotal());
        mViewHolder.totalText.setText(totalFormatted);

        ListView view = findViewById(R.id.orderListView);
        ArrayList<StoreItems> items = checkout.getItems();
        OrderAdapter adapter = new OrderAdapter(items, this);
        view.setAdapter(adapter);
    }
}
