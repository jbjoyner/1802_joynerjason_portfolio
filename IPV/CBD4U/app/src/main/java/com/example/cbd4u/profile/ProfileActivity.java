package com.example.cbd4u.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cbd4u.MainActivity;
import com.example.cbd4u.R;
import com.example.cbd4u.login.LoginActivity;
import com.example.cbd4u.objects.StoreUser;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class ProfileActivity extends AppCompatActivity {

    private static class ViewHolder {
        EditText nameText;
        EditText addressText;
        EditText phoneText;
        EditText emailText;
        Button btnSave;
    }
    private final ViewHolder mViewHolder = new ViewHolder();
    private StoreUser mStoreUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_profile_layout);
        loadStoreUSer();
        if (mStoreUser != null) {
            if (mStoreUser.getName() == null || mStoreUser.getAddress() == null || mStoreUser.getPhone() == null || mStoreUser.getEmail() == null) {
        loadView();
        saveUserInput();
            }
        }

        if (mStoreUser != null && mStoreUser.getName() != null && mStoreUser.getAddress() != null && mStoreUser.getPhone() != null && mStoreUser.getEmail() != null) {
            launchHome();
        }

        Places.initialize(getApplicationContext(), "AIzaSyC7QtcfDjxsPVXp3KQKaP9ew4wiAmxsyLk");
        if (mViewHolder.addressText != null) {
            mViewHolder.addressText.setFocusable(false);
            mViewHolder.addressText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS,
                            Place.Field.NAME,
                            Place.Field.ID);

                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,
                            fieldList)
                            .build(ProfileActivity.this);
                    startActivityForResult(intent, 100);
                }
            });
        }
        formatPhone();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK) {
            Place place = null;
            if (data != null) {
                place = Autocomplete.getPlaceFromIntent(data);
            }
            if (place != null) {
                mViewHolder.addressText.setText(place.getAddress());
            }
        } else if (requestCode == AutocompleteActivity.RESULT_ERROR) {
            Status status = null;
            if (data != null) {
                status = Autocomplete.getStatusFromIntent(data);
            }
            if (status != null) {
                Toast.makeText(getApplicationContext(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void saveUserInput() {
        mViewHolder.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = mViewHolder.nameText.getText().toString();
                final String address = mViewHolder.addressText.getText().toString();
                final String phone = mViewHolder.phoneText.getText().toString();
                final String email = mViewHolder.emailText.getText().toString();

                if (name.isEmpty() || address.isEmpty() || phone.isEmpty() || email.isEmpty()) {
                    Toast.makeText(ProfileActivity.this, R.string.blank_field, Toast.LENGTH_SHORT).show();
                } else {
                    mStoreUser.setName(name);
                    mStoreUser.setAddress(address);
                    mStoreUser.setPhone(phone);
                    mStoreUser.setEmail(email);
                    saveData();
                    launchHome();
                }
            }
        });
    }

    private void launchHome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void loadView() {
        mViewHolder.nameText = findViewById(R.id.create_name);
        mViewHolder.addressText = findViewById(R.id.create_address);
        mViewHolder.phoneText = findViewById(R.id.create_phone);
        mViewHolder.emailText = findViewById(R.id.create_email);
        mViewHolder.btnSave = findViewById(R.id.btnCreateSave);

        mViewHolder.nameText.setText(mStoreUser.getName());
        mViewHolder.addressText.setText(mStoreUser.getAddress());
        mViewHolder.phoneText.setText(mStoreUser.getPhone());
        mViewHolder.emailText.setText(mStoreUser.getEmail());
    }

    private void loadStoreUSer() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(LoginActivity.STORE_USER, null);
        Type type = new TypeToken<StoreUser>() {}.getType();
        mStoreUser = gson.fromJson(json, type);

        if (mStoreUser == null) {
            mStoreUser = new StoreUser();
        }
    }

    private void saveData() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mStoreUser);
        editor.putString(LoginActivity.STORE_USER, json);
        editor.apply();
    }

    private void formatPhone() {
        if (mViewHolder.phoneText != null) {
            mViewHolder.phoneText.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
                private boolean backSpace = false;
                private boolean edited = false;
                private int cursor;

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    cursor = s.length() - mViewHolder.phoneText.getSelectionStart();
                    backSpace = count > after;
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    String string = s.toString();
                    String phone = string.replaceAll("[^\\d]", "");
                    if (!edited) {
                        if (phone.length() >= 6 && !backSpace) {
                            edited = true;
                            String number = "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6);
                            mViewHolder.phoneText.setText(number);
                            mViewHolder.phoneText.setSelection(mViewHolder.phoneText.getText().length() - cursor);
                        } else if (phone.length() >= 3 && !backSpace) {
                            edited = true;
                            String ans = "(" + phone.substring(0, 3) + ") " + phone.substring(3);
                            mViewHolder.phoneText.setText(ans);
                            mViewHolder.phoneText.setSelection(mViewHolder.phoneText.getText().length() - cursor);
                        }
                    } else {
                        edited = false;
                    }
                }
            });
        }
    }

}
