package com.example.cbd4u.broadcastreceiver;

import android.content.Context;
import android.content.Intent;

import com.example.cbd4u.checkin.Checkin;

public class GeofenceBroadcastReceiver extends android.content.BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent checkinIntent = new Intent(context, Checkin.class);
        context.startActivity(checkinIntent);
    }
}
