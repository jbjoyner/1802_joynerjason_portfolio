package com.example.cbd4u.ui.benefitsAndUses;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.cbd4u.R;
import com.example.cbd4u.adapters.SectionsPagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class BenefitsAndUsesFragment extends Fragment {

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private SectionsPagerAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.benefits_fragment_layout, container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadView();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadView();
    }

    private void loadView() {
        mViewPager = requireView().findViewById(R.id.viewPager);
        setupViewPager(mViewPager);

        mTabLayout = requireView().findViewById(R.id.tabLayout);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        mAdapter = new SectionsPagerAdapter(getParentFragmentManager(), 2);
        mAdapter.addFragment(new BenefitsFragment(), "Benefits");
        mAdapter.addFragment(new UsesFragment(), "Uses");
        viewPager.setAdapter(mAdapter);
    }
}
