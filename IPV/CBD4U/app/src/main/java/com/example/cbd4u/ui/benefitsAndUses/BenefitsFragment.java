package com.example.cbd4u.ui.benefitsAndUses;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cbd4u.R;

public class BenefitsFragment extends Fragment {

    private static class ViewHolder {
        TextView benefits;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_benefits, container, false);
        mViewHolder.benefits = root.findViewById(R.id.benefits);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewHolder.benefits.setText(R.string.benefits);
    }
}