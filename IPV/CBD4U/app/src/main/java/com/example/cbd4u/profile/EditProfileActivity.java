package com.example.cbd4u.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cbd4u.R;
import com.example.cbd4u.login.LoginActivity;
import com.example.cbd4u.objects.StoreUser;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class EditProfileActivity extends AppCompatActivity {
    private static class ViewHolder {
        TextView banner;
        EditText nameText;
        EditText addressText;
        EditText phoneText;
        EditText emailText;
        Button btnSave;
    }
    private final ViewHolder mViewHolder = new ViewHolder();
    private StoreUser mStoreUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_edit_layout);
        mViewHolder.banner = findViewById(R.id.profile_banner);
        mViewHolder.banner.setText(R.string.update_profile);
        loadStoreUSer();
        loadViews();
        formatPhone();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            Place place = null;
            if (data != null) {
                place = Autocomplete.getPlaceFromIntent(data);
            }

            if (place != null) {
                mViewHolder.addressText.setText(place.getAddress());
            }
        } else if (requestCode == AutocompleteActivity.RESULT_ERROR) {
            Status status = null;
            if (data != null) {
                status = Autocomplete.getStatusFromIntent(data);
            }
            if (status != null) {
                Toast.makeText(this, status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loadStoreUSer() {
        SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(LoginActivity.STORE_USER, null);
        Type type = new TypeToken<StoreUser>() {}.getType();
        mStoreUser = gson.fromJson(json, type);

        if (mStoreUser == null) {
            mStoreUser = new StoreUser();
        }
    }

    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mStoreUser);
        editor.putString(LoginActivity.STORE_USER, json);
        editor.apply();
    }

    private void loadViews() {

        mViewHolder.nameText = findViewById(R.id.create_name);
        mViewHolder.addressText = findViewById(R.id.create_address);
        mViewHolder.phoneText = findViewById(R.id.create_phone);
        mViewHolder.emailText = findViewById(R.id.create_email);
        mViewHolder.btnSave = findViewById(R.id.btnCreateSave);

        Places.initialize(this, "AIzaSyC7QtcfDjxsPVXp3KQKaP9ew4wiAmxsyLk");

        mViewHolder.addressText.setFocusable(false);
        mViewHolder.addressText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS,
                        Place.Field.ID,
                        Place.Field.NAME);

                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,
                        fieldList)
                        .build(EditProfileActivity.this);
                startActivityForResult(intent, 100);
            }
        });


        mViewHolder.nameText.setText(mStoreUser.getName());
        mViewHolder.addressText.setText(mStoreUser.getAddress());
        mViewHolder.phoneText.setText(mStoreUser.getPhone());
        mViewHolder.emailText.setText(mStoreUser.getEmail());
        mViewHolder.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveButtonClick();
            }
        });
    }

    private void saveButtonClick() {
        String name = mViewHolder.nameText.getText().toString();
        String address = mViewHolder.addressText.getText().toString();
        String phone = mViewHolder.phoneText.getText().toString();
        String email = mViewHolder.emailText.getText().toString();
        mViewHolder.phoneText.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        if (name.isEmpty() || address.isEmpty() || phone.isEmpty() || email.isEmpty()) {
            Toast.makeText(this, R.string.blank_field, Toast.LENGTH_SHORT).show();
        } else {
            mStoreUser.setName(name);
            mStoreUser.setAddress(address);
            mStoreUser.setPhone(phone);
            mStoreUser.setEmail(email);
            saveData();
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle(R.string.success);
            dialog.setMessage(R.string.profile_updated);
            dialog.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            dialog.show();
        }
    }

    private void formatPhone() {
        mViewHolder.phoneText.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            private boolean backSpace = false;
            private boolean edited = false;
            private int cursor;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                cursor = s.length()-mViewHolder.phoneText.getSelectionStart();
                backSpace = count > after;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                String phone = string.replaceAll("[^\\d]", "");
                if (!edited) {
                    if (phone.length() >= 6 && !backSpace) {
                        edited = true;
                        String number = "(" + phone.substring(0, 3) + ") " + phone.substring(3,6) + "-" + phone.substring(6);
                        mViewHolder.phoneText.setText(number);
                        mViewHolder.phoneText.setSelection(mViewHolder.phoneText.getText().length()- cursor);
                    } else if (phone.length() >= 3 && !backSpace) {
                        edited = true;
                        String ans = "(" +phone.substring(0, 3) + ") " + phone.substring(3);
                        mViewHolder.phoneText.setText(ans);
                        mViewHolder.phoneText.setSelection(mViewHolder.phoneText.getText().length()- cursor);
                    }
                } else {
                    edited = false;
                }
            }
        });
    }
}
