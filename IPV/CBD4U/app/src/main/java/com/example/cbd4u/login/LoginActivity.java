package com.example.cbd4u.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cbd4u.R;
import com.example.cbd4u.objects.StoreUser;
import com.example.cbd4u.profile.ProfileActivity;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity {
    private static final int MY_REQUEST_CODE = 123;
    public static final String STORE_USER = "storeUser";
    private List<AuthUI.IdpConfig> mProviders;
    private StoreUser mStoreUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_screen);

        mProviders = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build()
        );
        showSignInOptions();

        loadStoreUSer();
    }

    private void showSignInOptions() {
        startActivityForResult(
                AuthUI.getInstance().createSignInIntentBuilder()
                        .setAvailableProviders(mProviders)
                        .setLogo(R.drawable.logo)
                        .setTheme(R.style.AppTheme)
                        .build(), MY_REQUEST_CODE
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_REQUEST_CODE) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                startActivity(new Intent(this, ProfileActivity.class));
                finish();
                if (user != null) {
                    mStoreUser.setName(user.getDisplayName());
                    mStoreUser.setEmail(user.getEmail());
                    mStoreUser.setCheckedIn(false);
                    saveData();
                }
            } else {
                if (response != null) {
                    Toast.makeText(this, "" + Objects.requireNonNull(response.getError()).getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void loadStoreUSer() {
        SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(LoginActivity.STORE_USER, null);
        Type type = new TypeToken<StoreUser>() {}.getType();
        mStoreUser = gson.fromJson(json, type);

        if (mStoreUser == null) {
            mStoreUser = new StoreUser();
        }
    }

    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mStoreUser);
        editor.putString(LoginActivity.STORE_USER, json);
        editor.apply();
    }
}
