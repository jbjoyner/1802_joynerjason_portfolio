package com.example.cbd4u.ui.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.cbd4u.R;
import com.example.cbd4u.admin.AdminPassword;
import com.example.cbd4u.database.DataBaseHelper;
import com.example.cbd4u.geofence.GeofenceHelper;
import com.example.cbd4u.login.LoginActivity;
import com.example.cbd4u.objects.StoreItems;
import com.example.cbd4u.objects.StoreUser;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Objects;

public class HomeFragment extends Fragment {

    private static final String PRODUCTS = "Products";
    private static final String TAG = "HomeFragment.tag";

    private FirebaseStorage mStorage;
    private DataBaseHelper mHelper;
    private Cursor mCursor;
    private GridView mGridView;
    private TodoCursorAdapter mAdapter;
    private ArrayList<StoreItems> mItems;
    private StoreItems mStoreItems;
    private StoreUser mStoreUser;
    private boolean price = false;
    private boolean name = false;
    private boolean featured = false;
    private boolean sales = false;
    private boolean newItem = false;

    public HomeFragment() {
    }

    private static class ViewHolder {
        TextView nameText;
        TextView mgText;
        TextView descriptionText;
        TextView categoryText;
        TextView promotionText;
        TextView priceText;
        TextView textView;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadStoreUSer();
        getAllItems();
        mHelper = DataBaseHelper.getINSTANCE(getContext());
        mHelper.DeleteAllItems();
        mStorage = FirebaseStorage.getInstance();

        if (mStoreUser.getItems() == null) {
            mItems = new ArrayList<>();
        } else {
            mItems = mStoreUser.getItems();
        }
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Places.initialize(requireContext(), "AIzaSyC7QtcfDjxsPVXp3KQKaP9ew4wiAmxsyLk");
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        mViewHolder.textView = root.findViewById(R.id.text_home);
        mViewHolder.textView.setText(PRODUCTS);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        populateSpinners();
        loadView();
    }

    private void loadView() {
        View view = getView();

        if (view != null) {
            mGridView = view.findViewById(R.id.grid_view);
            mGridView.setClickable(false);
            mGridView.setFocusable(false);
        }

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mCursor.moveToPosition(position)) {
                    final String name = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_NAME));
                    final String itemId = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_ITEM_ID));
                    final String mg = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_MG));
                    final double price = mCursor.getDouble(mCursor.getColumnIndex(DataBaseHelper.COLUMN_PRICE));
                    final String description = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_DESCRIPTION));
                    final String category = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_CATEGORY));
                    final String promotion = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_PROMOTION));
                    final String quantity = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_QUANTITY));
                    final String image = mCursor.getString(mCursor.getColumnIndex(DataBaseHelper.COLUMN_IMAGE));
                    final String priceConverted = String.valueOf(price);

                    AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());

                    builder.setView(R.layout.details_layout);

                    final View customLayout = getLayoutInflater().inflate(R.layout.details_layout, null);
                    builder.setView(customLayout);
                    mViewHolder.nameText = customLayout.findViewById(R.id.name_details_text);
                    mViewHolder.mgText = customLayout.findViewById(R.id.mg_details_text);
                    mViewHolder.descriptionText = customLayout.findViewById(R.id.description_details_text);
                    mViewHolder.categoryText = customLayout.findViewById(R.id.category_details_text);
                    mViewHolder.promotionText = customLayout.findViewById(R.id.promotion_details_text);
                    mViewHolder.priceText = customLayout.findViewById(R.id.price_details_text);
                    final ImageView detailsView = customLayout.findViewById(R.id.details_image);

                    StorageReference gsReference = mStorage.getReferenceFromUrl(image);

                    gsReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Picasso.get().load(uri).into(detailsView);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            exception.printStackTrace();
                        }
                    });

                    mViewHolder.nameText.setText(name);
                    mViewHolder.mgText.setText(String.format("%smg", mg));
                    mViewHolder.priceText.setText(priceConverted);
                    mViewHolder.descriptionText.setText(description);
                    mViewHolder.categoryText.setText(category);
                    mViewHolder.promotionText.setText(promotion);

                    builder.setNeutralButton(getString(R.string.close), null);
                    builder.setPositiveButton(getString(R.string.add), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                            builder.setTitle(R.string.success);
                            builder.setMessage(R.string.item_added);
                            builder.setNeutralButton(R.string.cancel, null);
                            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mStoreItems = new StoreItems(name, itemId, description, price, quantity, 1, mg, category, promotion, image);
                                    mItems.add(mStoreItems);
                                    mStoreUser.setItems(mItems);
                                    saveData();
                                }
                            });
                            builder.show();
                        }
                    });
                    builder.show();
                }
            }
        });

        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (mStoreUser.isAdmin()) {
                    Intent intent = new Intent(getContext(), AdminPassword.class);
                    startActivity(intent);
                    return true;
                } else {
                    Toast.makeText(getContext(), R.string.not_admin, Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        });
    }

    private void loadStoreUSer() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(LoginActivity.STORE_USER, null);
        Type type = new TypeToken<StoreUser>() {}.getType();
        mStoreUser = gson.fromJson(json, type);

        if (mStoreUser == null) {
            mStoreUser = new StoreUser();
        }
    }

    private void saveData() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mStoreUser);
        editor.putString(LoginActivity.STORE_USER, json);
        editor.apply();
    }

    private void getAllItems() {
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection(PRODUCTS)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                String id = document.getId();
                                StoreItems items = document.toObject(StoreItems.class);
                                mHelper.insertItem(items.getName(), id, items.getMgStrength(), items.getPrice(), items.getDescription(), items.getCategory(), items.getPromotion(), items.getQuantity(), items.getImage());
                                mCursor = mHelper.getAllData();
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        mCursor = mHelper.SortNameAscending();
                        mAdapter = new TodoCursorAdapter(getContext(),  mCursor);
                        mGridView.setAdapter(mAdapter);
                    }
                });
    }

    private void populateSpinners() {
        final Spinner filterSpinner = requireView().findViewById(R.id.filter_spinner);
        final Spinner sortSpinner = requireView().findViewById(R.id.sort_spinner);

        String[] filterArray = getResources().getStringArray(R.array.filter_spinner);
        String[] sortArray = getResources().getStringArray(R.array.sort_spinner);

        ArrayAdapter<String> filterAdapter = new ArrayAdapter<>(
                requireContext(),
                android.R.layout.simple_list_item_1,
                filterArray
        );

        ArrayAdapter<String> sortAdapter = new ArrayAdapter<>(
                requireContext(),
                android.R.layout.simple_list_item_1,
                sortArray
        );

        if (filterSpinner != null) {
            filterSpinner.setAdapter(filterAdapter);
        }

        if (sortSpinner != null) {
            sortSpinner.setAdapter(sortAdapter);
        }

        if (filterSpinner != null) {
            filterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i) {
                        case 0:
                            name = true;
                            featured = false;
                            sales = false;
                            newItem = false;
                            if (!price) {
                                mViewHolder.textView.setText(R.string.all_items);
                                mCursor = mHelper.SortNameAscending();
                            } else {
                                mViewHolder.textView.setText(R.string.all_items);
                                mCursor = mHelper.SortNameAscendingPrice();
                            }
                            mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                            mGridView.setAdapter(mAdapter);
                            break;
                        case 1:
                            name = false;
                            featured = true;
                            sales = false;
                            newItem = false;
                            if (!price) {
                                mViewHolder.textView.setText(R.string.featured_items);
                                mCursor = mHelper.SortFeaturedAscending();
                            } else {
                                mViewHolder.textView.setText(R.string.featured_items);
                                mCursor = mHelper.SortFeaturedAscendingPrice();
                            }
                            mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                            mGridView.setAdapter(mAdapter);
                            break;
                        case 2:
                            name = false;
                            featured = false;
                            sales = true;
                            newItem = false;
                            if (!price) {
                                mViewHolder.textView.setText(R.string.sale_items);
                                mCursor = mHelper.SortSaleAscending();
                            } else {
                                mViewHolder.textView.setText(R.string.sale_items);
                                mCursor = mHelper.SortSaleAscendingPrice();
                            }
                            mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                            mGridView.setAdapter(mAdapter);
                            break;
                        case 3:
                            name = false;
                            featured = false;
                            sales = false;
                            newItem = true;
                            if (!price) {
                                mViewHolder.textView.setText(R.string.new_arrivals);
                                mCursor = mHelper.SortNewAscending();
                            } else {
                                mViewHolder.textView.setText(R.string.new_arrivals);
                                mCursor = mHelper.SortNewAscendingPrice();
                            }
                            mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                            mGridView.setAdapter(mAdapter);
                            break;
                        default:
                            mCursor = mHelper.getAllData();
                            mAdapter = new TodoCursorAdapter(getContext(),  mCursor);
                            mGridView.setAdapter(mAdapter);
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    Log.i("Spinners", "Nothing Selected");
                }
            });
        }

        if (sortSpinner != null) {
            sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i) {
                        case 0:
                            price = false;
                            if (name) {
                                mViewHolder.textView.setText(R.string.all_items);
                                mCursor = mHelper.SortNameAscending();
                                mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                                mGridView.setAdapter(mAdapter);
                            } else if (featured) {
                                mViewHolder.textView.setText(R.string.featured_items);
                                mCursor = mHelper.SortFeaturedAscending();
                                mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                                mGridView.setAdapter(mAdapter);
                            } else if (sales) {
                                mViewHolder.textView.setText(R.string.sale_items);
                                mCursor = mHelper.SortSaleAscending();
                                mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                                mGridView.setAdapter(mAdapter);
                            } else if (newItem) {
                                mViewHolder.textView.setText(R.string.new_arrivals);
                                mCursor = mHelper.SortNewAscending();
                                mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                                mGridView.setAdapter(mAdapter);
                            }
                            break;
                        case 1:
                            price = true;
                            if (name) {
                                mViewHolder.textView.setText(R.string.all_items);
                                mCursor = mHelper.SortNameAscendingPrice();
                                mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                                mGridView.setAdapter(mAdapter);
                            } else if (featured) {
                                mViewHolder.textView.setText(R.string.featured_items);
                                mCursor = mHelper.SortFeaturedAscendingPrice();
                                mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                                mGridView.setAdapter(mAdapter);
                            } else if (sales) {
                                mViewHolder.textView.setText(R.string.sale_items);
                                mCursor = mHelper.SortSaleAscendingPrice();
                                mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                                mGridView.setAdapter(mAdapter);
                            } else if (newItem) {
                                mViewHolder.textView.setText(R.string.new_arrivals);
                                mCursor = mHelper.SortNewAscendingPrice();
                                mAdapter = new TodoCursorAdapter(getContext(), mCursor);
                                mGridView.setAdapter(mAdapter);
                            }
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    Log.i("Spinners", "Nothing Selected");
                }
            });
        }
    }

    private class TodoCursorAdapter extends CursorAdapter {
        public TodoCursorAdapter(Context context, Cursor cursor) {
            super(context, cursor, 0);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return LayoutInflater.from(context).inflate(R.layout.item_display_layout, parent, false);
        }

        @Override
        public void bindView(View view, final Context context, Cursor cursor) {
            TextView nameTextView = view.findViewById(R.id.admin_item_name_text);
            TextView mgTextView = view.findViewById(R.id.admin_mg_text);
            TextView priceTextView = view.findViewById(R.id.admin_price_text);
            TextView categoryTextView = view.findViewById(R.id.category_text);
            TextView promotionTextView = view.findViewById(R.id.admin_promo_text);
            final ImageView imageView = view.findViewById(R.id.image_view);
            Button addButton = view.findViewById(R.id.btnAdd);


            final String name = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_NAME));
            final String mg = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_MG));
            final String itemId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_ITEM_ID));
            final String description = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_DESCRIPTION));
            final double price = cursor.getDouble(cursor.getColumnIndex(DataBaseHelper.COLUMN_PRICE));
            final String image = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_IMAGE));
            final String category = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_CATEGORY));
            final String promotion = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_PROMOTION));
            final String quantity = cursor.getString(cursor.getColumnIndex(DataBaseHelper.COLUMN_QUANTITY));

            StorageReference gsReference = mStorage.getReferenceFromUrl(image);

            gsReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).into(imageView);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    exception.printStackTrace();
                }
            });

            categoryTextView.setText(category);
            nameTextView.setText(name);
            mgTextView.setText(String.format("%smg", mg));
            priceTextView.setText(String.format("$%s", price));
            promotionTextView.setText(promotion);
            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                    builder.setTitle(R.string.success);
                    builder.setMessage(R.string.item_added);
                    builder.setNeutralButton(R.string.cancel, null);
                    builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mStoreItems = new StoreItems(name, itemId, description, price, quantity, 1, mg, category, promotion, image);
                            mItems.add(mStoreItems);
                            mStoreUser.setItems(mItems);
                            saveData();
                        }
                    });
                    builder.show();
                }
            });
        }
    }


}

