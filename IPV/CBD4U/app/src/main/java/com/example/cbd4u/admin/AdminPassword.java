package com.example.cbd4u.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cbd4u.R;

public class AdminPassword extends AppCompatActivity {

    EditText password;
    Button btnSubmit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_verification);

        password = findViewById(R.id.password_field);
        btnSubmit = findViewById(R.id.btnPassword);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password.getText().toString().equals("admin")) {
                    Intent intent = new Intent(AdminPassword.this, AdminActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(AdminPassword.this, "Wrong Password", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
