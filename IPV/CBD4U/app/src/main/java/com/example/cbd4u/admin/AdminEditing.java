package com.example.cbd4u.admin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cbd4u.R;
import com.example.cbd4u.objects.StoreItems;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class AdminEditing extends AppCompatActivity {

    private static class ViewHolder {
        TextView name;
        TextView description;
        EditText quantity;
        TextView mgStrength;
        EditText price;
        TextView category;
        Spinner promotion;
        Button btnSave;
    }

    private final ViewHolder mViewHolder = new ViewHolder();

    private StoreItems item;
    private DocumentReference reference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_edit_layout);

        Intent intent = getIntent();
        item = (StoreItems) intent.getSerializableExtra(AdminActivity.ITEM);

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        reference = db.collection("Products").document(item.getItemId());

        populateSpinners();

        mViewHolder.name = findViewById(R.id.name_edit);
        mViewHolder.description = findViewById(R.id.description_edit);
        mViewHolder.quantity = findViewById(R.id.quantity_edit);
        mViewHolder.mgStrength = findViewById(R.id.mgStrength_edit);
        mViewHolder.price = findViewById(R.id.price_edit);
        mViewHolder.category = findViewById(R.id.category_edit);
        mViewHolder.promotion = findViewById(R.id.promotion_spinner);
        mViewHolder.btnSave = findViewById(R.id.btnEditSave);
        mViewHolder.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSave();
            }
        });

        loadView();
    }

    private void loadView() {
        mViewHolder.name.setText(item.getName());
        mViewHolder.description.setText(item.getDescription());
        mViewHolder.quantity.setText(item.getQuantity());
        mViewHolder.mgStrength.setText(item.getMgStrength());
        mViewHolder.price.setText(String.valueOf(item.getPrice()));
        mViewHolder.category.setText(item.getCategory());
    }

    private void btnSave() {
        String quantity = mViewHolder.quantity.getText().toString();
        String price = mViewHolder.price.getText().toString();
        double convertedPrice = Double.parseDouble(price);

        reference.update("quantity", quantity);
        reference.update("price", convertedPrice).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(AdminEditing.this);
                    dialog.setTitle(R.string.success);
                    dialog.setMessage(R.string.item_updated);
                    dialog.setNeutralButton(R.string.cancel, null);
                    dialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(AdminEditing.this, AdminActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    dialog.show();
                } else {
                    Toast.makeText(AdminEditing.this, "Update Failed", Toast.LENGTH_SHORT).show();
                    Log.i("Failed", String.valueOf(task.getException()));
                }
            }
        });
    }

    private void populateSpinners() {
       mViewHolder.promotion = findViewById(R.id.promotion_spinner);

        final String[] filterArray = getResources().getStringArray(R.array.admin_spinner);

        ArrayAdapter<String> partAdapter = new ArrayAdapter<>(
                AdminEditing.this,
                android.R.layout.simple_list_item_1,
                filterArray
        );

        if (mViewHolder.promotion != null) {
            mViewHolder.promotion.setAdapter(partAdapter);
        }

        if (mViewHolder.promotion != null) {
            mViewHolder.promotion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    switch (i) {
                        case 0:
                            reference.update("promotion", filterArray[0]);
                            break;
                        case 1:
                            reference.update("promotion", filterArray[1]);
                            break;
                        case 2:
                            reference.update("promotion", filterArray[2]);
                            break;
                        case 3:
                            reference.update("promotion", filterArray[3]);
                            break;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    Log.i("Spinners", "Nothing Selected");
                }
            });
        }
    }
}
