package com.example.cbd4u.objects;

import java.io.Serializable;
import java.util.ArrayList;

public class StoreUser implements Serializable {

    private String name;
    private String address;
    private String phone;
    private String email;
    private int checkins;
    private boolean age;
    private boolean admin;
    private boolean checkedIn;
    private ArrayList<StoreItems> items;

    public StoreUser(String name, String address, String phone, String email, int checkins, boolean age, boolean admin, boolean checkedIn, ArrayList<StoreItems> items) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.checkins = checkins;
        this.age = age;
        this.admin = admin;
        this.checkedIn = checkedIn;
        this.items = items;
    }

    public StoreUser() {
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public int getCheckins() {
        return checkins;
    }

    public boolean isAge() {
        return age;
    }

    public boolean isAdmin() {
        return admin;
    }

    public boolean isCheckedIn() {
        return checkedIn;
    }

    public ArrayList<StoreItems> getItems() {
        return items;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCheckins(int checkins) {
        this.checkins = checkins;
    }

    public void setAge(boolean age) {
        this.age = age;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public void setCheckedIn(boolean checkedIn) {
        this.checkedIn = checkedIn;
    }

    public void setItems(ArrayList<StoreItems> items) {
        this.items = items;
    }
}
