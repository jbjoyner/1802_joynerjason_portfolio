package com.example.cbd4u.ui.delivery;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.ListFragment;

import com.example.cbd4u.R;
import com.example.cbd4u.adapters.DeliveryAdapter;
import com.example.cbd4u.checkout.CheckoutActivity;
import com.example.cbd4u.login.LoginActivity;
import com.example.cbd4u.objects.StoreItems;
import com.example.cbd4u.objects.StoreUser;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class DeliveryFragment extends ListFragment {

    private StoreUser mStoreUser;
    private ArrayList<StoreItems> mStoreItems;
    private FirebaseStorage mStorage;
    private DeliveryAdapter adapter;
    private ArrayList<StoreItems> mItems;

    private static class ViewHolder {
        TextView nameText;
        TextView mgText;
        TextView descriptionText;
        TextView categoryText;
        TextView promotionText;
        TextView quantityText;
        TextView priceText;
        TextView title;
        Button checkout;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadStoreUSer();
        if (mStoreUser.getItems() == null) {
            mItems = new ArrayList<>();
        } else {
            mItems = mStoreUser.getItems();
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_delivery, container, false);
        mViewHolder.title = root.findViewById(R.id.delivery_text);
        mViewHolder.title.setText(R.string.items_for_delivery);
        mViewHolder.checkout = root.findViewById(R.id.btnEnter);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mStorage = FirebaseStorage.getInstance();

        if (mStoreUser.getItems() != null) {
            if (mStoreUser.getItems().isEmpty()) {
                mViewHolder.checkout.setVisibility(View.INVISIBLE);
            }
        }
        if (mStoreUser.getItems() == null) {
            mViewHolder.checkout.setVisibility(View.INVISIBLE);
        }

        adapter = new DeliveryAdapter(mStoreUser.getItems(), getContext());
        setListAdapter(adapter);

        mViewHolder.checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mStoreUser.getItems() != null) {
                    Intent intent = new Intent(getContext(), CheckoutActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, final int position, long id) {
        super.onListItemClick(l, v, position, id);

        mStoreItems = mStoreUser.getItems();
        final String name = mStoreItems.get(position).getName();
        final String mg = mStoreItems.get(position).getMgStrength();
        final double price = mStoreItems.get(position).getPrice();
        final String description = mStoreItems.get(position).getDescription();
        final String category = mStoreItems.get(position).getCategory();
        final String promotion = mStoreItems.get(position).getPromotion();
        final String image = mStoreItems.get(position).getImage();
        final String  priceConverted = String.valueOf(price);
        final int quantityOrdered = mStoreItems.get(position).getQuantityOrdered();

        final AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());

        builder.setView(R.layout.details_layout);

        final View customLayout = getLayoutInflater().inflate(R.layout.details_layout, null);
        builder.setView(customLayout);
        mViewHolder.nameText = customLayout.findViewById(R.id.name_details_text);
        mViewHolder.mgText = customLayout.findViewById(R.id.mg_details_text);
        mViewHolder.descriptionText = customLayout.findViewById(R.id.description_details_text);
        mViewHolder.categoryText = customLayout.findViewById(R.id.category_details_text);
        mViewHolder.promotionText = customLayout.findViewById(R.id.promotion_details_text);
        mViewHolder.quantityText = customLayout.findViewById(R.id.quantity_details_text);
        mViewHolder.priceText = customLayout.findViewById(R.id.price_details_text);
        final ImageView detailsView = customLayout.findViewById(R.id.details_image);

        StorageReference gsReference = mStorage.getReferenceFromUrl(image);

        gsReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(detailsView);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                exception.printStackTrace();
            }
        });

        mViewHolder.nameText.setText(name);
        mViewHolder.mgText.setText(String.format("%smg", mg));
        mViewHolder.priceText.setText(priceConverted);
        mViewHolder.descriptionText.setText(description);
        mViewHolder.categoryText.setText(category);
        mViewHolder.promotionText.setText(promotion);
        mViewHolder.quantityText.setText(String.valueOf(quantityOrdered));

        builder.setPositiveButton(getString(R.string.close), null);
        builder.setNeutralButton("Delete Item", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AlertDialog.Builder delete = new AlertDialog.Builder(requireContext());
                delete.setTitle(R.string.delete_sure);
                delete.setMessage(R.string.delete_you_sure);
                delete.setNeutralButton(R.string.close, null);
                delete.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mStoreItems.remove(position);
                        mStoreUser.setItems(mItems);
                        saveData();
                        adapter.notifyDataSetChanged();
                        if (mItems.isEmpty()) {
                            mViewHolder.checkout.setVisibility(View.INVISIBLE);
                        }
                    }
                });
                delete.show();
            }
        });

        builder.setNegativeButton("Change Quantity", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                final AlertDialog.Builder quantity = new AlertDialog.Builder(requireContext());

                quantity.setView(R.layout.change_quantity_layout);

                final View customLayout = getLayoutInflater().inflate(R.layout.change_quantity_layout, null);
                quantity.setView(customLayout);
                final EditText quantityAmount = customLayout.findViewById(R.id.admin_quantity_text);
                quantity.setPositiveButton(R.string.close, null);
                quantity.setNegativeButton(R.string.btnSave, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String quantityOrdered = quantityAmount.getText().toString();
                        int quant = Integer.parseInt(quantityOrdered);
                        mStoreItems.get(position).setQuantityOrdered(quant);
                        mStoreUser.setItems(mItems);
                        saveData();
                        adapter.notifyDataSetChanged();
                    }
                });
                quantity.show();
            }
        });
        builder.show();
    }

    private void loadStoreUSer() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(LoginActivity.STORE_USER, null);
        Type type = new TypeToken<StoreUser>() {}.getType();
        mStoreUser = gson.fromJson(json, type);

        if (mStoreUser == null) {
            mStoreUser = new StoreUser();
        }
    }

    private void saveData() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mStoreUser);
        editor.putString(LoginActivity.STORE_USER, json);
        editor.apply();
    }
}
