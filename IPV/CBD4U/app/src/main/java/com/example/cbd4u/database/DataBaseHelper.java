package com.example.cbd4u.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_FILE = "database.db";
    private static final int DATABASE_VERSION = 1;
    private static final String FEATURED = "Featured Item";
    private static final String NEW = "New Arrival";
    private static final String SALE = "Sale Item";

    private static final String TABLE_NAME = "products";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_ITEM_ID = "_Itemid";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_MG = "mg";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_CATEGORY = "category";
    public static final String COLUMN_PROMOTION = "promotion";
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_QUANTITY = "quantity";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_ITEM_ID + " TEXT, " +
            COLUMN_NAME + " TEXT, " +
            COLUMN_MG + " TEXT, " +
            COLUMN_PRICE + " REAL, " +
            COLUMN_DESCRIPTION + " TEXT, " +
            COLUMN_CATEGORY + " TEXT, " +
            COLUMN_PROMOTION + " TEXT, " +
            COLUMN_QUANTITY + " TEXT, " +
            COLUMN_IMAGE + " TEXT)";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    private static DataBaseHelper INSTANCE = null;

    public static  DataBaseHelper getINSTANCE (Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DataBaseHelper(context);
        }
        return INSTANCE;
    }

    private final SQLiteDatabase mDatabase;

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    private DataBaseHelper(Context context) {
        super(context, DATABASE_FILE, null,
                DATABASE_VERSION);

        mDatabase = getWritableDatabase();
    }

    public void insertItem(String name, String itemId, String mg, double price, String description, String category, String promotion, String quantity, String image) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, name);
        contentValues.put(COLUMN_ITEM_ID, itemId);
        contentValues.put(COLUMN_MG, mg);
        contentValues.put(COLUMN_PRICE, price);
        contentValues.put(COLUMN_DESCRIPTION, description);
        contentValues.put(COLUMN_CATEGORY, category);
        contentValues.put(COLUMN_PROMOTION, promotion);
        contentValues.put(COLUMN_IMAGE, image);
        contentValues.put(COLUMN_QUANTITY, quantity);

        mDatabase.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getAllData() {
        return mDatabase.query(TABLE_NAME, null, null, null, null, null, null);
    }

    public Cursor SortNameAscending() {
        String orderBy = COLUMN_NAME + " ASC";

        return mDatabase.query(TABLE_NAME, null, null, null, null, null, orderBy);
    }

    public Cursor SortFeaturedAscending() {
        String orderBy = COLUMN_NAME + " ASC";
        String selection = COLUMN_PROMOTION + " LIKE ?";
        String[] args = {FEATURED};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public Cursor SortSaleAscending() {
        String orderBy = COLUMN_NAME + " ASC";
        String selection = COLUMN_PROMOTION + " LIKE ?";
        String[] args = {SALE};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public Cursor SortNewAscending() {
        String orderBy = COLUMN_NAME + " ASC";
        String selection = COLUMN_PROMOTION + " LIKE ?";
        String[] args = {NEW};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public Cursor SortNameAscendingPrice() {
        String orderBy = COLUMN_PRICE + " ASC";

        return mDatabase.query(TABLE_NAME, null, null, null, null, null, orderBy);
    }

    public Cursor SortFeaturedAscendingPrice() {
        String orderBy = COLUMN_PRICE + " ASC";
        String selection = COLUMN_PROMOTION + " LIKE ?";
        String[] args = {FEATURED};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public Cursor SortSaleAscendingPrice() {
        String orderBy = COLUMN_PRICE + " ASC";
        String selection = COLUMN_PROMOTION + " LIKE ?";
        String[] args = {SALE};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public Cursor SortNewAscendingPrice() {
        String orderBy = COLUMN_PRICE + " ASC";
        String selection = COLUMN_PROMOTION + " LIKE ?";
        String[] args = {NEW};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public void DeleteAllItems() {
        mDatabase.delete(TABLE_NAME, null, null);
    }
}
