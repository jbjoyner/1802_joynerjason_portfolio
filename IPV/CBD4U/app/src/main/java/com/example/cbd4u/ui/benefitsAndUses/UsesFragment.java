package com.example.cbd4u.ui.benefitsAndUses;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cbd4u.R;

public class UsesFragment extends Fragment {

    private static class ViewHolder {
        TextView uses;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_uses, container, false);
        mViewHolder.uses = root.findViewById(R.id.uses);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewHolder.uses.setText(R.string.uses);
    }
}
