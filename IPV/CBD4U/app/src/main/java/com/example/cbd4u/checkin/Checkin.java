package com.example.cbd4u.checkin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cbd4u.R;
import com.example.cbd4u.login.LoginActivity;
import com.example.cbd4u.objects.StoreUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class Checkin extends AppCompatActivity {

    private int mCheckinCount;
    private StoreUser mStoreUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadStoreUSer();

        if (!mStoreUser.isCheckedIn()) {
            checkIn();
        }
    }

    public void checkIn() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Checkin.this);

        builder.setView(R.layout.checkin_screen);

        final View customLayout = getLayoutInflater().inflate(R.layout.checkin_screen, null);
        builder.setView(customLayout);
        builder.setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNeutralButton(getString(R.string.check_in), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mCheckinCount = mStoreUser.getCheckins();
                mCheckinCount++;
                Toast.makeText(Checkin.this, R.string.thank_for_checking_in, Toast.LENGTH_SHORT).show();
                mStoreUser.setCheckins(mCheckinCount);
                mStoreUser.setCheckedIn(true);
                saveData();
                finish();
            }
        });
        builder.show();
    }

    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(mStoreUser);
        editor.putString(LoginActivity.STORE_USER, json);
        editor.apply();
    }

    private void loadStoreUSer() {
        SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(LoginActivity.STORE_USER, null);
        Type type = new TypeToken<StoreUser>() {}.getType();
        mStoreUser = gson.fromJson(json, type);

        if (mStoreUser == null) {
            mStoreUser = new StoreUser();
        }
    }
}
