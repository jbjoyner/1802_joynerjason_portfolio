package com.example.cbd4u.ui.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cbd4u.profile.EditProfileActivity;
import com.example.cbd4u.R;
import com.example.cbd4u.login.LoginActivity;
import com.example.cbd4u.objects.StoreItems;
import com.example.cbd4u.objects.StoreUser;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

public class ProfileFragment extends Fragment {

    private static class ViewHolder {
        TextView banner;
        TextView nameText;
        TextView addressText;
        TextView phoneText;
        TextView emailText;
        TextView checkin;
        TextView items;
        Button btnEdit;
        Button btnSignout;
    }
    private final ViewHolder mViewHolder = new ViewHolder();
    private StoreUser mStoreUser;

    public interface onSignOutClickListener {
        void onSignOutClick();
    }
    private onSignOutClickListener mListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof onSignOutClickListener) {
            mListener = (onSignOutClickListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        mViewHolder.banner = root.findViewById(R.id.profile_banner);
        mViewHolder.banner.setText(R.string.update_profile);
        loadStoreUSer();
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadViews();

        Places.initialize(requireContext(), "AIzaSyC7QtcfDjxsPVXp3KQKaP9ew4wiAmxsyLk");

        mViewHolder.addressText.setFocusable(false);
        mViewHolder.addressText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Place.Field> fieldList = Arrays.asList(Place.Field.ADDRESS,
                        Place.Field.ID,
                        Place.Field.NAME);

                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,
                        fieldList)
                        .build(requireContext());
                startActivityForResult(intent, 100);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        getActivity();
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            Place place = null;
            if (data != null) {
                place = Autocomplete.getPlaceFromIntent(data);
            }

            if (place != null) {
                mViewHolder.addressText.setText(place.getAddress());
            }
        } else if (requestCode == AutocompleteActivity.RESULT_ERROR) {
            Status status = null;
            if (data != null) {
                status = Autocomplete.getStatusFromIntent(data);
            }
            if (status != null) {
                Toast.makeText(requireContext(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loadStoreUSer() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(LoginActivity.STORE_USER, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(LoginActivity.STORE_USER, null);
        Type type = new TypeToken<StoreUser>() {}.getType();
        mStoreUser = gson.fromJson(json, type);

        if (mStoreUser == null) {
            mStoreUser = new StoreUser();
        }
    }

    private void loadViews() {
        loadStoreUSer();

        View view = getView();
        if (view != null) {
            mViewHolder.nameText = view.findViewById(R.id.create_name);
            mViewHolder.addressText = view.findViewById(R.id.create_address);
            mViewHolder.phoneText = view.findViewById(R.id.create_phone);
            mViewHolder.emailText = view.findViewById(R.id.create_email);
            mViewHolder.checkin = view.findViewById(R.id.profile_checkin);
            mViewHolder.items = view.findViewById(R.id.profile_items);
            mViewHolder.btnEdit = view.findViewById(R.id.btnCreateSave);
            mViewHolder.btnSignout = view.findViewById(R.id.btnSignout);
        }

        mViewHolder.nameText.setText(mStoreUser.getName());
        mViewHolder.addressText.setText(mStoreUser.getAddress());
        mViewHolder.phoneText.setText(mStoreUser.getPhone());
        mViewHolder.emailText.setText(mStoreUser.getEmail());
        mViewHolder.checkin.setText(String.valueOf(mStoreUser.getCheckins()));
        int quantity = 0;
        if (mStoreUser.getItems() != null) {
            for (StoreItems items: mStoreUser.getItems()
            ) {
                quantity += items.getQuantityOrdered();
            }
            mViewHolder.items.setText(String.valueOf(quantity));
        } else {
            mViewHolder.items.setText("0");
        }

        mViewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editButtonClick();
            }
        });
        mViewHolder.btnSignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSignOutClick();
            }
        });
    }

    private void editButtonClick() {
        Intent intent = new Intent(requireContext(), EditProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadStoreUSer();
        loadViews();
    }
}

