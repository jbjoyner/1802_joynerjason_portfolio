package com.example.cbd4u.objects;

import java.io.Serializable;

public class StoreItems implements Serializable {

    private String name;
    private String itemId;
    private String description;
    private double price;
    private String quantity;
    private int quantityOrdered;
    private String mgStrength;
    private String category;
    private String promotion;
    private String image;

    public StoreItems(String name, String itemId, String description, double price, String quantity, int quantityOrdered, String mgStrength, String category, String promotion, String image) {
        this.name = name;
        this.itemId = itemId;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.quantityOrdered = quantityOrdered;
        this.mgStrength = mgStrength;
        this.category = category;
        this.promotion = promotion;
        this.image = image;
    }

    public StoreItems() {
    }

    public String getName() {
        return name;
    }

    public String getItemId() {
        return itemId;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() { return description; }

    public String getQuantity() { return quantity; }

    public int getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(int quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public String  getMgStrength() { return mgStrength; }

    public String getCategory() {
        return category;
    }

    public String getPromotion() {
        return promotion;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setMgStrength(String mgStrength) {
        this.mgStrength = mgStrength;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
