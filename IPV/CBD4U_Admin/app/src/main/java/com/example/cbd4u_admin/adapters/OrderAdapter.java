package com.example.cbd4u_admin.adapters;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;


import com.example.cbd4u_admin.R;
import com.example.cbd4u_admin.objects.StoreItems;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OrderAdapter extends BaseAdapter {

    private static class ViewHolder {
        TextView name;
        TextView quantity;
        TextView mg;
        TextView price;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    private static final String TAG = "";

    private final ArrayList<StoreItems> mItems;
    private final Context mContext;

    public OrderAdapter(ArrayList<StoreItems> mItems, Context mContext) {
        this.mItems = mItems;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {

        if (mItems != null) {
            return  mItems.size();
        } else {
            Log.i(TAG, "getCount: There is no collection" );
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {

        if (mItems != null && position >= 0 && position < mItems.size()) {
            return mItems.get(position);
        } else {
            Log.i(TAG, "getItem: There was a problem getting an item.");
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
            return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.delivery_layout, parent, false);
        }

        FirebaseStorage storage = FirebaseStorage.getInstance();

        StoreItems item = (StoreItems) getItem(position);

        mViewHolder.name = convertView.findViewById(R.id.delivery_name);
        mViewHolder.quantity = convertView.findViewById(R.id.delivery_quantity);
        mViewHolder.mg = convertView.findViewById(R.id.delivery_mg);
        mViewHolder.price = convertView.findViewById(R.id.delivery_price);
        final ImageView imageView = convertView.findViewById(R.id.delivery_image);

        mViewHolder.name.setText(item.getName());
//        mViewHolder.quantity.setText(String.format("Quantity %s", item.getQuantityOrdered()));
        mViewHolder.price.setText(String.format("$%s", item.getPrice()));
        mViewHolder.mg.setText(String.format("%smg", item.getMgStrength()));

        StorageReference gsReference = storage.getReferenceFromUrl(item.getImage());
        gsReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).memoryPolicy(MemoryPolicy.NO_CACHE).into(imageView);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                exception.printStackTrace();
            }
        });
        return convertView;
    }
}
