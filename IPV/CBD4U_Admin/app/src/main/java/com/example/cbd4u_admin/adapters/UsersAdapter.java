package com.example.cbd4u_admin.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.cbd4u_admin.R;
import com.example.cbd4u_admin.objects.StoreUser;

import java.util.ArrayList;

public class UsersAdapter extends BaseAdapter {

    private static class ViewHolder {
        TextView name;
        TextView checkin;
        TextView phone;
        TextView email;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    private static final String TAG = "";

    private final ArrayList<StoreUser> mUsers;
    private final Context mContext;

    public UsersAdapter(ArrayList<StoreUser> mUsers, Context mContext) {
        this.mUsers = mUsers;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {

        if (mUsers != null) {
            return  mUsers.size();
        } else {
            Log.i(TAG, "getCount: There is no collection" );
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {

        if (mUsers != null && position >= 0 && position < mUsers.size()) {
            return mUsers.get(position);
        } else {
            Log.i(TAG, "getItem: There was a problem getting an item.");
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
            return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.user_listview_layout, parent, false);
        }

        StoreUser user = (StoreUser) getItem(position);

        mViewHolder.name = convertView.findViewById(R.id.userNameTxt);
        mViewHolder.checkin = convertView.findViewById(R.id.userCheckinTxt);
        mViewHolder.phone = convertView.findViewById(R.id.userPhoneTxt);
        mViewHolder.email = convertView.findViewById(R.id.userEmailText);

        mViewHolder.name.setText(user.getName());
        mViewHolder.checkin.setText(user.getCheckins());
        mViewHolder.email.setText(user.getEmail());
        mViewHolder.phone.setText(user.getPhone());

        return convertView;
    }
}
