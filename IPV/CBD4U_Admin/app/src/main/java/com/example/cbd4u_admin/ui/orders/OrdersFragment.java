package com.example.cbd4u_admin.ui.orders;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.cbd4u_admin.R;
import com.example.cbd4u_admin.adapters.OrderAdapter;
import com.example.cbd4u_admin.objects.Checkout;
import com.example.cbd4u_admin.objects.StoreItems;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class OrdersFragment extends Fragment {

    private static final String ORDER_TO_SEND = "Order to Send";

    private Checkout checkout;

    private static class ViewHolder {
        TextView nameText;
        TextView addressText;
        TextView phoneText;
        TextView emailText;
        TextView numItems;
        TextView checkins;
        TextView totalText;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_orders, container, false);

        mViewHolder.nameText = root.findViewById(R.id.nameTxt);
        mViewHolder.addressText = root.findViewById(R.id.addTxt);
        mViewHolder.phoneText = root.findViewById(R.id.phTxt);
        mViewHolder.emailText = root.findViewById(R.id.emailTxt);
        mViewHolder.numItems = root.findViewById(R.id.noTxt);
        mViewHolder.checkins = root.findViewById(R.id.checkins_textview);
        mViewHolder.totalText = root.findViewById(R.id.totTxt);
        mViewHolder.numItems.setText(String.valueOf(checkout.getQuantity()));
        mViewHolder.nameText.setText(checkout.getName());
        mViewHolder.addressText.setText(checkout.getAddress());
        mViewHolder.phoneText.setText(checkout.getPhone());
        mViewHolder.checkins.setText(String.valueOf(checkout.getCheckins()));
        mViewHolder.emailText.setText(checkout.getEmail());
        String COUNTRY = "US";
        String LANGUAGE = "en";
        String totalFormatted = NumberFormat.getCurrencyInstance(new Locale(LANGUAGE, COUNTRY)).format(checkout.getTotal());
        mViewHolder.totalText.setText(totalFormatted);

        ListView view = root.findViewById(R.id.orderListView);
        ArrayList<StoreItems> items = checkout.getItems();
        OrderAdapter adapter = new OrderAdapter(items, requireContext());
        view.setAdapter(adapter);

        return root;
    }
}