package com.example.cbd4u_admin.objects;

import java.io.Serializable;
import java.util.ArrayList;

public class Checkout implements Serializable {

    private final String name;
    private final String address;
    private final String phone;
    private final String email;
    private final int quantity;
    private final float subtotal;
    private final float fee;
    private final int checkins;
    private final float total;
    private final ArrayList<StoreItems> items;

    public Checkout(String name, String address, String phone, String email, int quantity, float subtotal, float fee, int checkins, float total, ArrayList<StoreItems> items) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.quantity = quantity;
        this.subtotal = subtotal;
        this.fee = fee;
        this.checkins = checkins;
        this.total = total;
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public int getQuantity() {
        return quantity;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public float getFee() {
        return fee;
    }

    public int getCheckins() {
        return checkins;
    }

    public float getTotal() {
        return total;
    }

    public ArrayList<StoreItems> getItems() {
        return items;
    }
}
