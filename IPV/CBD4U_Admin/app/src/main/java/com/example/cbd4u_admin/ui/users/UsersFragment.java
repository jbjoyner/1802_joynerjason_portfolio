package com.example.cbd4u_admin.ui.users;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.cbd4u_admin.R;
import com.example.cbd4u_admin.database.DataBaseHelper;
import com.example.cbd4u_admin.objects.StoreItems;
import com.example.cbd4u_admin.objects.StoreUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Objects;

public class UsersFragment extends Fragment {

    private static final String TAG = "UsersFragment";
    private ListView lv;
    private static final String USERS = "Users";
    private DataBaseHelper mHelper;
    private Cursor mCursor;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHelper = DataBaseHelper.getINSTANCE(getContext());
        mHelper.DeleteAllItems();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_users, container, false);
        lv = root.findViewById(R.id.userListView);
        lv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(requireContext(), "Test", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return root;
    }

    private void getAllUsers() {
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection(USERS)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                String id = document.getId();
                                StoreUser user = document.toObject(StoreUser.class);
//                                mHelper.insertItem(user.getName(), user.getAddress(), user.getPhone(), user.getEmail(), user.getCheckins(), user.isAge());
                                mCursor = mHelper.getAllData();
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                        mCursor = mHelper.SortNameAscending();
//                        mAdapter = new TodoCursorAdapter(getContext(),  mCursor);
//                        mGridView.setAdapter(mAdapter);
                    }
                });
    }

}