package com.example.cbd4u_admin.ui.products;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.cbd4u_admin.R;
import com.example.cbd4u_admin.objects.StoreItems;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

public class ProductsFragment extends Fragment {

    public static final String STORE_ITEMS = "STORE_ITEMS";
    private static final String TAG = "HomeFragment";
    private String name;
    private String itemId;
    private String description;
    private double price;
    private int quantity;
    private int quantityOrdered;
    private String mgStrength;
    private String category;
    private String promotion;
    private String image;
    private StoreItems items;

    private static class ViewHolder {
        EditText nameText;
        EditText mgText;
        EditText descriptionText;
        Spinner categoryText;
        Spinner promotionText;
        EditText priceText;
        EditText imageText;
        EditText quantityText;
        Button btnSubmit;
    }
    private final ViewHolder mViewHolder = new ViewHolder();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_products, container, false);
        mViewHolder.nameText = root.findViewById(R.id.txtName);
        mViewHolder.categoryText = root.findViewById(R.id.categorySpinner);
        mViewHolder.promotionText = root.findViewById(R.id.promotionSpinner);
        mViewHolder.descriptionText = root.findViewById(R.id.txtDescription);
        mViewHolder.priceText = root.findViewById(R.id.txtPrice);
        mViewHolder.mgText = root.findViewById(R.id.txtMgStrength);
        mViewHolder.quantityText = root.findViewById(R.id.txtQuantity);
        mViewHolder.imageText = root.findViewById(R.id.txtImage);
        mViewHolder.btnSubmit = root.findViewById(R.id.btnSubmit);
        mViewHolder.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertData();
             }
        });

        return root;
    }

    private void insertData() {

        if (mViewHolder.nameText.getText().toString().isEmpty()
                | mViewHolder.imageText.getText().toString().isEmpty()
                | mViewHolder.quantityText.getText().toString().isEmpty()
                | mViewHolder.mgText.getText().toString().isEmpty()
                | mViewHolder.priceText.getText().toString().isEmpty()
                | mViewHolder.descriptionText.getText().toString().isEmpty()
                | mViewHolder.imageText.getText().toString().isEmpty()) {
            Toast.makeText(requireContext(), "One or more fields is blank. Please complete the entry and try again.", Toast.LENGTH_SHORT).show();

        } else {
            name = mViewHolder.nameText.getText().toString();
//            category = mViewHolder.categoryText.getText().toString();
            description = mViewHolder.descriptionText.getText().toString();
            String priceTxt = mViewHolder.priceText.getText().toString();
            String quantityTxt = mViewHolder.quantityText.getText().toString();
//            promotion = mViewHolder.promotionText.getText().toString();
            mgStrength = mViewHolder.mgText.getText().toString();
            image = mViewHolder.imageText.getText().toString();
            price = Double.parseDouble(priceTxt);
            quantity = Integer.parseInt(quantityTxt);
        }

        // Access a Cloud Firestore instance from your Activity
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // Create a new user with a first and last name
        items = new StoreItems();
        items.setCategory(category);
        items.setDescription(description);
        items.setImage(image);
        items.setMgStrength(mgStrength);
        items.setName(name);
        items.setPrice(price);
        items.setPromotion(promotion);
        items.setQuantity(quantity);

        // Add a new document with a generated ID
        db.collection("new items")
                .add(items)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                        items.setItemId(documentReference.getId());
                        Toast.makeText(requireContext(), "Item Saved Successfully", Toast.LENGTH_SHORT).show();
                        saveData();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                        Toast.makeText(requireContext(), "Item Saved Failure", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void saveData() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences(ProductsFragment.STORE_ITEMS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(items);
        editor.putString(ProductsFragment.STORE_ITEMS, json);
        editor.apply();
    }
}