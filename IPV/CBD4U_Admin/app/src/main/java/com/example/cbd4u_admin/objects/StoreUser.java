package com.example.cbd4u_admin.objects;

import java.io.Serializable;
import java.util.ArrayList;

public class StoreUser implements Serializable {

    private String name;
    private String address;
    private String phone;
    private String email;
    private int checkins;
    private boolean age;
    private boolean admin;
    private boolean checkedIn;
    private int quantityOrdered;
    private ArrayList<StoreItems> items;

    public StoreUser() {
    }

    public StoreUser(String name, String address, String phone, String email, int checkins, boolean age, boolean admin, boolean checkedIn, int quantityOrdered, ArrayList<StoreItems> items) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.checkins = checkins;
        this.age = age;
        this.admin = admin;
        this.checkedIn = checkedIn;
        this.quantityOrdered = quantityOrdered;
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCheckins() {
        return checkins;
    }

    public void setCheckins(int checkins) {
        this.checkins = checkins;
    }

    public boolean isAge() {
        return age;
    }

    public void setAge(boolean age) {
        this.age = age;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isCheckedIn() {
        return checkedIn;
    }

    public void setCheckedIn(boolean checkedIn) {
        this.checkedIn = checkedIn;
    }

    public int getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(int quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public ArrayList<StoreItems> getItems() {
        return items;
    }

    public void setItems(ArrayList<StoreItems> items) {
        this.items = items;
    }
}
