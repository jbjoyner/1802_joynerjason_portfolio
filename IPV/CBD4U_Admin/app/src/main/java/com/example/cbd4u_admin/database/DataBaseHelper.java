package com.example.cbd4u_admin.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_FILE = "database.db";
    private static final int DATABASE_VERSION = 1;
    private static final String FEATURED = "Featured Item";
    private static final String NEW = "New Arrival";
    private static final String SALE = "Sale Item";

    private static final String TABLE_NAME = "products";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_PHONR = "phone";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_CHECKINS = "checkins";
    public static final String COLUMN_AGE = "age";

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " +
            TABLE_NAME + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_NAME + " TEXT, " +
            COLUMN_ADDRESS + " TEXT, " +
            COLUMN_PHONR + " REAL, " +
            COLUMN_EMAIL + " TEXT, " +
            COLUMN_CHECKINS + " TEXT, " +
            COLUMN_AGE + " TEXT)";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    private static DataBaseHelper INSTANCE = null;

    public static  DataBaseHelper getINSTANCE (Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DataBaseHelper(context);
        }
        return INSTANCE;
    }

    private final SQLiteDatabase mDatabase;

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    private DataBaseHelper(Context context) {
        super(context, DATABASE_FILE, null,
                DATABASE_VERSION);

        mDatabase = getWritableDatabase();
    }

    public void insertItem(String name, String address, String phone, String email, String checkins, String age) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, name);
        contentValues.put(COLUMN_ADDRESS, address);
        contentValues.put(COLUMN_PHONR, phone);
        contentValues.put(COLUMN_EMAIL, email);
        contentValues.put(COLUMN_CHECKINS, checkins);
        contentValues.put(COLUMN_AGE, age);

        mDatabase.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getAllData() {
        return mDatabase.query(TABLE_NAME, null, null, null, null, null, null);
    }

    public Cursor SortNameAscending() {
        String orderBy = COLUMN_NAME + " ASC";

        return mDatabase.query(TABLE_NAME, null, null, null, null, null, orderBy);
    }

    public Cursor SortFeaturedAscending() {
        String orderBy = COLUMN_NAME + " ASC";
        String selection = COLUMN_AGE + " LIKE ?";
        String[] args = {FEATURED};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public Cursor SortSaleAscending() {
        String orderBy = COLUMN_NAME + " ASC";
        String selection = COLUMN_AGE + " LIKE ?";
        String[] args = {SALE};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public Cursor SortNewAscending() {
        String orderBy = COLUMN_NAME + " ASC";
        String selection = COLUMN_AGE + " LIKE ?";
        String[] args = {NEW};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public Cursor SortNameAscendingPrice() {
        String orderBy = COLUMN_PHONR + " ASC";

        return mDatabase.query(TABLE_NAME, null, null, null, null, null, orderBy);
    }

    public Cursor SortFeaturedAscendingPrice() {
        String orderBy = COLUMN_PHONR + " ASC";
        String selection = COLUMN_AGE + " LIKE ?";
        String[] args = {FEATURED};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public Cursor SortSaleAscendingPrice() {
        String orderBy = COLUMN_PHONR + " ASC";
        String selection = COLUMN_AGE + " LIKE ?";
        String[] args = {SALE};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public Cursor SortNewAscendingPrice() {
        String orderBy = COLUMN_PHONR + " ASC";
        String selection = COLUMN_AGE + " LIKE ?";
        String[] args = {NEW};

        return mDatabase.query(TABLE_NAME, null, selection, args, null, null, orderBy);
    }

    public void DeleteAllItems() {
        mDatabase.delete(TABLE_NAME, null, null);
    }
}
