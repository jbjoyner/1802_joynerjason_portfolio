# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.23)
# Database: ContactList
# Generation Time: 2018-12-01 18:15:00 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table MyContacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `MyContacts`;

CREATE TABLE `MyContacts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(100) NOT NULL DEFAULT '',
  `LastName` varchar(100) NOT NULL DEFAULT '',
  `PhoneNumber` decimal(65,0) NOT NULL,
  `Email` varchar(100) NOT NULL DEFAULT '',
  `Relation` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `MyContacts` WRITE;
/*!40000 ALTER TABLE `MyContacts` DISABLE KEYS */;

INSERT INTO `MyContacts` (`id`, `FirstName`, `LastName`, `PhoneNumber`, `Email`, `Relation`)
VALUES
	(1,'Emily','Smith',3696286082,'ESmith@gmail.com','Friend'),
	(2,'Hannah','Johnson',4627540798,'HJohnson@gmail.com','Family'),
	(3,'Sarah','Williams',5196134343,'SWilliams@gmail.com','Other'),
	(4,'Ashley','Jones',7791898960,'AJones@gmail.com','Business'),
	(5,'Taylor','Brown',8440677079,'TBrown@gmail.com','Family'),
	(6,'Jessica','Davis',7771151049,'JDavis@gmail.com','Friend'),
	(7,'Michael','Miller',9127631111,'MMiller@gmail.com','Friend'),
	(8,'Jacob','Wilson',6145571941,'JWilson@gmail.com','Other'),
	(9,'Alex','Moore',2493215889,'AMoore@gmail.com','Family'),
	(10,'Tyler','Taylor',3085984611,'TTaylor@gmail.com','Business'),
	(11,'Andrew','Anderson',6369979873,'aAnderson@gmail.com','Family'),
	(12,'Daniel','Thomas',8095734428,'DThomas@gmail.com','Other'),
	(13,'Matthew','Jackson',8722806073,'MJackson@gmail.com','Friend'),
	(14,'Emma','White',2957356164,'EWhite@gmail.com','Business'),
	(15,'John','Harris',6983335881,'JHarris@gmail.com','Other');

/*!40000 ALTER TABLE `MyContacts` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
