﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodeAssignment2
{
    public class Contact
    {
        string firstName;
        string lastName;
        string phoneNumber;
        string email;
        string relation;
        int imageIndex;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string Email { get => email; set => email = value; }
        public string Relation { get => relation; set => relation = value; }
        public int ImageIndex { get => imageIndex; set => imageIndex = value; }

        public override string ToString()
        {
            return FirstName + ", " + LastName;
        }

        public Contact()
        {

        }

        public Contact(string str) : this()
        {
            //Array to hold the string once split
            string[] split = str.Split(',');

            //Place the values back in their fields
            FirstName = split[0];
            LastName = split[1];
            PhoneNumber = split[2];
            Email = split[3];
            Relation = split[4];
            int tmpImageIndex = 0;
            if (int.TryParse(split[5], out tmpImageIndex))
            {
                ImageIndex = tmpImageIndex;
            }
        }

        public string GetStringToSave()
        {
            string returnValue = "";

            returnValue = FirstName + "," + LastName + "," + PhoneNumber + "," + Email + "," + Relation + "," + ImageIndex;

            return returnValue;
        }
    }
}
