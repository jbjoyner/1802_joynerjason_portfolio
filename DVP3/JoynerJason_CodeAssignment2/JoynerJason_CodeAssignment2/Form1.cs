﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using MySql.Data.MySqlClient;

namespace JoynerJason_CodeAssignment2
{
    public partial class ContactList : Form
    {
        public ContactList()
        {
            InitializeComponent();

            //Variable to hold the string returned for the buildconnectionstring method
            string connectionString = BuildConnectionString();

            //Method to iniate the connection
            Connect(connectionString);

            //Create the subscription for our custom event args
            ModifyObject += HandleModifyObject;

            //Method to get the data from the database
            RetrieveData();

            //Call the handle size method
            HandleClientWindowSize();
        }

        //Written by Keith Webster.  Used with permission.  Not to be distributed.  
        //Place this inside the class space in the form you would like to size.
        //Call this method AFTER InitializeComponent() inside the form's constructor.
        void HandleClientWindowSize()
        {
            //Modify ONLY these float values
            float HeightValueToChange = 2.0f;
            float WidthValueToChange = 6.0f;

            //DO NOT MODIFY THIS CODE
            int height = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Height / HeightValueToChange);
            int width = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Width / WidthValueToChange);
            if (height < Size.Height)
                height = Size.Height;
            if (width < Size.Width)
                width = Size.Width;
            this.Size = new Size(width, height);
        }

        public Contact GetContact
        {
            //Getter
            get
            {
                //Instantiate a new object and assign the values
                Contact contact = new Contact();
                contact.FirstName = txtFirstName.Text;
                contact.LastName = txtLastName.Text;
                contact.PhoneNumber = txtPhoneNumber.Text;
                contact.Email = txtEmail.Text;
                contact.Relation = cmbRelation.Text;
                contact.ImageIndex = cmbRelation.SelectedIndex;
                return contact;
            }
            //Setter
            set
            {
                txtFirstName.Text = value.FirstName;
                txtLastName.Text = value.LastName;
                txtPhoneNumber.Text = value.PhoneNumber;
                txtEmail.Text = value.Email;
                cmbRelation.Text = value.Relation;
                cmbRelation.SelectedIndex = value.ImageIndex;
            }
        }

        //Counter for index purposes
        int x = 0;

        //Create the mysql connection
        MySqlConnection conn = new MySqlConnection();

        //Create the data table to hold the data pulled from the database
        DataTable theData = new DataTable();

        //Declare the sql reader
        MySqlDataReader reader = null;

        //Create the custome event handler 
        public EventHandler<ModifyObjectEventArgs> ModifyObject;

        private bool RetrieveData()
        {
            //SQL string to get the data from the database
            string sql = "SELECT FirstName, LastName, PhoneNumber, Email, Relation FROM MyContacts LIMIT 10";

            //SQL adapter
            MySqlDataAdapter adr = new MySqlDataAdapter(sql, conn);

            //SQL adapter command type
            adr.SelectCommand.CommandType = CommandType.Text;

            // Method to fill the datatable with the database data
            adr.Fill(theData);

            //Loop through the data rows and add the items to the list and listview
            for (int i = 0; i < theData.Rows.Count; i++)
            {
                //Instantiate a new Contact
                Contact contact = new Contact();

                //Populate the user controls with the data from the database
                contact.FirstName = theData.Rows[i]["FirstName"].ToString();
                contact.LastName = theData.Rows[i]["LastName"].ToString();
                contact.Email = theData.Rows[i]["Email"].ToString();
                contact.Relation = theData.Rows[i]["Relation"].ToString();
                contact.PhoneNumber = theData.Rows[i]["PhoneNumber"].ToString();

                //Conditionals to set the image index
                if (contact.Relation == "Friend")
                {
                    contact.ImageIndex = 0;
                }
                else if (contact.Relation =="Family")
                {
                    contact.ImageIndex = 1;
                }
                else if (contact.Relation =="Business")
                {
                    contact.ImageIndex = 2;
                }
                else if (contact.Relation == "Other")
                {
                    contact.ImageIndex = 3;
                }
                //Instantiate a new listviewitem and set the values
                ListViewItem lvi = new ListViewItem();
                lvi.Text = contact.ToString();
                lvi.ImageIndex = contact.ImageIndex;
                lvi.Tag = contact;

                //Add the listview item to the listview
                lvwContacts.Items.Add(lvi);

                //Display the first item in the listview
                GetContact = lvwContacts.Items[0].Tag as Contact;
            }

            //Close the connection
            conn.Close();
            return true;
        }

        private void Connect(string myConnectionString)
        {
            try
            {
                //Create the sql connection string
                conn.ConnectionString = myConnectionString;

                //Open the connection
                conn.Open();
            }
            catch (MySqlException e)
            {
                //String variable to hold message
                string msg = "";

                //Switch to show what the error is
                switch (e.Number)
                {
                    case 0:
                        msg = e.ToString();
                        break;
                    case 1042:
                        msg = "Can't resolve host address.\n" + myConnectionString;
                        break;
                    case 1045:
                        msg = "Invalid username/password";
                        break;
                    default:
                        msg = e.ToString() + "\n" + myConnectionString;
                        break;
                }
                //Messagebox to show the error message to the user
                MessageBox.Show(msg);
            }
        }

        private string BuildConnectionString()
        {
            //String variable for the server ip
            string serverIp = "";

            //String variable for the port
            string port = "";
            try
            {
                //Streamreader to acces the ip address on the text file
                using (StreamReader sr = new StreamReader("C:\\VFW\\connect.txt"))
                {
                    serverIp = sr.ReadLine();
                    port = sr.ReadLine();
                }
            }
            catch (Exception e)
            {
                //Messagebox to show the error to the user
                MessageBox.Show(e.ToString());
            }
            return "server=" + serverIp + "; uid=dbsAdmin; pwd=password; database=ContactList; SslMode=none; port=" + port + ";";
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            //Check that the value is greater than 0 to avoid out of bounds error
            if (x > 0)
            {
                //Show the value of the index
                GetContact = lvwContacts.Items[x - 1].Tag as Contact;

                //Decrease the value
                x--;
            }
            else
            {
                return;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            //Check the value to avoid out of bounds error
            if (x < lvwContacts.Items.Count - 1)
            {
                //Show the value of the index
                GetContact = lvwContacts.Items[x + 1].Tag as Contact;

                //Increase the value
                x++;
            }
            else
            {
                return;
            }
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            //Set the value of x to zero
            x = 0;

            //Show the value at the first index
            GetContact = lvwContacts.Items[x].Tag as Contact;
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            //Set the value of x to the last index number
            x = lvwContacts.Items.Count - 1;

            //Show the value at the last index
            GetContact = lvwContacts.Items[x].Tag as Contact;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //Close the connection
            conn.Close();

            //Store the information in the user database
            string stm = "INSERT into MyContacts (FirstName, LastName, PhoneNumber, Email, Relation) values (@FirstName,@LastName,@PhoneNumber,@Email,@Relation)";

            // Prepare SQL Statement
            MySqlCommand cmd = new MySqlCommand(stm, conn);

            //Binding variables
            cmd.Parameters.AddWithValue("@FirstName", GetContact.FirstName);
            cmd.Parameters.AddWithValue("@LastName", GetContact.LastName);
            cmd.Parameters.AddWithValue("@PhoneNumber", GetContact.PhoneNumber);
            cmd.Parameters.AddWithValue("@Email", GetContact.Email);
            cmd.Parameters.AddWithValue("@Relation", GetContact.Relation);

            //Open the connection
            conn.Open();

            // Execute SQL statement
            reader = cmd.ExecuteReader();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (lvwContacts.FocusedItem != null)
            {
                //Grab the data from the listview item
                Contact gc = lvwContacts.FocusedItem.Tag as Contact;

                //Close the connection
                conn.Close();

                //Store the information in the user database
                string stm = "UPDATE MyContacts SET FirstName = @FirstName, LastName = @LastName, PhoneNumber = @PhoneNumber, Email = @Email, Relation = @Relation WHERE PhoneNumber = " + gc.PhoneNumber;

                // Prepare SQL Statement
                MySqlCommand cmd = new MySqlCommand(stm, conn);

                //Binding variables
                cmd.Parameters.AddWithValue("@FirstName", GetContact.FirstName);
                cmd.Parameters.AddWithValue("@LastName", GetContact.LastName);
                cmd.Parameters.AddWithValue("@PhoneNumber", GetContact.PhoneNumber);
                cmd.Parameters.AddWithValue("@Email", GetContact.Email);
                cmd.Parameters.AddWithValue("@Relation", GetContact.Relation);

                //Open the connection
                conn.Open();

                // Execute SQL statement
                reader = cmd.ExecuteReader();

                //Check for the null and check to make sure that an object is selected
                if (ModifyObject != null && lvwContacts.SelectedItems.Count > 0)
                {
                    //Call the modifyobject custom event
                    ModifyObject(this, new ModifyObjectEventArgs(lvwContacts.SelectedItems[0]));
                }
            }
            else
            {
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //Close the connection
            conn.Close();

            //Store the information in the user database
            string stm = "DELETE from MyContacts WHERE FirstName = @FirstName AND LastName = @LastName";

            // Prepare SQL Statement
            MySqlCommand cmd = new MySqlCommand(stm, conn);

            //Binding variables
            cmd.Parameters.AddWithValue("@FirstName", GetContact.FirstName);
            cmd.Parameters.AddWithValue("@LastName", GetContact.LastName);
            cmd.Parameters.AddWithValue("@PhoneNumber", GetContact.PhoneNumber);
            cmd.Parameters.AddWithValue("@Email", GetContact.Email);
            cmd.Parameters.AddWithValue("@Relation", GetContact.Relation);

            //Open the connection
            conn.Open();

            // Execute SQL statement
            reader = cmd.ExecuteReader();

            //Conditionals to avoid null exception
            if (lvwContacts.FocusedItem != null && lvwContacts.Items.Count > 0)
            {
                    //Remove the item at the selected index
                    lvwContacts.FocusedItem.Remove();
            }
            else
            {
                //Reset the user inputs to blank
                GetContact = new Contact();

                //Display a message to the user
                MessageBox.Show("No more items to delete");
            }

            //Clear the user input fields
            GetContact = new Contact();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            //Clear all user input fields
            GetContact = new Contact();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Create the stream variable
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            //Set the filters for the file
            saveFileDialog1.InitialDirectory = "c:";
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    //Use StreamWriter to save the file
                    using (StreamWriter outStream = new StreamWriter(myStream))
                    {
                        for (int i = 0; i < lvwContacts.Items.Count; i++)
                        {
                            Contact c = lvwContacts.Items[i].Tag as Contact;

                            outStream.WriteLine(c.GetStringToSave());
                        }
                    }
                    myStream.Close();
                }
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Allows the user to retrieve a file and load the values 

            //Clear the listview to avoid duplication
            lvwContacts.Items.Clear();

            //Variable for our string to hold the value we pull from the file
            string line = null;

            //Variable to hold the value the user chooses to load
            string fileName = null;

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\Documents";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = openFileDialog1.FileName;
            }

            if (fileName != null)
            {
                //List to hold our string values pulled from the saved file
                List<Contact> contacts = new List<Contact>();
                
                //Open the stream reader to pull up the file chosen
                using (StreamReader inStream = new StreamReader(fileName))
                {
                    while (inStream.Peek() > -1)
                    {
                        line = inStream.ReadLine();

                        contacts.Add(new Contact(line));
                    }
                    for (int x = 0; x < contacts.Count; x++)
                    {
                        //Instantiate a new listviewitem and set the values
                        ListViewItem lvi = new ListViewItem();
                        lvi.Text = contacts[x].ToString();
                        lvi.ImageIndex = contacts[x].ImageIndex;
                        lvi.Tag = contacts[x];

                        //Add the listview item to the listview
                        lvwContacts.Items.Add(lvi);
                    }
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Exit the Application
            Application.Exit();
        }

        private void lvwContacts_Click(object sender, EventArgs e)
        {
            //Reset the value of x to 0 to avoid crashes
            x = 0;

            //Show the value of the clicked item
            GetContact = lvwContacts.FocusedItem.Tag as Contact;

            //Set the value of x to the index clicked on
            x = lvwContacts.SelectedIndices[x];
        }

        public class ModifyObjectEventArgs : EventArgs
        {
            //Declare a new listviewitem
            ListViewItem ObjectToModify;

            //Set the properties
            public ListViewItem ObjectToModify1 { get => ObjectToModify; set => ObjectToModify = value; }

            //Constructor for class object
            public ModifyObjectEventArgs(ListViewItem lvi)
            {
                ObjectToModify = lvi;
            }
        }

        private void HandleModifyObject(object sender, ModifyObjectEventArgs e)
        {
            //Set the object equal to the listview item selected
            Contact contact = new Contact();
            contact.FirstName = txtFirstName.Text;
            contact.LastName = txtLastName.Text;
            contact.PhoneNumber = txtPhoneNumber.Text;
            contact.Email = txtEmail.Text;
            contact.Relation = cmbRelation.Text;
            
            e.ObjectToModify1.Text = contact.ToString();
            e.ObjectToModify1.ImageIndex = contact.ImageIndex;
            e.ObjectToModify1.Tag = GetContact;
        }
    }
}

