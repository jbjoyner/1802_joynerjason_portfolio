﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace JoynerJason_CodeAssignment3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            HandleClientWindowSize();
        }

        //Written by Keith Webster.  Used with permission.  Not to be distributed.  
        //Place this inside the class space in the form you would like to size.
        //Call this method AFTER InitializeComponent() inside the form's constructor.
        void HandleClientWindowSize()
        {
            //Modify ONLY these float values
            float HeightValueToChange = 1.6f;
            float WidthValueToChange = 6.0f;

            //DO NOT MODIFY THIS CODE
            int height = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Height / HeightValueToChange);
            int width = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Width / WidthValueToChange);
            if (height < Size.Height)
                height = Size.Height;
            if (width < Size.Width)
                width = Size.Width;
            this.Size = new Size(width, height);
        }

        //Global variable for the Data object
        public Data GetData;

        //List to hold the data for the game board
        List<Data> data = new List<Data>();

        //Global counter to control loop
        int x = 0;

        //Global boolean to check for double play
        bool doublePlay = false;

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Turns on the check mark the blue pieces
            blueToolStripMenuItem.Checked = true;

            //Turns of the check mark for the red pieces
            redToolStripMenuItem.Checked = false;

            //Switch all buttons to blue
            r1c1button.ImageList = blueImages;
            r1c2button.ImageList = blueImages;
            r1c3button.ImageList = blueImages;
            r2c1button.ImageList = blueImages;
            r2c2button.ImageList = blueImages;
            r2c3button.ImageList = blueImages;
            r3c1button.ImageList = blueImages;
            r3c2button.ImageList = blueImages;
            r3c3button.ImageList = blueImages;
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Turns on the check mark for the red pieces
            redToolStripMenuItem.Checked = true;

            //Turns off the check mark the blue pieces
            blueToolStripMenuItem.Checked = false;

            //Switch all buttons to red
            r1c1button.ImageList = redImages;
            r1c2button.ImageList = redImages;
            r1c3button.ImageList = redImages;
            r2c1button.ImageList = redImages;
            r2c2button.ImageList = redImages;
            r2c3button.ImageList = redImages;
            r3c1button.ImageList = redImages;
            r3c2button.ImageList = redImages;
            r3c3button.ImageList = redImages;
        }

        private void xToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Mark x with check mark
            xToolStripMenuItem.Checked = true;

            //Remove check mark form 0
            oToolStripMenuItem.Checked = false;

            //Set the value of doublePlay to false
            doublePlay = false;
        }

        private void oToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Mark x with check mark
            xToolStripMenuItem.Checked = false;

            //Remove check mark from o
            oToolStripMenuItem.Checked = true;

            //Set the value of doublePlat to false
            doublePlay = false;
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            //Clear all the buttons
            r1c1button.ImageList = null;
            r1c2button.ImageList = null;
            r1c3button.ImageList = null;
            r2c1button.ImageList = null;
            r2c2button.ImageList = null;
            r2c3button.ImageList = null;
            r3c1button.ImageList = null;
            r3c2button.ImageList = null;
            r3c3button.ImageList = null;

            r1c1button.ImageIndex = -1;
            r1c2button.ImageIndex = -1;
            r1c3button.ImageIndex = -1;
            r2c1button.ImageIndex = -1;
            r2c2button.ImageIndex = -1;
            r2c3button.ImageIndex = -1;
            r3c1button.ImageIndex = -1;
            r3c2button.ImageIndex = -1;
            r3c3button.ImageIndex = -1;

            //Clear the list
            data.Clear();

            //Turns on the check mark the blue pieces
            blueToolStripMenuItem.Checked = true;

            //Turns of the check mark for the red pieces
            redToolStripMenuItem.Checked = false;

            //Remove the check for x
            xToolStripMenuItem.Checked = false;

            //Remove the check for o
            oToolStripMenuItem.Checked = false;

            //Set the value of x to 0
            x = 0;
        }

        private void r1c1button_Click(object sender, EventArgs e)
        {
            //Create a new Data object
            GetData = new Data();

            //Check for null
            if (r1c1button.Image == null)
            {
                //Check for both values to be true
                if (blueToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c1button.ImageList = blueImages;
                        r1c1button.ImageIndex = 1;
                        GetData.Button = "r1c1button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = true;
                        GetData.RedChecked = false;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "blueImages";
                        CheckForDoublePlay();
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (blueToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c1button.ImageList = blueImages;
                        r1c1button.ImageIndex = 0;
                        GetData.Button = "r1c1button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = false;
                        GetData.RedChecked = false;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 0;
                        GetData.ImageList = "blueImages";
                        CheckForDoublePlay();
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c1button.ImageList = redImages;
                        r1c1button.ImageIndex = 1;
                        GetData.Button = "r1c1button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = true;
                        GetData.RedChecked = true;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        CheckForDoublePlay();
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c1button.ImageList = redImages;
                        r1c1button.ImageIndex = 0;
                        GetData.Button = "r1c1button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = false;
                        GetData.RedChecked = true;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        CheckForDoublePlay();
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
            }
            else
            {
                MessageBox.Show("That box is already chosen. Please select another box.");
            }
        }

        private void r1c2button_Click(object sender, EventArgs e)
        {
            //Create a new Data object
            GetData = new Data();

            //Check for null
            if (r1c2button.Image == null)
            {
                //Check for both values to be true
                if (blueToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c2button.ImageList = blueImages;
                        r1c2button.ImageIndex = 1;
                        GetData.Button = "r1c2button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = true;
                        GetData.RedChecked = false;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (blueToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c2button.ImageList = blueImages;
                        r1c2button.ImageIndex = 0;
                        GetData.Button = "r1c2button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = false;
                        GetData.RedChecked = false;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 0;
                        GetData.ImageList = "blueImages";
                        CheckForDoublePlay();
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c2button.ImageList = redImages;
                        r1c2button.ImageIndex = 1;
                        GetData.Button = "r1c2button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = true;
                        GetData.RedChecked = true;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        CheckForDoublePlay();
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c2button.ImageList = redImages;
                        r1c2button.ImageIndex = 0;
                        GetData.Button = "r1c2button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = false;
                        GetData.RedChecked = true;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        CheckForDoublePlay();
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
            }
            else
            {
                MessageBox.Show("That box is already chosen. Please select another box.");
            }
        }

        private void r1c3button_Click(object sender, EventArgs e)
        {
            //Create a new Data object
            GetData = new Data();

            //Check for null
            if (r1c3button.Image == null)
            {
                //Check for both values to be true
                if (blueToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c3button.ImageList = blueImages;
                        r1c3button.ImageIndex = 1;
                        GetData.Button = "r1c3button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = true;
                        GetData.RedChecked = false;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (blueToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c3button.ImageList = blueImages;
                        r1c3button.ImageIndex = 0;
                        GetData.Button = "r1c3button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = false;
                        GetData.RedChecked = false;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 0;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c3button.ImageList = redImages;
                        r1c3button.ImageIndex = 1;
                        GetData.Button = "r1c3button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = true;
                        GetData.RedChecked = true;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r1c3button.ImageList = redImages;
                        r1c3button.ImageIndex = 0;
                        GetData.Button = "r1c3button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = false;
                        GetData.RedChecked = true;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
            }
            else
            {
                MessageBox.Show("That box is already chosen. Please select another box.");
            }
        }

        private void r2c1button_Click(object sender, EventArgs e)
        {
            //Create a new Data object
            GetData = new Data();

            //Check for null
            if (r2c1button.Image == null)
            {
                //Check for both values to be true
                if (blueToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c1button.ImageList = blueImages;
                        r2c1button.ImageIndex = 1;
                        GetData.Button = "r2c1button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = true;
                        GetData.RedChecked = false;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (blueToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c1button.ImageList = blueImages;
                        r2c1button.ImageIndex = 0;
                        GetData.Button = "r2c1button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = false;
                        GetData.RedChecked = false;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 0;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c1button.ImageList = redImages;
                        r2c1button.ImageIndex = 1;
                        GetData.Button = "r2c1button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = true;
                        GetData.RedChecked = true;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c1button.ImageList = redImages;
                        r2c1button.ImageIndex = 0;
                        GetData.Button = "r2c1button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = false;
                        GetData.RedChecked = true;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
            }
            else
            {
                MessageBox.Show("That box is already chosen. Please select another box.");
            }
        }

        private void r2c2button_Click(object sender, EventArgs e)
        {
            //Create a new Data object
            GetData = new Data();

            //Check for null
            if (r2c2button.Image == null)
            {
                //Check for both values to be true
                if (blueToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c2button.ImageList = blueImages;
                        r2c2button.ImageIndex = 1;
                        GetData.Button = "r2c2button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = true;
                        GetData.RedChecked = false;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (blueToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c2button.ImageList = blueImages;
                        r2c2button.ImageIndex = 0;
                        GetData.Button = "r2c2button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = false;
                        GetData.RedChecked = false;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 0;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c2button.ImageList = redImages;
                        r2c2button.ImageIndex = 1;
                        GetData.Button = "r2c2button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = true;
                        GetData.RedChecked = true;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c2button.ImageList = redImages;
                        r2c2button.ImageIndex = 0;
                        GetData.Button = "r2c2button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = false;
                        GetData.RedChecked = true;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
            }
            else
            {
                MessageBox.Show("That box is already chosen. Please select another box.");
            }
        }

        private void r2c3button_Click(object sender, EventArgs e)
        {
            //Create a new Data object
            GetData = new Data();

            //Check for null
            if (r2c3button.Image == null)
            {
                //Check for both values to be true
                if (blueToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c3button.ImageList = blueImages;
                        r2c3button.ImageIndex = 1;
                        GetData.Button = "r2c3button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = true;
                        GetData.RedChecked = false;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (blueToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c3button.ImageList = blueImages;
                        r2c3button.ImageIndex = 0;
                        GetData.Button = "r2c3button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = false;
                        GetData.RedChecked = false;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 0;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c3button.ImageList = redImages;
                        r2c3button.ImageIndex = 1;
                        GetData.Button = "r2c3button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = true;
                        GetData.RedChecked = true;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r2c3button.ImageList = redImages;
                        r2c3button.ImageIndex = 0;
                        GetData.Button = "r2c3button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = false;
                        GetData.RedChecked = true;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
            }
            else
            {
                MessageBox.Show("That box is already chosen. Please select another box.");
            }
        }

        private void r3c1button_Click(object sender, EventArgs e)
        {
            //Create a new Data object
            GetData = new Data();

            //Check for null
            if (r3c1button.Image == null)
            {
                //Check for both values to be true
                if (blueToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r3c1button.ImageList = blueImages;
                        r3c1button.ImageIndex = 1;
                        GetData.Button = "r3c1button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = true;
                        GetData.RedChecked = false;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (blueToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r3c1button.ImageList = blueImages;
                        r3c1button.ImageIndex = 0;
                        GetData.Button = "r3c1button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = false;
                        GetData.RedChecked = false;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 0;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r3c1button.ImageList = redImages;
                        r3c1button.ImageIndex = 1;
                        GetData.Button = "r3c1button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = true;
                        GetData.RedChecked = true;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r3c1button.ImageList = redImages;
                        r3c1button.ImageIndex = 0;
                        GetData.Button = "r3c1button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = false;
                        GetData.RedChecked = true;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
            }
            else
            {
                MessageBox.Show("That box is already chosen. Please select another box.");
            }
        }

        private void r3c2button_Click(object sender, EventArgs e)
        {
            //Create a new Data object
            GetData = new Data();

            //Check for null
            if (r3c2button.Image == null)
            {
                //Check for both values to be true
                if (blueToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r3c2button.ImageList = blueImages;
                        r3c2button.ImageIndex = 1;
                        GetData.Button = "r3c2button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = true;
                        GetData.RedChecked = false;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (blueToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r3c2button.ImageList = blueImages;
                        r3c2button.ImageIndex = 0;
                        GetData.Button = "r3c2button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = false;
                        GetData.RedChecked = false;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 0;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r3c2button.ImageList = redImages;
                        r3c2button.ImageIndex = 1;
                        GetData.Button = "r3c2button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = true;
                        GetData.RedChecked = true;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r3c2button.ImageList = redImages;
                        r3c2button.ImageIndex = 0;
                        GetData.Button = "r3c2button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = false;
                        GetData.RedChecked = true;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
            }
            else
            {
                MessageBox.Show("That box is already chosen. Please select another box.");
            }
        }

        private void r3c3button_Click(object sender, EventArgs e)
        {
            //Create a new Data object
            GetData = new Data();

            //Check for null
            if (r3c3button.Image == null)
            {
                //Check for both values to be true
                if (blueToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r3c3button.ImageList = blueImages;
                        r3c3button.ImageIndex = 1;
                        GetData.Button = "r3c3button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = true;
                        GetData.RedChecked = false;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (blueToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r3c3button.ImageList = blueImages;
                        r3c3button.ImageIndex = 0;
                        GetData.Button = "r3c3button";
                        GetData.BlueChecked = true;
                        GetData.XChecked = false;
                        GetData.RedChecked = false;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 0;
                        GetData.ImageList = "blueImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && xToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    {
                        //Set the button values and store the data to the object and store the object in a list
                        r3c3button.ImageList = redImages;
                        r3c3button.ImageIndex = 1;
                        GetData.Button = "r3c3button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = true;
                        GetData.RedChecked = true;
                        GetData.OChecked = false;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
                //Check for both values to be true
                else if (redToolStripMenuItem.Checked == true && oToolStripMenuItem.Checked == true)
                {
                    //Check for a double play
                    CheckForDoublePlay();

                    //Run if value is false
                    if (doublePlay == false)
                    { 
                        //Set the button values and store the data to the object and store the object in a list
                        r3c3button.ImageList = redImages;
                        r3c3button.ImageIndex = 0;
                        GetData.Button = "r3c3button";
                        GetData.BlueChecked = false;
                        GetData.XChecked = false;
                        GetData.RedChecked = true;
                        GetData.OChecked = true;
                        GetData.ImageIndex = 1;
                        GetData.ImageList = "redImages";
                        data.Add(GetData);
                        GetScore();
                        x++;
                    }
                }
            }
            else
            {
                MessageBox.Show("That box is already chosen. Please select another box.");
            }
        }

        private void loadGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnNewGame_Click(this, new EventArgs());
            
            {
                OpenFileDialog dlg = new OpenFileDialog();

                if (DialogResult.OK == dlg.ShowDialog())
                {
                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.ConformanceLevel = ConformanceLevel.Document;

                    settings.IgnoreComments = true;
                    settings.IgnoreWhitespace = true;

                    using (XmlReader reader = XmlReader.Create(dlg.FileName, settings))
                    {
                        reader.MoveToContent();

                        if (reader.Name != "TicTacToeXML")
                        {
                            MessageBox.Show("This is not correct stock data");
                            return;
                        }

                        while (reader.Read())
                        {
                            Data GetData = new Data();

                            if (reader.Name == "Button" && reader.IsStartElement())
                            {
                                GetData.Button = reader.ReadElementContentAsString();
                            }
                            if (reader.Name == "ImageList" && reader.IsStartElement())
                            {
                                GetData.ImageList = reader.ReadElementContentAsString();
                            }
                            if (reader.Name == "ImageIndex" && reader.IsStartElement())
                            {
                                GetData.ImageIndex = reader.ReadElementContentAsInt();
                            }
                            if (reader.Name == "RedChecked" && reader.IsStartElement())
                            {
                                GetData.RedChecked = reader.ReadElementContentAsBoolean();
                            }
                            if (reader.Name == "BlueChecked" && reader.IsStartElement())
                            {
                                GetData.BlueChecked = reader.ReadElementContentAsBoolean();
                            }
                            if (reader.Name == "XChecked" && reader.IsStartElement())
                            {
                                GetData.XChecked = reader.ReadElementContentAsBoolean();
                            }
                            if (reader.Name == "OChecked" && reader.IsStartElement())
                            {
                                GetData.OChecked = reader.ReadElementContentAsBoolean();
                            }
                            
                            data.Add(GetData);
                            x++;

                            foreach (Data d in data)
                            {
                                //MessageBox.Show($"{d.Button}");
                                if (d.Button == "r1c1button")
                                {
                                    r1c1button.ImageList = blueImages;
                                    r1c1button.ImageIndex = d.ImageIndex;
                                }
                                else if (d.Button =="r1c2button")
                                {
                                    r1c2button.ImageList = blueImages;
                                    r1c2button.ImageIndex = d.ImageIndex;
                                }
                                else if (d.Button == "r1c3button")
                                {
                                    r1c3button.ImageList = blueImages;
                                    r1c3button.ImageIndex = d.ImageIndex;
                                }
                                else if (d.Button == "r2c1button")
                                {
                                    r2c1button.ImageList = blueImages;
                                    r2c1button.ImageIndex = d.ImageIndex;
                                }
                                else if (d.Button == "r2c2button")
                                {
                                    r2c2button.ImageList = blueImages;
                                    r2c2button.ImageIndex = d.ImageIndex;
                                }
                                else if (d.Button == "r2c3button")
                                {
                                    r2c3button.ImageList = blueImages;
                                    r2c3button.ImageIndex = d.ImageIndex;
                                }
                                else if (d.Button == "r3c1button")
                                {
                                    r3c1button.ImageList = blueImages;
                                    r3c1button.ImageIndex = d.ImageIndex;
                                }
                                else if (d.Button == "r3c2button")
                                {
                                    r3c2button.ImageList = blueImages;
                                    r3c2button.ImageIndex = d.ImageIndex;
                                }
                                else if (d.Button == "r3c3button")
                                {
                                    r3c3button.ImageList = blueImages;
                                    r3c3button.ImageIndex = d.ImageIndex;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void saveGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();

            dlg.DefaultExt = "xml";

            if (DialogResult.OK == dlg.ShowDialog())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.ConformanceLevel = ConformanceLevel.Document;

                settings.Indent = true;

                using (XmlWriter writer = XmlWriter.Create(dlg.FileName, settings))
                {
                    writer.WriteStartElement("TicTacToeXML");

                    for (int i = 0; i < data.Count; i++)
                    {
                        Data GetData = new Data();

                        GetData = data[i];

                        writer.WriteStartElement("TicTacToeItem");

                        writer.WriteElementString("Button", GetData.Button.ToString());

                        writer.WriteElementString("ImageList", GetData.ImageList.ToString());

                        writer.WriteElementString("ImageIndex", GetData.ImageIndex.ToString());

                        writer.WriteElementString("RedChecked", GetData.RedChecked.ToString().ToLower());

                        writer.WriteElementString("BlueChecked", GetData.BlueChecked.ToString().ToLower());

                        writer.WriteElementString("XChecked", GetData.XChecked.ToString().ToLower());

                        writer.WriteElementString("OChecked", GetData.OChecked.ToString().ToLower());

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();

                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Exit the application
            Application.Exit();
        }

        private void GetScore()
        {
            //Interger to check for 3 in a row
            int x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c1button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r1c2button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r1c3button" && d.XChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("X is the Winner!");
                }
            }

            //Reset the value to o
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r2c1button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c2button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c3button" && d.XChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("X is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r3c1button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c2button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c3button" && d.XChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("X is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c1button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c1button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c1button" && d.XChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("X is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c2button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c2button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c2button" && d.XChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("X is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c3button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c3button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c3button" && d.XChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("X is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c1button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c2button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c3button" && d.XChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("X is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c3button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c2button" && d.XChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c1button" && d.XChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("X is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c1button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r1c2button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r1c3button" && d.OChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("O is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r2c1button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c2button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c3button" && d.OChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("O is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r3c1button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c2button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c3button" && d.OChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("O is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c1button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c1button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c1button" && d.OChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("O is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c2button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c2button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c2button" && d.OChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("O is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c3button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c3button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c3button" && d.OChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("O is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c1button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c2button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c3button" && d.OChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("O is the Winner!");
                }
            }

            //Reset the value to 0
            x = 0;

            //Loop through the list to check for 3 in a row
            foreach (Data d in data)
            {
                if (d.Button == "r1c3button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r2c2button" && d.OChecked == true)
                {
                    x++;
                }
                if (d.Button == "r3c1button" && d.OChecked == true)
                {
                    x++;
                }
                if (x == 3)
                {
                    MessageBox.Show("O is the Winner!");
                }
            }
        }

        private bool CheckForDoublePlay()
        {
            //Check that their is an object in the list
            if (data.Count > 0)
            {
                //Check to see if the value matches the selected letter
                if (data[x - 1].XChecked == true && xToolStripMenuItem.Checked == true)
                {
                    MessageBox.Show("Sorry it is O's turn to play");
                    doublePlay = true;
                }

                //Check to see if the value matches the selected letter
                else if (data[x - 1].OChecked == true && oToolStripMenuItem.Checked == true)
                {
                    MessageBox.Show("Sorry it is X's turn to play");
                    doublePlay = true;
                }
            }
            return doublePlay;
        }
    }
}

