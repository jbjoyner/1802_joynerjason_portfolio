﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodeAssignment3
{
    public class Data
    {
        string button;
        string imageList;
        int imageIndex;
        bool redChecked;
        bool blueChecked;
        bool xChecked;
        bool oChecked;

        public string Button { get => button; set => button = value; }
        public string ImageList { get => imageList; set => imageList = value; }
        public int ImageIndex { get => imageIndex; set => imageIndex = value; }
        public bool RedChecked { get => redChecked; set => redChecked = value; }
        public bool BlueChecked { get => blueChecked; set => blueChecked = value; }
        public bool XChecked { get => xChecked; set => xChecked = value; }
        public bool OChecked { get => oChecked; set => oChecked = value; }
    }
}
