﻿namespace JoynerJason_CodeAssignment1
{
    partial class NeedHaveList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstNeed = new System.Windows.Forms.ListBox();
            this.lstHave = new System.Windows.Forms.ListBox();
            this.btnMoveToHave = new System.Windows.Forms.Button();
            this.btnMoveToNeed = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lstNeed
            // 
            this.lstNeed.FormattingEnabled = true;
            this.lstNeed.ItemHeight = 24;
            this.lstNeed.Location = new System.Drawing.Point(58, 159);
            this.lstNeed.Name = "lstNeed";
            this.lstNeed.Size = new System.Drawing.Size(175, 388);
            this.lstNeed.TabIndex = 0;
            this.lstNeed.DoubleClick += new System.EventHandler(this.lstNeed_DoubleClick);
            // 
            // lstHave
            // 
            this.lstHave.FormattingEnabled = true;
            this.lstHave.ItemHeight = 24;
            this.lstHave.Location = new System.Drawing.Point(249, 159);
            this.lstHave.Name = "lstHave";
            this.lstHave.Size = new System.Drawing.Size(175, 388);
            this.lstHave.TabIndex = 1;
            this.lstHave.DoubleClick += new System.EventHandler(this.lstHave_DoubleClick);
            // 
            // btnMoveToHave
            // 
            this.btnMoveToHave.AutoSize = true;
            this.btnMoveToHave.Location = new System.Drawing.Point(85, 612);
            this.btnMoveToHave.Name = "btnMoveToHave";
            this.btnMoveToHave.Size = new System.Drawing.Size(145, 55);
            this.btnMoveToHave.TabIndex = 2;
            this.btnMoveToHave.Text = "Move to Have";
            this.btnMoveToHave.UseVisualStyleBackColor = true;
            this.btnMoveToHave.Click += new System.EventHandler(this.btnMoveToHave_Click);
            // 
            // btnMoveToNeed
            // 
            this.btnMoveToNeed.AutoSize = true;
            this.btnMoveToNeed.Location = new System.Drawing.Point(85, 673);
            this.btnMoveToNeed.Name = "btnMoveToNeed";
            this.btnMoveToNeed.Size = new System.Drawing.Size(145, 53);
            this.btnMoveToNeed.TabIndex = 3;
            this.btnMoveToNeed.Text = "Move to Need";
            this.btnMoveToNeed.UseVisualStyleBackColor = true;
            this.btnMoveToNeed.Click += new System.EventHandler(this.btnMoveToNeed_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(249, 612);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(145, 55);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::JoynerJason_CodeAssignment1.Properties.Resources.Iphone7Image;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(448, 823);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 550);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Need";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(244, 550);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Have";
            // 
            // NeedHaveList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 854);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnMoveToNeed);
            this.Controls.Add(this.btnMoveToHave);
            this.Controls.Add(this.lstHave);
            this.Controls.Add(this.lstNeed);
            this.Controls.Add(this.pictureBox1);
            this.Location = new System.Drawing.Point(500, 0);
            this.Name = "NeedHaveList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "NeedHaveList";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NeedHaveList_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstNeed;
        private System.Windows.Forms.ListBox lstHave;
        private System.Windows.Forms.Button btnMoveToHave;
        private System.Windows.Forms.Button btnMoveToNeed;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}