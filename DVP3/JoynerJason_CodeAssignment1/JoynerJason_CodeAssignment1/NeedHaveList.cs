﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace JoynerJason_CodeAssignment1
{
    public partial class NeedHaveList : Form
    {
        public NeedHaveList()
        {
            InitializeComponent();
            HandleClientWindowSize();
        }

        //Instantiate a new GroceryList Form
        GroceryList gl = new GroceryList();

        //EventHandlers
        public EventHandler PopulateGroceryList;
        public EventHandler ClearUserInput;

        //Written by Keith Webster.  Used with permission.  Not to be distributed.  
        //Place this inside the class space in the form you would like to size.
        //Call this method AFTER InitializeComponent() inside the form's constructor.
        void HandleClientWindowSize()
        {
            //Modify ONLY these float values
            float HeightValueToChange = 2.0f;
            float WidthValueToChange = 6.0f;

            //DO NOT MODIFY THIS CODE
            int height = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Height / HeightValueToChange);
            int width = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Width / WidthValueToChange);
            if (height < Size.Height)
                height = Size.Height;
            if (width < Size.Width)
                width = Size.Width;
            this.Size = new Size(width, height);
            //this.Size = new Size(376, 720);
        }

        public void AddToNeedClick_Click(object sender, EventArgs e)
        {
            //Instantiate a new GroceryList Form
            GroceryList gl = new GroceryList();

            //Grab the GroceryList and save it in the variable
            gl = (GroceryList)sender;

            //Add the item to listbox
            lstNeed.Items.Add(gl.GetList);
        }

        public void AddToHaveClick_Click(object sender, EventArgs e)
        {
            //Instantiate a new GroceryList Form
            GroceryList gl = new GroceryList();

            //Grab the GroceryList and save it in the variable
            gl = (GroceryList)sender;

            //Add the item to listbox
            lstHave.Items.Add(gl.GetList);
        }

        private void NeedHaveList_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Close the Application
            Application.Exit();
        }

        private void btnMoveToHave_Click(object sender, EventArgs e)
        {
            //Grabs the object from the selected index and moves it to the other listbox
            if (lstNeed.SelectedIndex >= 0 && lstNeed.SelectedIndex < lstNeed.Items.Count)
            {
                //
                List tmpList = lstNeed.Items[lstNeed.SelectedIndex] as List;

                lstHave.Items.Add(tmpList);
                lstNeed.Items.RemoveAt(lstNeed.SelectedIndex);

                ClearUserInput?.Invoke(this, new EventArgs());
            }
        }

        private void btnMoveToNeed_Click(object sender, EventArgs e)
        {
            //Grabs the object from the selected index and moves it to the other listbox
            if (lstHave.SelectedIndex >= 0 && lstHave.SelectedIndex < lstHave.Items.Count)
            {
                List tmpList = lstHave.Items[lstHave.SelectedIndex] as List;

                lstNeed.Items.Add(tmpList);
                lstHave.Items.RemoveAt(lstHave.SelectedIndex);

                ClearUserInput?.Invoke(this, new EventArgs());
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //Deletes the object at the selected index
            if (lstNeed.SelectedIndex >= 0 && lstNeed.SelectedIndex < lstNeed.Items.Count)
            {
                lstNeed.Items.RemoveAt(lstNeed.SelectedIndex);
            }
            else if (lstHave.SelectedIndex >= 0 && lstHave.SelectedIndex < lstHave.Items.Count)
            {
                lstHave.Items.RemoveAt(lstHave.SelectedIndex);
            }

            ClearUserInput?.Invoke(this, new EventArgs());
        }

        private void lstNeed_DoubleClick(object sender, EventArgs e)
        {
            //Grabs the object from the selected index
            if (lstNeed.SelectedIndex >= 0 && lstNeed.SelectedIndex < lstNeed.Items.Count)
            {
                List list = lstNeed.Items[lstNeed.SelectedIndex] as List;

                PopulateGroceryList?.Invoke(list, new EventArgs());
            }
        }

        private void lstHave_DoubleClick(object sender, EventArgs e)
        {
            //Grabs the object from the selected index
            if (lstHave.SelectedIndex >= 0 && lstHave.SelectedIndex < lstHave.Items.Count)
            {
                List list = lstHave.Items[lstHave.SelectedIndex] as List;

                PopulateGroceryList?.Invoke(list, new EventArgs());
            }
        }

        public void SaveListBox(object sender, EventArgs e)
        {
            //Create the stream variable
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            //Set the filters for the file
            saveFileDialog1.InitialDirectory = "c:\\Documents";
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    //Use StreamWriter to save the file
                    using (StreamWriter outStream = new StreamWriter(myStream))
                    {
                        foreach (List list in lstNeed.Items)
                        {
                            list.ListNumer = 1;
                            outStream.WriteLine(list.GetStringToSave());
                        }

                        foreach (List list in lstHave.Items)
                        {
                            list.ListNumer = 2;
                            outStream.WriteLine(list.GetStringToSave());
                        }
                    }
                    myStream.Close();
                }
            }
        }

        public void LoadListBox(object sender, EventArgs e)
        {
            //Allows the user to retrieve a file and load the values 

            //Variable for our string to hold the value we pull from the file
            string line = null;

            //Variable to hold the value the user chooses to load
            string fileName = null;

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\Documents";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = openFileDialog1.FileName;
            }

            if (fileName != null)
            {
                //List to hold our string values pulled from the saved file
                List<List> lists = new List<List>();

                List list = new List();
                //Open the stream reader to pull up the file chosen
                using (StreamReader inStream = new StreamReader(fileName))
                {
                    int x = 0;
                    while (inStream.Peek() > -1)
                    {
                        line = inStream.ReadLine();

                        lists.Add(new List(line));

                        if (lists[x].ListNumer == 1)
                        {
                            lstNeed.Items.Add(lists[x]);
                        }
                        else if (lists[x].ListNumer == 2)
                        {
                            lstHave.Items.Add(lists[x]);
                        }
                        x++;
                    }
                }
            }
        }
    }
}
