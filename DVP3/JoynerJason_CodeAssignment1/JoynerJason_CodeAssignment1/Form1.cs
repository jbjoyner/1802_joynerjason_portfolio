﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace JoynerJason_CodeAssignment1
{
    public partial class GroceryList : Form
    {
        public GroceryList()
        {
            InitializeComponent();
            HandleClientWindowSize();
        }

        //Global Variables
        GroceryList gl;
        NeedHaveList nhl;

        //EventHandlers
        public EventHandler AddToNeedClick;
        public EventHandler AddToHaveClick;
        public EventHandler ClearUserInput;
        public EventHandler SaveClick;
        public EventHandler LoadClick;

        public List GetList
        {
            //Getters
            get
            {
                List list = new List();
                list.Items = txtItem.Text;
                list.Quantity = numQuantity.Value;
                list.Coupon = chkCoupon.Checked;
                return list;
            }
            //Setters
            set
            {
                txtItem.Text = value.Items;
                numQuantity.Value = value.Quantity;
                chkCoupon.Checked = value.Coupon;
            }
        }

        //Written by Keith Webster.  Used with permission.  Not to be distributed.  
        //Place this inside the class space in the form you would like to size.
        //Call this method AFTER InitializeComponent() inside the form's constructor.
        void HandleClientWindowSize()
        {
            //Modify ONLY these float values
            float HeightValueToChange = 2.0f;
            float WidthValueToChange = 6.0f;

            //DO NOT MODIFY THIS CODE
            int height = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Height / HeightValueToChange);
            int width = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Width / WidthValueToChange);
            if (height < Size.Height)
                height = Size.Height;
            if (width < Size.Width)
                width = Size.Width;
            this.Size = new Size(width, height);
        }

        private void btnAddToNeed_Click(object sender, EventArgs e)
        {
            //Invoke the eventhandlers
            AddToNeedClick?.Invoke(this, new EventArgs());
            ClearUserInput?.Invoke(this, new EventArgs());
        }

        private void GroceryList_Load(object sender, EventArgs e)
        {
            //instantiate new Forms
            nhl = new NeedHaveList();
            gl = new GroceryList();

            //Show the new form
            nhl.Show();

            //Subscribe to the methods
            AddToNeedClick += nhl.AddToNeedClick_Click;
            AddToHaveClick += nhl.AddToHaveClick_Click;
            nhl.PopulateGroceryList += PopulateGroceryList_Click;
            ClearUserInput += ClearUserInput_Click;
            nhl.ClearUserInput += ClearUserInput_Click;
            SaveClick += nhl.SaveListBox;
            LoadClick += nhl.LoadListBox;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Exit the application
            Application.Exit();
        }

        private void GroceryList_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Close the application
            Application.Exit();
        }

        public void PopulateGroceryList_Click(object sender, EventArgs e)
        {
            //Instantiate a new form
            List list = sender as List;

            //Set the new form to the List properties
            GetList = list;
        }

        public void ClearUserInput_Click(object sender, EventArgs e)
        {
            //Clear the user inputs
            GetList = new List();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Invoke the save method
            SaveClick?.Invoke(this, new EventArgs());
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Invoke the load method
            LoadClick?.Invoke(this, new EventArgs());
        }

        private void btnAddToHave_Click(object sender, EventArgs e)
        {
            //Invoke the eventhandlers
            AddToHaveClick?.Invoke(this, new EventArgs());
            ClearUserInput?.Invoke(this, new EventArgs());
        }
    }
}
