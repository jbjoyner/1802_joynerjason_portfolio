﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoynerJason_CodeAssignment1
{
    public class List
    {
        //Class Variables
        string items;
        decimal quantity;
        bool coupon;
        int listNumer;

        //Public Properties
        public string Items { get => items; set => items = value; }
        public decimal Quantity { get => quantity; set => quantity = value; }
        public bool Coupon { get => coupon; set => coupon = value; }
        public int ListNumer { get => listNumer; set => listNumer = value; }

        //Override tostring method
        public override string ToString()
        {
            return Items;
        }

        //Constructor for empty object
        public List()
        {

        }

        //Constructor for the object
        public List(string _items, decimal _quantity, bool _coupon, int _listNumber)
        {
            Items = _items;
            Quantity = _quantity;
            Coupon = _coupon;
            ListNumer = _listNumber;
        }

        public List(string str) : this()
        {
            //Array to hold the string once split
            string[] split = str.Split(',');

            //Place the values back in their fields
            Items = split[0];
            decimal tmpDecimal = 0;
            if (decimal.TryParse(split[1], out tmpDecimal))
            {
                Quantity = tmpDecimal;
            }
            bool tmpBool = false;
            if (bool.TryParse(split[2], out tmpBool))
            {
                Coupon = tmpBool;
            }
            int tmpListNumber = 0;
            if (int.TryParse(split[3], out tmpListNumber))
            {
                ListNumer = tmpListNumber;
            }
        }
        
        public string GetStringToSave()
        {
            string returnValue = "";

            returnValue = Items + "," + Quantity + "," + Coupon + "," + ListNumer;

            return returnValue;
        }
    }
}
